<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  //   \Illuminate\Support\Facades\Artisan::call('config:cache');
   //  \Illuminate\Support\Facades\Artisan::call('key:generate');
    return view('admin.auth.login');
});


Route::get('/reboot', function () {
     \Illuminate\Support\Facades\Artisan::call('config:cache');
     \Illuminate\Support\Facades\Artisan::call('key:generate');
  
});



Auth::routes();

Route::group(['middleware'=>['auth']],function(){
    /* admin middleware start */
    Route::group(['middleware'=>['admin']],function() {
        Route::get('/home', 'backend\AdminController@index')->name('home');
        Route::get('/admin', 'backend\AdminController@index')->name('admin.home');

        // Route::prefix('ProductManagement')->group(function(){

        /* Brand Add Start */
    Route::prefix('ProductManagement')->group(function(){
        Route::get('/brand', 'backend\BrandController@index')->name('brand.add');
        Route::post('/brand/add', 'backend\BrandController@store')->name('brand.store');
        Route::get('/brand/{id}/Show', 'backend\BrandController@show')->name('brand.edit');
        Route::post('/Brand/erase', 'backend\BrandController@destroy')->name('brand.destroy');
        Route::post('/brand/update', 'backend\BrandController@update')->name('brand.update');

        /* Brand Add End */

        /* Category Start */

        Route::get('/category', 'backend\CategoryController@index')->name('category.add');
        Route::post('/category/add', 'backend\CategoryController@store')->name('category.store');
        Route::get('/category/{id}/Show', 'backend\CategoryController@show')->name('category.edit');
        Route::post('/Category/erase', 'backend\CategoryController@destroy')->name('category.destroy');
        Route::post('/category/update', 'backend\CategoryController@update')->name('category.update');

        /* Category End */

        /* Sub-Category Start */

        Route::get('/sub-category', 'backend\SubCategoryController@index')->name('sub-category.add');
        Route::post('/sub-category/add', 'backend\SubCategoryController@store')->name('sub-category.store');
        Route::get('/sub-category/{id}/Edit', 'backend\SubCategoryController@show')->name('sub-category.edit');
        Route::post('/sub-Category/erase', 'backend\SubCategoryController@destroy')->name('sub-category.destroy');
        Route::post('/sub-category/update', 'backend\SubCategoryController@update')->name('sub-category.update');

        /* Sub-Category End */

        /* Product Add */

        Route::get('/product', 'backend\ProductController@index')->name('product.add');
        Route::get('/product/view', 'backend\ProductController@viewproduct')->name('product.view');
        Route::post('/product/add', 'backend\ProductController@store')->name('product.store');
        Route::get('/product/{id}/Show', 'backend\ProductController@show')->name('product.edit');
        Route::post('/Product/erase', 'backend\ProductController@destroy')->name('product.destroy');
        Route::post('/product/update', 'backend\ProductController@update')->name('product.update');
        Route::post('/status-update', 'backend\ProductController@status_update')->name('product.statusupdate');
        Route::post('/sub-cat-search','backend\ProductController@SearchSubcategory')->name('product.subcategory');

        /* Product End */

        /* Karat Start */

        Route::get('/karat', 'backend\KaratController@index')->name('karat.add');
        Route::post('/karat/add', 'backend\KaratController@store')->name('karat.store');
        Route::get('/karat/{id}/Show', 'backend\KaratController@show')->name('karat.edit');
        Route::post('/Karat/erase', 'backend\KaratController@destroy')->name('karat.destroy');
        Route::post('/karat/update', 'backend\KaratController@update')->name('karat.update');

        /* Karat End */

        /* Gold Type Start */

        Route::get('/GoldType', 'backend\GoldTypeController@index')->name('goldtype.add');
        Route::post('/GoldType/add', 'backend\GoldTypeController@store')->name('goldtype.store');
        Route::get('/GoldType/{id}/Show', 'backend\GoldTypeController@show')->name('goldtype.edit');
        Route::post('/GoldType/erase', 'backend\GoldTypeController@destroy')->name('goldtype.destroy');
        Route::post('/GoldType/update', 'backend\GoldTypeController@update')->name('goldtype.update');

        /* Gold Type End */

        /* Color Start */

        Route::get('/color', 'backend\ColorController@index')->name('color.add');
        Route::post('/color/add', 'backend\ColorController@store')->name('color.store');
        Route::get('/color/{id}/Show', 'backend\ColorController@show')->name('color.edit');
        Route::post('/color/erase', 'backend\ColorController@destroy')->name('color.destroy');
        Route::post('/color/update', 'backend\ColorController@update')->name('color.update');

        /* Color End */

        /* weight Start */

        Route::get('/weight', 'backend\WeightController@index')->name('weight.add');
        Route::post('/weight/add', 'backend\WeightController@store')->name('weight.store');
        Route::get('/weight/{id}/Show', 'backend\WeightController@show')->name('weight.edit');
        Route::post('/weight/erase', 'backend\WeightController@destroy')->name('weight.destroy');
        Route::post('/weight/update', 'backend\WeightController@update')->name('weight.update');

        /* weight End */

        /* Color Start */

        Route::get('/origin', 'backend\OriginController@index')->name('origin.add');
        Route::post('/origin/add', 'backend\OriginController@store')->name('origin.store');
        Route::get('/origin/{id}/Show', 'backend\OriginController@show')->name('origin.edit');
        Route::post('/Origin/erase', 'backend\OriginController@destroy')->name('origin.destroy');
        Route::post('/origin/update', 'backend\OriginController@update')->name('origin.update');

        /* Color End */

        /* Product Size Start */

        Route::get('/size', 'backend\SizeController@index')->name('size.add');
        Route::post('/add', 'backend\SizeController@store')->name('size.store');
        Route::get('/size/{id}/Show', 'backend\SizeController@show')->name('size.edit');
        Route::post('/Size/erase', 'backend\SizeController@destroy')->name('size.destroy');
        Route::post('/size/update', 'backend\SizeController@update')->name('size.update');

        /* Product Size End */
    });

      //  });
        /*demo cart start */
        Route::get('/New-demo-add','DemoCartctrl@index')->name('democart.index');
        Route::post('/New-cart-add','DemoCartctrl@AddToCart')->name('democart.add');
        Route::post('/cart-remove','DemoCartctrl@CartsRemove')->name('democart.remove');
        Route::post('/cart-update','DemoCartctrl@CartsUpdate')->name('democart.update');
        Route::get('/carts','DemoCartctrl@Carts')->name('democart.carts');
        /*demo cart end */


        Route::prefix('PurchaseMangement')->group(function(){
            Route::get('/New-Purchase','backend\PurchaseController@index')->name('purchase.add');
            Route::get('/view-Purchase','backend\PurchaseController@viewpurchase')->name('purchase.view');
            Route::post('/product-find','backend\PurchaseController@ProductFind')->name('product.find');
            Route::post('/product-add-cart','backend\PurchaseController@ProductAddCart')->name('purchase.addCart');
            Route::get('/product-cart-item','backend\PurchaseController@Carts')->name('purchase.carts');
            Route::post('/product-cart-remove','backend\PurchaseController@CartsRemove')->name('purchase.cart-remove');
            Route::post('/product-cart-edit','backend\PurchaseController@CartFind')->name('purchase.cart-edit');
            Route::post('/product-cart-update','backend\PurchaseController@CartUpdate')->name('purchase.UpdateCart');
            Route::post('/purchase-save','backend\PurchaseController@store')->name('purchase.save');
            Route::get('/Invoice-View','backend\InvoiceController@index')->name('invoice.view');

            Route::get('/Invoice-Visit/{id}','backend\InvoiceController@InvoiceDetails')->name('invoice.print');
            Route::post('/Invoice-Confirmation','backend\InvoiceController@InvoiceConfirm')->name('invoice.confirmation');
            Route::get('/Invoice-due','backend\InvoiceController@InvoiceDue')->name('invoice.due');
            Route::get('/sale-Invoice-due','backend\InvoiceController@Sale_Invoice_Due')->name('sale.invoice.due');

            Route::post('/Invoice-duepayment','backend\InvoiceController@InvoiceDuePayment')->name('invoice.duepayment');
            Route::post('/Invoice-change-date','backend\InvoiceController@changepaymentdate')->name('invoice.changedate');

        });

        Route::prefix('Stock')->group(function(){
            Route::get('/all-stock','backend\StockController@stock')->name('stock.view');

            Route::get('/stock-info','backend\StockController@viewstock')->name('stock.all');
            
            Route::post('/search-query','backend\StockController@SearchStock')->name('stock.search');

            Route::get('/stock-in','backend\StockController@stockin')->name('stock.in');
            Route::post('/stock-in-save','backend\StockController@instocksave')->name('instock.save');


            Route::get('/stock-out','backend\StockController@index')->name('stock.out');
            Route::post('/stock-out-save','backend\StockController@store')->name('stockout.store');

            Route::get('/outstock-view','backend\StockController@gold_maker_view')->name('stockout.view');
            Route::get('/instock-view','backend\StockController@instockview')->name('stock.inview');
            Route::get('/stock-barcode-list','backend\StockController@stock_barcode')->name('stock.barcodelist');

            Route::post('/stock-search','backend\StockController@BarcodeSearch')->name('stock.barcode');
            Route::post('/stock-multiple-search','backend\StockController@BarcodeSerialSearch')->name('stock.barcode-serial');

            /* Out Stock Start */

            Route::post('/Gold-For-GoldMaker','backend\StockController@GoldToGoldMaker')->name('stockout.goldmakergold');
            Route::get('/Invoice-stockout/{id}','backend\StockController@OutStockInvoice')->name('OutStock.invoice');


        });

        Route::prefix('SuplierManagement')->group(function(){
            Route::get('/Add-supplier','backend\SupplierController@index')->name('supplier.add');
            Route::post('/store-supplier','backend\SupplierController@store')->name('supplier.store');
            Route::post('/supplier-update','backend\SupplierController@update')->name('supplier.update');
            Route::post('/supplier/erase','backend\SupplierController@destroy')->name('supplier.delete');
            Route::post('/supplier-search','backend\SupplierController@suppliersearch')->name('supplier.search');
            Route::post('/subcategory-search','backend\SupplierController@SearchSubcategory')->name('purchase.subcategory');
        });

        Route::prefix('Mahajan')->group(function(){
            Route::get('/Add-mahajan','backend\MahajanController@index')->name('mahajan.add');
            Route::post('/store-mahajan','backend\MahajanController@store')->name('mahajan.store');
            Route::post('/mahajan-update','backend\MahajanController@update')->name('mahajan.update');
            Route::post('/mahajan/erase','backend\MahajanController@destroy')->name('mahajan.delete');
            Route::post('/mahajan-search','backend\MahajanController@suppliersearch')->name('mahajan.search');
            Route::post('/mahajan-subcategory','backend\MahajanController@SearchSubcategory')->name('mahajan.subcategory');
        });

        Route::prefix('Mortgage')->group(function(){
            Route::get('/add-mortgage','backend\MortgageController@index')->name('mortgage.add');
            Route::post('/mortgage-add-cart','backend\MortgageController@MortgageAddCart')->name('mortgage.addCart');
            Route::get('/mortgage-cart-item','backend\MortgageController@Carts')->name('mortgage.carts');
            Route::post('/mortgage-cart-remove','backend\MortgageController@CartsRemove')->name('mortgage.cart-remove');
            Route::post('/mortgage-save','backend\MortgageController@store')->name('mortgage.save');
            Route::get('/view-mortgage','backend\MortgageController@view_mortgage')->name('mortgage.view');
            Route::get('/Mortgage-Invoice-due','backend\MortgageController@MortgageInvoiceDue')->name('mortgage.invoice.due');
            Route::get('/mortgage-barcode','backend\MortgageController@barcodelist')->name('mortgage.barcodelist');

            /*send to mahajan start*/
            Route::get('/send-to-mahajan-add','backend\MortgageController@send_to_mahajan')->name('sendtomahajan.add');
            Route::get('/send-to-mahajan-view/{id}','backend\MortgageController@send_to_mahajan_view')->name('sendtomahajan.view');
            Route::post('/send-to-mahajan-store','backend\MortgageController@send_to_mahajan_store')->name('sendtomahajan.store');
            Route::post('/send-to-mahajan-update','backend\MortgageController@send_to_mahajan_update')->name('sendtomahajan.update');
            /*send to mahajan end*/

            /*receive from mahajan start*/
            Route::get('/receive-mahajan-add','backend\MortgageController@receive_from_mahajan')->name('receivemahajan.add');
            Route::post('/mortgage-info','backend\MortgageController@get_mortgage_info')->name('get_mortgage_info');
            Route::post('/receive-from-mahajan-store','backend\MortgageController@receive_from_mahajan_store')->name('receive_from_mahajan_save');
            /*receive from mahajan end*/
            /*mortgage interest starts here*/
            Route::get('pay-to-mahajan','backend\MortgageController@pay_int_to_mahajan')->name('paytomahajan.add');
            Route::get('receive_int_from_customer','backend\MortgageController@rcv_int_customer')->name('receive_int_customer.add');
            Route::post('/pay_interest_to_mahajan_save','backend\MortgageController@pay_int_to_mahajan_store')->name('pay_int_to_mahajan_save');
            Route::post('/rcv_int_cust_save','backend\MortgageController@rcv_int_cust_store')->name('rcv_int_save');
            Route::post('/mortgage-invoice','backend\MortgageController@get_mortgage_invoice')->name('get_mortgage_invoice');
            /*mortgage interest ends here*/
        });


         Route::prefix('localcost')->group(function(){
            Route::get('/add-cost','backend\LocalCostController@index')->name('localcost.add');
            Route::post('/store-cost','backend\LocalCostController@store')->name('localcost.store');
            Route::post('/erase','backend\LocalCostController@destroy')->name('localcost.erase');
            Route::get('/costing-history','backend\LocalCostController@history')->name('localcost.history');
            Route::post('/search-costing-history','backend\LocalCostController@search')->name('localcost.search');

            /* cost type store start */

             Route::get('/store-cost-type','backend\CostTypeController@index')->name('localcost.typestore');
             Route::post('/save-cost-type','backend\CostTypeController@store')->name('localcost.storetype');

             Route::get('/edit-save-cost-type/{id}','backend\CostTypeController@edit')->name('localcost.edit');
             Route::post('/update-cost-type','backend\CostTypeController@update')->name('localcost.updatetype');






        });

        


        Route::prefix('CustomerManagement')->group(function(){
            Route::get('/Add-customer','backend\CustomerController@index')->name('customer.add');
            Route::post('/store-customer','backend\CustomerController@store')->name('customer.store');
            Route::post('/customer-update','backend\CustomerController@update')->name('customer.update');
            Route::post('/Customer/erase','backend\CustomerController@destroy')->name('customer.delete');
            Route::post('/customer-search','backend\CustomerController@customersearch')->name('customer.search');
        });


        Route::prefix('SaleManagement')->group(function(){
            Route::get('/barcode-list','backend\SaleController@barcodelist')->name('sale.barcodelist');
            Route::get('/add-sale','backend\SaleController@index')->name('sale.view');
            Route::get('/sale-report','backend\SaleController@viewsale')->name('sale.salesummary');
            Route::post('/sale-search','backend\SaleController@BarcodeSearch')->name('sale.barcode');
            Route::post('/get-weight-barcode','backend\SaleController@getWeightByBarcode')->name('getWeight');
            Route::post('/sale-multiple-search','backend\SaleController@BarcodeSerialSearch')->name('sale.barcode-serial');
            Route::get('/sale-find','backend\SaleController@SaleCart')->name('sale.find');
            Route::post('/sale-remove','backend\SaleController@SaleCartRemove')->name('sale.remove');
            Route::post('/sale-save','backend\SaleController@store')->name('sale.store');

            /* Sale Cart Update Start */
            Route::post('/sale-cart-update','backend\SaleController@salecartupdate')->name('salecart.update');

            /* Sale Cart Update End */
            Route::get('/sale-invoice/{id}','backend\SaleController@SaleInvoice')->name('sale.invoice');

        });


        Route::prefix('GoldMakerManagement')->group(function(){
            Route::get('/add-goldmaker','backend\GoldMakerController@index')->name('goldmaker.add');
            Route::post('/store-goldmaker','backend\GoldMakerController@store')->name('goldmaker.store');
            Route::post('/goldmaker-update','backend\GoldMakerController@update')->name('goldmaker.update');
            Route::post('/GoldMaker/erase','backend\GoldMakerController@destroy')->name('goldmaker.delete');
            Route::post('/goldmaker-search','backend\GoldMakerController@goldmakersearch')->name('goldmaker.search');
            Route::get('/profile-grid-gold-maker','backend\GoldMakerController@gold_maker_grid')->name('goldmaker.profiles');
            Route::get('/invoice-goldmaker/{id}','backend\GoldMakerController@gold_maker_invoice')->name('goldmaker.invoice');
            /* new product setup start */
            Route::post('/barcode-store','backend\GoldMakerController@barcodelist')->name('goldmaker.barcodelist');
            Route::get('/barcode-list','backend\GoldMakerController@barcodelist_fetch')->name('goldmaker.listofbarcode');
            Route::post('/barcode-remove','backend\GoldMakerController@coderemove')->name('goldmaker.barcodelistremove');
            /* new product setup end*/
        });


        /* Employee Management */

        Route::prefix('Employee')->group(function() {
            Route::get('/add','backend\EmployeeController@index')->name('employee.add');
            Route::post('/save','backend\EmployeeController@store')->name('employee.store');
            Route::get('/edit/{id}','backend\EmployeeController@show')->name('employee.edit');
            Route::post('/update','backend\EmployeeController@update')->name('employee.update');
            Route::post('/erase','backend\EmployeeController@destroy')->name('employee.destroy');
        });


        Route::prefix('Salary')->group(function() {
            Route::get('/add','backend\SalaryController@index')->name('salary.add');
            Route::post('/save','backend\SalaryController@store')->name('salary.store');
            Route::get('/history','backend\SalaryController@history')->name('salary.history');
            Route::post('/search-salary','backend\SalaryController@search')->name('salary.search');
        });


        Route::prefix('MoneySummary')->group(function() {
            Route::get('/receive-money','backend\MoneySummaryController@index')->name('moneysummary.receiveadd');
            Route::post('/store-receive-money','backend\MoneySummaryController@store')->name('moneysummary.store');
            Route::get('/expense-money','backend\MoneySummaryController@CostView')->name('moneysummary.costmoney');
            Route::post('/store-expense-money','backend\MoneySummaryController@storeExpense')->name('expensemoney.store');

            /* Report of Income Start */
            Route::get('/income-report','backend\MoneySummaryController@IncomeReport')->name('moneysummary.incomereport');
            Route::get('/expense-report','backend\MoneySummaryController@ExpenseReport')->name('moneysummary.expensereport');

            Route::post('/search-income-report','backend\MoneySummaryController@SearchIncome')->name('incomereport.search');
            /* Report of Income End */


        });


        /* Size End */
    });
    /* admin middleware end */

    /* local user middleware start */
    Route::group(['middleware'=>['user']],function(){
       // Route::get('/home', 'backend\AdminController@')->name('home');
        Route::get('/user',function(){
            return view('home');
        });
    });
    /* local user middleware end */

    Route::get('/alluser',function(){
        return view('home');
    });
});

