@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add New Produced Product</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Produced Product</li>
                </ol>
            </div>
            <div id="page-content">

                <!-- Contact Toolbar -->
                <!---------------------------------->

                <!---------------------------------->


                <div class="row">
                    {{--loop starts here--}}
                     @foreach($goldmakers as $gm)
                    <div class="col-sm-4 col-md-3">
                        <div class="panel pos-rel">
                            <div class="pad-all text-center">
                                <div class="widget-control">
                                    <a href="#" class="add-tooltip btn btn-trans" data-original-title="Favorite"><span
                                                class="favorite-color"><i class="demo-psi-star icon-lg"></i></span></a>
                                    <div class="btn-group dropdown">
                                        <a href="#" class="dropdown-toggle btn btn-trans" data-toggle="dropdown"
                                           aria-expanded="false"><i class="demo-psi-dot-vertical icon-lg"></i></a>
                                        <ul class="dropdown-menu dropdown-menu-right" style="">
                                            <li><a href="#"><i class="icon-lg icon-fw demo-psi-pen-5"></i> Edit</a></li>
                                            <li><a href="#"><i class="icon-lg icon-fw demo-pli-recycling"></i>
                                                    Remove</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="icon-lg icon-fw demo-pli-mail"></i> Send a Message</a>
                                            </li>
                                            <li><a href="#"><i class="icon-lg icon-fw demo-pli-calendar-4"></i> View
                                                    Details</a></li>
                                            <li><a href="#"><i class="icon-lg icon-fw demo-pli-lock-user"></i> Lock</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <a href="{{route('goldmaker.invoice',$gm->id)}}">
                                    <img alt="Profile Picture" class="img-lg img-circle mar-ver"
                                         src="{{ ($gm->image)? url('public/admin/goldmaker/'.$gm->image): url('public/admin/goldmaker/user.png') }}" style="height: 100px;width: 100px;">
                                    <p class="text-lg text-semibold mar-no text-main">{{$gm->name}}</p>
                                    <p class="text-md">Gold Maker</p>
                                    <p class="text-md">{{$gm->address}}</p>
                                    <p class="text-md">{{$gm->phone}}</p>
                                </a>
                                <div class="pad-top btn-groups">
                                    <a href="#" class="btn btn-icon demo-pli-facebook icon-lg add-tooltip"
                                       data-original-title="Facebook" data-container="body"></a>
                                    <a href="#" class="btn btn-icon demo-pli-twitter icon-lg add-tooltip"
                                       data-original-title="Twitter" data-container="body"></a>
                                    <a href="#" class="btn btn-icon demo-pli-google-plus icon-lg add-tooltip"
                                       data-original-title="Google+" data-container="body"></a>
                                    <a href="#" class="btn btn-icon demo-pli-instagram icon-lg add-tooltip"
                                       data-original-title="Instagram" data-container="body"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                    {{--loop ends here--}}

                </div>
            </div>
        </div><!----------------end of page-content------------------>
    </div>


@endsection