@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Gold Maker</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Brand</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Brand</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    {{ Form::open(['route'=>'goldmaker.store','method'=>'post','id'=>'GoldMakerForm','enctype'=>'Multipart/form-data']) }}

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::hidden('id',null,['class'=>'form-control','id'=>'id']) }}
                                        {{ Form::label('GoldMakername','Gold Maker Name : ',['class'=>'control-label'])}}
                                        {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Ex: Gold Maker Name','id'=>'name'])}}
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('phone') ? 'has-error' : ''}}">

                                        {{ Form::label('GoldMakerphone','Gold Maker Phone : ',['class'=>'control-label'])}}
                                        {{ Form::text('phone',old('phone'),['class'=>'form-control','placeholder'=>'Ex: Gold Maker Name','id'=>'phone'])}}
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>



                                    <div class="col-lg-12 col-sm-12 {{$errors->has('goldmakerimage') ? 'has-error' : ''}}">

                                        {{ Form::label('GoldMakerphone','Gold Maker Image : ',['class'=>'control-label'])}}
                                        {{ Form::file('goldmakerimage',['class'=>'form-control'])}}
                                        @if ($errors->has('goldmakerimage'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('goldmakerimage') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        {{ Form::label('old Image','GoldMaker OLD IMAGE')}}
                                        <img style="height: 80px;width: 80px;" id="oldimage">
                                    </div>



                                    <div class="col-lg-12 col-sm-12 {{$errors->has('address') ? 'has-error' : ''}}">

                                        {{ Form::label('GoldMakeraddress','Gold Maker Address : ',['class'=>'control-label'])}}
                                        {{ Form::textarea('address',null,['class'=>'form-control','placeholder'=>'Ex: Gold Maker Address','id'=>'address'])}}
                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif

                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('Save Gold Maker',['type'=>'submit','id'=>'saveGoldMaker','class'=>'btn btn-primary']) }}

                                        @if($errors->all())
                                            {{ Form::hidden('canceledit',"Cancel Edit",['type'=>'reset','id'=>'cancel','class'=>'btn btn-danger']) }}
                                        @endif

                                        {{ Form::hidden('canceledit',"Cancel Edit",['type'=>'reset','id'=>'cancel','class'=>'btn btn-danger']) }}

                                    </div>
                                    {{ Form::close() }}
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                    <table class="table table-bordered table-striped" id="brandTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Phone </th>
                                            <th>Image</th>
                                            <th>Address</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                            @foreach($GoldMakers as $info)
                                                <tr id="rowid{{$info->id}}" class="abcd">
                                                    <td>{{++$i}}</td>
                                                    <td id="name{{ $info->id }}" data-id="{{ $info->name }}">{{$info->name}}</td>
                                                    <td id="phone{{ $info->id }}" data-id="{{ $info->phone }}">{{$info->phone}}</td>
                                                    <td id="image{{ $info->id }}" data-id="{{ $info->image }}">
                                                        <img src="{{ ($info->image)? url('public/admin/goldmaker/'.$info->image): url('public/admin/goldmaker/user.png') }}" style="height: 70px;width: 70px;" class="img-responsive">
                                                    </td>
                                                    <td id="address{{ $info->id }}" data-id="{{ $info->address }}">
                                                        @if(!empty($info->address))
                                                            {{ $info->address }}
                                                        @else
                                                            <p class="text-center">no address found</p>
                                                        @endif


                                                    </td>
                                                    <td>
                                                        <a class="btn btn-sm btn-info edit" data-id="{{ $info->id }}"><i class="demo-pli-pen-5"></i></a> ||
                                                        <button class="btn btn-sm btn-danger erase" data-id="{{$info->id}}" data-url="{{url('GoldMakerManagement/GoldMaker/erase')}}"><i class="demo-pli-trash"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#brandTable').DataTable();
            $("#cancel").hide();
        });

        /* edit request start */
        $(document).on('click','.edit',function(){
            $("#cancel").show(200);

            $("#cancel").attr("type","button");
            var id = $(this).attr('data-id');
            var name = $("#name"+id).attr('data-id');
            var phone = $("#phone"+id).attr('data-id');
            var address = $("#address"+id).attr('data-id');
            var route = '{{ route("goldmaker.update") }}';
            var img = $("#image"+id).attr('data-id');
            $("#name").val(name);
            $("#phone").val(phone);
            $("#address").val(address);
            $("#id").val(id);
            $('#GoldMakerForm').attr('action',route);
            $("#saveGold Maker").text("Update Gold Maker");
            $("#saveGold Maker").attr('class','btn btn-info');
            $("#cancel").attr('type','reset');

            $("#oldimage").attr('src',"{{url('public/admin/goldmaker')}}"+"/"+img);
        });

        $("#cancel").click(function(){
            var route = '{{ route("goldmaker.add") }}';
            $("#GoldMakerForm").attr('action',route);
            $("#id").val("");
            $("#saveGold Maker").text("Add Gold Maker");
            $("#saveGold Maker").attr('class','btn btn-primary');
            $(this).hide(299)
        });
        /* edit request end */
    </script>

@endsection