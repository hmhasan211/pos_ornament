@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
            </div>


            <!--Page content-->
            <!--===================================================-->
            <div id="page-content">

                <div class="panel">
                    <div class="panel-body">
                        <div class="invoice-masthead">
                            <div class="invoice-text">
                                <h3 class="h3 text-uppercase text-primary hidden-print">Gold Maker's Profile</h3>
                            </div>
                            <div class="invoice-brand" style="white-space:nowrap">
                                <div class="invoice-logo">
                                    <img src="{{asset('public/admin/img/alhamid.png')}}">
                                </div>
                            </div>
                        </div>

                        <div class="invoice-bill row">
                            <div class="col-sm-6 text-xs-center">
                                @if(count($instocks)>0)
                                <address>
                                    <img alt="Profile Picture" class="img-lg img-circle pull-left"
                                         src="{{ ($instocks[0]->gold_maker->image)? url('public/admin/goldmaker/'.$instocks[0]->gold_maker->image): url('public/admin/goldmaker/user.png') }}"
                                         style="height: 100px;width: 100px; margin-right: 10px">
                                    <span class="">
					                    <p><strong>{{$instocks[0]->gold_maker->name}}</strong></p>
                                        <p>{{$instocks[0]->gold_maker->address}}</p>
                                        <p>{{$instocks[0]->gold_maker->phone}}</p>
                                    </span>
                                </address>
                                    @endif
                            </div>
                            <div class="col-sm-6 text-xs-center">
                                <table class="invoice-details">
                                    <tbody>
                                    <tr>
                                        <td class="text-main text-bold">Invoice #</td>
                                        <td class="text-right text-info text-bold">AH-{{rand()}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-main text-bold">Order Status</td>
                                        <td class="text-right"><span class="badge badge-success">Complete</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-main text-bold">Billing Date</td>
                                        <td class="text-right">{{now()}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <hr class="new-section-sm bord-no">

                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 table-responsive">
                                <table class="table table-bordered invoice-summary data-table">
                                    <thead>
                                    <h4 class="text-center">Gold In From GoldMaker</h4>
                                    <tr class="bg-trans-dark">
                                        <th class="text-uppercase">Date</th>
                                        <th class="text-uppercase">Product Name</th>
                                        <th class="min-col text-center text-uppercase">Vori</th>
                                        <th class="min-col text-center text-uppercase">Ana</th>
                                        <th class="min-col text-right text-uppercase">Roti</th>
                                        <th class="min-col text-right text-uppercase">MiliRoti</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{--Defining var start--}}
                                    <?php

                                        $vorigram = 0;
                                        $anagram = 0;
                                        $rotigram = 0;
                                        $milirotigram = 0;
                                    ?>

                                    {{--Defining var End--}}
                                    @foreach($instocks as $instock)
                                        @if(count($instocks)>0)

                                            @if ($instock->vori)
                                                @php
                                                    $vorigram += $instock->vori * 11.664;
                                                @endphp
                                            @endif
                                            @if ($instock->ana)
                                                @php
                                                    $anagram += $instock->ana * 0.729;
                                                @endphp
                                            @endif
                                            @if ($instock->roti)
                                                @php
                                                $rotigram += $instock->roti * 0.1215;
                                                @endphp
                                            @endif
                                            @if ($instock->miliroti)
                                                @php
                                                $milirotigram += $instock->miliroti * 0.01215;
                                                @endphp

                                            @endif

                                        <tr>
                                            <td>
                                                <strong>{{$instock->created_at}}</strong>
                                            </td>
                                            <td class="text-center">{{$instock->product->product_name}}</td>
                                            <td class="text-center">{{$instock->vori}}</td>
                                            <td class="text-center">{{$instock->ana}}</td>
                                            <td class="text-right">{{$instock->roti}}</td>
                                            <td class="text-right">{{$instock->miliroti}}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    <?php $total_instock = $vorigram + $anagram + $rotigram + $milirotigram ?>

                                    </tbody>
                                </table>
                            </div>

                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 table-responsive">
                                <table class="table table-bordered invoice-summary data-table">
                                    <thead>
                                    <h4 class="text-center">Gold Out to GoldMaker</h4>
                                    <tr class="bg-trans-dark">
                                        <th class="text-uppercase">Date</th>
                                        <th class="text-uppercase">Product Name</th>
                                        <th class="min-col text-center text-uppercase">Vori</th>
                                        <th class="min-col text-center text-uppercase">Ana</th>
                                        <th class="min-col text-right text-uppercase">Roti</th>
                                        <th class="min-col text-right text-uppercase">MiliRoti</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $out_vori = 0;
                                        $out_ana = 0;
                                        $out_roti = 0;
                                        $out_miliroti = 0;
                                        $out_vorigram = 0;
                                        $out_anagram = 0;
                                        $out_rotigram = 0;
                                        $out_milirotigram = 0;
                                        $st_vori = 0;
                                        $st_ana = 0;
                                        $st_roti = 0;
                                        $st_miliroti = 0;
                                    ?>
                                    @foreach($outstocks as $outstock)
                                        @if($outstock->stockin)

                                            @php
                                            $st_vori=$outstock->stockin->vori;
                                            $st_ana=$outstock->stockin->ana;
                                            $st_roti=$outstock->stockin->roti;
                                            $st_miliroti=$outstock->stockin->roti;
                                            @endphp

                                        @else
                                            @php
                                            $st_vori=$outstock->instock->vori;
                                            $st_ana=$outstock->instock->ana;
                                            $st_roti=$outstock->instock->roti;
                                            $st_miliroti=$outstock->instock->roti;
                                            @endphp
                                        @endif
                                        @if(count($outstocks)>0)

                                            @if ($st_vori)
                                                @php
                                                    $out_vorigram += ($st_vori * 11.664);
                                                @endphp

                                            @endif
                                            @if ($st_ana)
                                                @php
                                                    $out_anagram += ($st_ana * 0.729);
                                                @endphp
                                            @endif
                                            @if ($st_roti)
                                                @php
                                                    $out_rotigram += ($st_roti * 0.1215);
                                                @endphp
                                            @endif
                                            @if ($st_miliroti)
                                                @php
                                                    $out_milirotigram += ($st_miliroti * 0.01215);
                                                @endphp

                                            @endif
                                            <tr>
                                                <td>
                                                    <strong>{{$outstock->created_at}}</strong>
                                                </td>
                                                <td class="text-center">{{$outstock->product->product_name}}</td>
                                                <td class="text-center">{{$st_vori}}</td>
                                                <td class="text-center">{{$st_ana}}</td>
                                                <td class="text-right">{{$st_roti}}</td>
                                                <td class="text-right">{{$st_miliroti}}</td>

                                            </tr>

                                        @endif
                                    @endforeach

                                    <?php $total_outstock = $out_vorigram + $out_anagram + $out_rotigram + $out_milirotigram ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="clearfix">
                            <table class="table invoice-total">
                                <tbody>
                                <tr>
                                    <td><strong>Total Gold IN :</strong></td>
                                    <td>{{$total_instock}} <span class="text-info">gm</span></td>
                                </tr>
                                <tr>
                                    <td><strong>Total Gold Out :</strong></td>
                                    <td>{{$total_outstock}}<span class="text-info">gm</span></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>



                        <hr class="new-section-sm bord-no">


                    </div>
                </div>


            </div>
            <!--===================================================-->
            <!--End page content-->

        </div><!---------------end of content-container------------------>
    </div>

    <script>
        $(function(){
            $('.data-table').DataTable();

        });

    </script>

@endsection