@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">New STOCK-IN</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">STOCK-IN</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">STOCK-IN-VIEW </h3>
                            </div>
                            <div class="panel-body">
                                {{-- Product Name search Button --}}
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                                        {{-- Added Product View  --}}
                                        <span style="display: block;height: 40px;width: 100%;background: #fff;"></span>
                                        <div class="row">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>SL</th>

                                                    <th>Invoice ID</th>
                                                    <th>Barcode</th>
                                                    <th>Name</th>
                                                    <th>GoldMaker</th>


                                                    {{--<th>StockoutInvoice</th>--}}

                                                    <th>status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php  $i=0;  @endphp
                                                @foreach($instock as $stock)

                                                    <tr>
                                                        <td> {{ ++$i}} </td>

                                                        <td>
                                                            {{ $stock->invoice->invoice_no }}
                                                        </td>
                                                        <td>
                                                            @php
                                                                //$barcode = '\App\Model\Barcode'::where('gold_maker_id',$stock->invoice->gold_maker_id)->first();
                                                                $barcode = '\App\Model\Barcode'::where('instock_id',$stock->id)->first();
                                                                echo $barcode->barcode;
                                                            @endphp
                                                        </td>
                                                        <td> {{ $stock->Product->product_name }} </td>
                                                        <td> {{ $stock->invoice->GoldMaker->name }} </td>

                                                        {{--<td> {{ $stock->stock_out_invoice->invoice_no }} </td>--}}

                                                        <td>
                                                            @if($stock->status == 0)
                                                                <label class="label label-info"> Pending </label>
                                                            @else
                                                                <label class="label label-info"> Delivered </label>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        {{-- Price Weight , Weight-amount Row End From Here --}}

                                    </div>{{-- left div end--}}

                                </div>{{-- Right div end--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

    </script>
@endsection