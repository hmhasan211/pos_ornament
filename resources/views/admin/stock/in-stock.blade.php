@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add New Purchase</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Product</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">

                    <div class="panel">

                        <div class="panel-heading">
                            <h3 class="panel-title">Add New Purchase</h3>
                        </div>


                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                    <div class="row">
                                        {{ Form::hidden('id',null,['id'=>'pid']) }}
                                        <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('product_code') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product','Product Code : ',['class'=>'control-label'])}}
                                            {{ Form::select('product_code',$product_code,null,['class'=>'form-control product_code_select2','required','id'=>'product_code','multiple'])}}

                                        </div>
                                        <!--  PRODUCT NAME  -->
                                        <div class="col-lg-9 col-sm-9 col-xs-12 {{$errors->has('product_name') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product','Product Name ( পণ্যের নাম )   : ',['class'=>'control-label'])}}
                                            {{ Form::select('product_name',$product_name,null,['class'=>'form-control product_name_select2','required','id'=>'product_name','multiple'])}}
                                        </div>
                                        <!-- / PRODUCT NAME  -->
                                    </div> {{-- Product COde and Product Name Search Row End --}}
                                    <div class="row">
                                        <!--  PRODUCT Brand NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('brand_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('brand','Brand ( কোম্পানি ) : ',['class'=>'control-label'])}}
                                            {{ Form::text('brand_id',null,['readonly','class'=>'form-control','required','id'=>'brand_id'])}}

                                        </div>
                                        <!-- / PRODUCT Brand NAME  -->

                                        <!--  PRODUCT category_id NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('category_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('color','Category : ',['class'=>'control-label'])}}
                                            {{ Form::text('category_id',null,['readonly','class'=>'form-control','id'=>'category_id'])}}
                                        </div>
                                        <!-- / PRODUCT category_id NAME  -->

                                        <!--  PRODUCT subcategory_id NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('subcategory_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('subcategory','Sub-Category : ',['class'=>'control-label'])}}
                                            {{ Form::text('subcategory_id',null,['readonly','class'=>'form-control','required','id'=>'subcategory_id'])}}
                                        </div>
                                        <!-- / PRODUCT subcategory_id NAME  -->

                                    </div>{{-- Brand Category Sub-Category Row End here  --}}

                                    <div class="row">

                                        <!--  PRODUCT SIZE NAME  -->

                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('size_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product','Size ( আকার ): ',['class'=>'control-label'])}}
                                            {{ Form::text('size_id',null,['readonly','class'=>'form-control','required','id'=>'size_id'])}}

                                        </div>
                                        <!-- / PRODUCT SIZE NAME  -->

                                        <!--  PRODUCT color NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('color_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('color','Color ( রং ) : ',['class'=>'control-label'])}}

                                            {{ Form::text('color_id',null,['readonly','class'=>'form-control','required','id'=>'color_id'])}}
                                        </div>
                                        <!-- / PRODUCT color NAME  -->

                                        <!--  PRODUCT karat NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('karat_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('karat','Karat ( ক্যারেট ) : ',['class'=>'control-label'])}}
                                            {{ Form::text('karat_id',null,['readonly','class'=>'form-control','required','id'=>'karat_id'])}}

                                        </div>
                                    </div>{{-- Size Karat Color Row End Here --}}



                                    <div class="row">
                                        <!-- Origin Select Start -->

                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('origin_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>

                                            {{ Form::label('origin','Origin ( অরিজিন ) : ',['class'=>'control-label'])}}
                                            {{ Form::text('origin_id',null,['readonly','class'=>'form-control','required','id'=>'origin_id'])}}


                                        </div>
                                        <!-- Origin Select End -->

                                        <!-- Origin Select Start -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('gold_type_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>

                                            {{ Form::label('Gold Type','Gold Type : ',['class'=>'control-label'])}}
                                            {{ Form::text('gold_type_id',null,['readonly','class'=>'form-control','required','id'=>'gold_type_id'])}}


                                        </div>
                                        <!-- Origin Select End -->

                                        <!--  PRODUCT quantity   -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>

                                            {{ Form::label('productqty','Quantity : ',['class'=>'control-label'])}}
                                            {{Form::number('prdocut_qty',null,['id'=>'product_qty','required','class'=>'form-control','placeholder'=>'Ex: 10'])}}
                                        </div>
                                        <!-- / PRODUCT quantity   -->

                                    </div> {{-- Upper Row End here --}}

                                    {{-- Product Qty Price and Rate Here --}}

                                    <div class="row">

                                        {{-- vori start --}}
                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('Vori','Vori ( ভরি )  : ',['class'=>'control-label'])}}
                                            {{ Form::number('vori',0,['id'=>'vori','class'=>'form-control','min'=>0]) }}
                                        </div>
                                        {{-- vori end --}}


                                        {{-- Anaa start --}}
                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('anaa','Anaa ( আনা ) : ',['class'=>'control-label'])}}
                                             <select class="form-control" id="ana" name="anaa">
                                                <option value="0">আনা সিলেক্ট করুন</option>
                                                <option value="1" data-image="{{ url('public/fontimage/01.jpg') }}">১ আনা</option>
                                                <option value="2" data-image="{{ url('public/fontimage/02.jpg') }}">২ আনা</option>
                                                <option value="3" data-image="{{ url('public/fontimage/03.jpg') }}">৩ আনা</option>
                                                <option value="4" data-image="{{ url('public/fontimage/04.jpg') }}">৪ আনা</option>
                                                <option value="5" data-image="{{ url('public/fontimage/05.jpg') }}">৫ আনা</option>
                                                <option value="6" data-image="{{ url('public/fontimage/06.jpg') }}">৬ আনা</option>
                                                <option value="7" data-image="{{ url('public/fontimage/07.jpg') }}">৭ আনা</option>
                                                <option value="8" data-image="{{ url('public/fontimage/08.jpg') }}">৮ আনা</option>
                                                <option value="9" data-image="{{ url('public/fontimage/09.jpg') }}">৯ আনা</option>
                                                <option value="10" data-image="{{ url('public/fontimage/10.jpg') }}">১০আনা</option>
                                                <option value="11" data-image="{{ url('public/fontimage/11.jpg') }}">১১ আনা</option>
                                                <option value="12" data-image="{{ url('public/fontimage/12.jpg') }}">১২ আনা</option>
                                                <option value="13" data-image="{{ url('public/fontimage/13.jpg') }}">১৩আনা</option>
                                                <option value="14" data-image="{{ url('public/fontimage/14.jpg') }}">১৪ আনা</option>
                                                <option value="15" data-image="{{ url('public/fontimage/15.jpg') }}">
                                                    ১৫ আনা</option>
                                            </select>
                                        </div>
                                        {{-- Anaa end --}}

                                        {{-- Roti start --}}
                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('Roti','Roti ( রতি )  : ',['class'=>'control-label'])}}
                                            <select class="form-control" id="roti" name="roti">
                                                <option value="0">রতি সিলেক্ট করুন </option>
                                                <option value="1" data-image="{{ url('public/fontimage/roti/1.jpg') }}">
                                                    ১ রতি
                                                </option>
                                                <option value="2" data-image="{{ url('public/fontimage/roti/2.jpg') }}">
                                                    ২ রতি
                                                </option>
                                                <option value="3" data-image="{{ url('public/fontimage/roti/3.jpg') }}">
                                                    ৩ রতি
                                                </option>
                                                <option value="4" data-image="{{ url('public/fontimage/roti/4.jpg') }}">
                                                    ৪ রতি
                                                </option>
                                                <option value="5" data-image="{{ url('public/fontimage/roti/5.jpg') }}">
                                                    ৫ রতি
                                                </option>

                                            </select>
                                        </div>
                                        {{-- Roti end --}}

                                        {{--weight Amount start  --}}
                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('mili',' আধা রতি ( ৫ দিতে হবে )  : ',['class'=>'label-control']) }}
                                            <select class="form-control" id="miliroti" name="miliroti">
                                                <option value="0">মিলি রতি সিলেক্ট করুন </option>
                                                <option value="1" data-image="{{ url('public/fontimage/miliroti/1.jpg') }}">
                                                    ১ পয়েন্ট
                                                </option>
                                                <option value="2" data-image="{{ url('public/fontimage/miliroti/2.jpg') }}">
                                                    ২ পয়েন্ট
                                                </option>
                                                <option value="3" data-image="{{ url('public/fontimage/miliroti/3.jpg') }}">
                                                    ৩ পয়েন্ট
                                                </option>
                                                <option value="4" data-image="{{ url('public/fontimage/miliroti/4.jpg') }}">
                                                    ৪ পয়েন্ট
                                                </option>
                                                <option value="5" data-image="{{ url('public/fontimage/miliroti/5.jpg') }}">
                                                    ৫ পয়েন্ট
                                                </option>

                                                <option value="6" data-image="{{ url('public/fontimage/miliroti/6.jpg') }}">
                                                    ৬ পয়েন্ট
                                                </option>
                                                <option value="7" data-image="{{ url('public/fontimage/miliroti/7.jpg') }}">
                                                    ৭ পয়েন্ট
                                                </option>
                                                <option value="8" data-image="{{ url('public/fontimage/miliroti/8.jpg') }}">
                                                    ৮ পয়েন্ট
                                                </option>
                                                <option value="9" data-image="{{ url('public/fontimage/miliroti/9.jpg') }}">
                                                    ৯ পয়েন্ট
                                                </option>

                                            </select>
                                        </div>
                                    {{-- weight End--}}



                                    <!--  PRODUCT product_price  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product','দর/Rate : ',['class'=>'control-label'])}}
                                            {{Form::number('product_price',old('product_price'),['id'=>'product_price','required','class'=>'form-control','placeholder'=>'Ex: 5000 tk','min'=>1])}}
                                        </div>
                                        <!-- / PRODUCT product_price  -->

                                        <!--  PRODUCT Rate  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product','দাম/Price : ',['class'=>'control-label'])}}
                                            {{Form::number('product_price',0,['id'=>'product_rate','readonly','class'=>'form-control','placeholder'=>'Ex: 5000 tk','min'=>1])}}
                                        </div>
                                        <!-- / PRODUCT Rate NAME  -->






                                    </div>{{--  Rate Qty Price Row End--}}

                                    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        <div class="form-group">
                                            <button class="btn btn-info form-control" type="button" id="add_bucket">Add To Bucket</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 pull-right">
                                    <h1 class="text-center bg-primary">Bucket List</h1>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>নাম</th>
                                            <th>গ্রাম </th>
                                            <th>দর</th>
                                            <th>মোট</th>
                                            <th>মুছুন</th>
                                        </tr>
                                        </thead>
                                        <tbody id="all_cart_item">

                                        </tbody>
                                    </table>
                                    <hr>

                                    {{-- suplier information added start from here --}}

                                    {{ Form::open(['route'=>'instock.save','method'=>'post']) }}
                                    <div class="row">
                                        <h4 class="text-center bg-primary" style="padding: 8px 0">Gold Maker Information : </h4>
                                       {{-- <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                            {{ Form::label('outstock_invoice','Select Invoice',['class'=>'label-control']) }}
                                            {{ Form::select('outstock_invoice',$invoices,false,['id'=>'old_invoice_id','class'=>'form-control supplier_id_select2','multiple'=>'multiple']) }}
                                        </div>--}}
                                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                            {{ Form::label('searchsupplier','Search Gold-Maker',['class'=>'label-control']) }}
                                            {{ Form::select('goldmaker_id',$goldmaker,false,['id'=>'goldmaker_id','class'=>'form-control supplier_id_select2','multiple'=>'multiple']) }}
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12 {{ $errors->has('name') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('name',' Name : ',['class'=>'label-control']) }}
                                            {{ Form::text('name',null,['class'=>'form-control','id'=>'goldmaker_name','placeholder'=>'Ex. Mr.xyz']) }}

                                            @if($errors->has('name'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif

                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12 {{ $errors->has('phone') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('phone','Phone : ',['class'=>'label-control']) }}
                                            {{ Form::text('phone',null,['class'=>'form-control','placeholder'=>'Ex : 923584596','id'=>'goldmaker_phone']) }}
                                            @if($errors->has('phone'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-lg-12 col-sm-12 col-xs-12 {{ $errors->has('address') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('address','Address : ',['class'=>'label-control' ]) }}
                                            {{ Form::textarea('address',null,['id'=>'goldmaker_address','class'=>'form-control','placeholder'=>'New York City','rows'=>5,'cols'=>'10']) }}
                                            @if($errors->has('address'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-12 col-sm-12 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            <table class="table-hover table table-bordered table-responsive">
                                                <tbody>



                                                {{-- Total amount of cart loop end from here --}}
                                                <tr>
                                                    <td> <span class="btn btn-primary">Total Amount : </span>  </td>
                                                    <td>
                                                        {{ Form::number('total_amount',null,['class'=>'form-control','id'=>'total_amount','placeholder'=>'100 tk','readonly']) }}
                                                    </td>
                                                </tr>

                                                </tbody>

                                            </table>
                                        </div>


                                        <div class="col-lg-12 col-sm-12 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>

                                            <button type="submit" class="btn btn-info btn-block">PRODUCT RE-STORE</button>
                                        </div>
                                    </div>

                                    {{-- Suplier information added end from here --}}

                                </div>
                            </div>
                        </div><!--  Panel Body Close -->

                    </div><!-- Panel Close -->

                </div><!-- Row Close -->
            </div><!-- Page Content Close -->
        </div>
    </div>



    <script>
        var global_price;
        var amount=0;
        var amount = "{{ Cart::total() }}";
        $(document).ready(function(){
            /* at first auto hide weight amount ... it will be show when weight value found either it will be hide */
            $("#ana").msDropdown();
            $("#roti").msDropdown();
            $("#miliroti").msDropdown();
            $("#tamount").val(amount);
            $("#weight_info").css("display","none");
            /* at first auto hide weight amount ... it will be show when weight value found either it will be hide */

            /* All added cart items showing with this functions */

            Carts();

            /* All added cart items showing with this functions */

            global_price = 0;
            product_code();
            $("#bkash_code").hide();
            $(".product_code_select2").select2({
                maximumSelectionLength: 1
            });

            $(".weight_id_select2").select2({ tags:true,maximumSelectionLength : 1});
            $(".product_name_select2").select2({maximumSelectionLength : 1});

            $(".supplier_id_select2").select2({tags: true ,maximumSelectionLength: 1});

        });

        /* when product code empty then automatic other field will be empty */



        /* suppliers search start */

        $(document).on('change','#goldmaker_id',function(){
            $("#goldmaker_name").val("");
            $("#goldmaker_phone").val("");
            $("#goldmaker_address").val("");


            var goldmaker_id = $(this).val();
            var token = "{{ csrf_token() }}";
            $.ajax({
                method:"post",
                url:"{{route('goldmaker.search')}}",
                data:{goldmaker_id:goldmaker_id,"_token":token},
                dataType:'json',
                success:function(response){
                    $("#goldmaker_name").val(response[0].name);
                    $("#goldmaker_phone").val(response[0].phone);
                    $("#goldmaker_address").val(response[0].address);
                    console.log("Response List : "+response[0].id);
                },
                error:function(err){console.log("Error List : "+err);}
            });

        });

        /* suppliers search end */

        function product_code(){
            var p_code = $("#product_code").val();
            if(p_code == ""){
                $('#product_name').val();
                $('#product_price').val();
                $('#product_local_name').val();
                $('#product_details').val();
                /* product price multple quantity equal Total Price */
                $("#tamount").val();
            }
        }

        /*
         ========== product information search with Product Code ===========
         */

        $(document).on('change','#product_code',function(){
            var product_code_id = $(this).val();
            var token = "{{ csrf_token() }}";
            if(product_code_id != ''){
                $.ajax({
                    method:"post",
                    url:"{{ route('product.find') }}",
                    data:{product_code_id: product_code_id,"_token":token},
                    dataType:"json",
                    success:function(response){

                        var parsedata = JSON.parse(response);

                        console.log(parsedata.brand);

                        $("#pid").val(parsedata.id);

                        $('.product_code_select2').select2({tags:true,maximumSelectionLength : 1});

                        global_price = parsedata.product_price;

                        $('#product_name').val(parsedata.id).attr('selected',true);

                        $('.product_name_select2').select2({tags:true,maximumSelectionLength : 1});


                        $('#product_price').val(parsedata.product_price);
                        $("#product_qty").val(1);

                        $("#size_id").val(parsedata.size);
                        $("#color_id").val(parsedata.color);
                        $("#karat_id").val(parsedata.karat);
                        $("#origin_id").val(parsedata.origin);
                        $("#gold_type_id").val(parsedata.goldtype);


                        $("#brand_id").val(parsedata.brand);
                        $("#category_id").val(parsedata.category);
                        $("#subcategory_id").val(parsedata.subcategorie);
                    },
                    error:function(err){
                        console.log(err);
                    }
                });
            }else{
                product_code();
            }
        });

        /*
         ========== product information search with Product Code ===========
         */

        /* if select product name */
        /*
         ========== product information search with Product Name ===========
         */

        $(document).on('change','#product_name',function(){
            var product_code_id = $(this).val();
            var token = "{{ csrf_token() }}";
            if(product_code_id != ''){
                $.ajax({
                    method:"post",
                    url:"{{ route('product.find') }}",
                    data:{product_code_id: product_code_id,"_token":token},
                    dataType:"json",
                    success:function(response){

                        var parsedata = JSON.parse(response);

                        console.log(parsedata.brand);

                        $("#pid").val(parsedata.id);

                        global_price = parsedata.product_price;

                        $('#product_name').val(parsedata.id);

                        $('#product_code').val(parsedata.id).attr('selected',true);
                        $('.product_code_select2').select2({tags:true,maximumSelectionLength : 1});
                        $('.product_name_select2').select2({tags:true,maximumSelectionLength : 1});
                        $('#product_price').val(parsedata.product_price);

                        $("#size_id").val(parsedata.size);
                        $("#color_id").val(parsedata.color);
                        $("#karat_id").val(parsedata.karat);
                        $("#origin_id").val(parsedata.origin);
                        $("#gold_type_id").val(parsedata.goldtype);

                        $("#brand_id").val(parsedata.brand);
                        $("#category_id").val(parsedata.category);
                        $("#subcategory_id").val(parsedata.subcategorie);
                        $("#product_qty").val(1);

                        $("#vori").val(0);
                        $("#product_rate").val(0);
                        /* product price multple quantity equal Total Price */
                    },
                    error:function(err){
                        console.log(err);
                    }
                });
            }else{
                product_code();
            }
        });

        /*
         ========== product information search with Product Name ===========
         */

        /* Calculate according to Vori start */

        $(document).on('keyup change','#vori',function(){
            var vori = $(this).val();
            var ana = $("#ana").val();
            var roti =$("#roti").val();
            var mroti = $("#miliroti").val();
            var rate = $("#product_rate").val();
            var vori_price = $("#product_price").val();
            if(vori==0){
                $(this).addClass("bg-danger").val(0);
                $("#product_rate").val(0);
            }
            else{
                $(this).removeClass("bg-danger");
                if(vori > 0 && ana == 0 && roti == 0 && mroti == 0 ){
                    var total = Number(vori)*Number(vori_price);
                    $("#product_rate").val(total.toFixed(2));
                }else if(vori > 0 && ana > 0 && roti == 0 && mroti == 0 ) {
                    var per_ana_price = Number(vori_price)/16;
                    var total_ana = Number(vori)*16+Number(ana);
                    var total_ana_price = Number(per_ana_price) * Number(total_ana);
                    $("#product_rate").val(total_ana_price.toFixed(2));
                }else if(vori > 0 && ana > 0 && roti > 0 && mroti == 0){
                    $per_vori_roti_price = Number(vori_price)/(16*6);  /* per vori roti amount start */
                    $total_roti = Number(vori)*16*6+(Number(ana)*6)+Number(roti);
                    $total_roti_price = $total_roti*$per_vori_roti_price;
                    $("#product_rate").val($total_roti_price.toFixed(2));
                }
                else if(vori >0 && ana == 0 && roti > 0 && ana ==0  ){
                    $per_vori_roti_price = Number(vori_price)/96;  /* per vori roti amount start */
                    $total_roti = Number(vori)*96+Number(roti);
                    $total_roti_price = $total_roti*$per_vori_roti_price;
                    $("#product_rate").val($total_roti_price.toFixed(2));

                    console.log("Total Roti :"+$total_roti+ " Per Roti Price : "+$per_vori_roti_price+" Total Price : "+$total_roti_price);
                }
                else if(vori > 0 && ana ==0 && roti == 0 && mroti > 0){
                    $per_vori_miliroti_price = Number(vori_price)/960;  /* per vori roti amount start */
                    $total_roti = Number(vori)*960+Number(mroti);
                    $total_roti_price = $total_roti*$per_vori_miliroti_price;
                    $("#product_rate").val($total_roti_price.toFixed(2));
                    console.log("Total Mili Roti :"+$total_roti+ " Per Mili Roti Price : "+$per_vori_miliroti_price+" Total Price : "+$total_roti_price);
                }
                else{
                    $per_vori_roti_price = Number(vori_price)/960;  /* per vori roti amount start */
                    $total_mili_roti = (Number(vori)*96+Number(ana)*6+Number(roti))*10+"."+Number(mroti);
                    $total = $total_mili_roti*$per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));
                    console.log("Total Vori = "+vori+" Vori * Mili Roti = "+$total_mili_roti+" Per Mili Roti Price : "+$per_vori_roti_price+" Total Price : "+$total);
                }
            }
        });

        $(document).on('keyup change','#ana',function(){
            var ana = $(this).val();
            var vori = $("#vori").val();
            var roti =$("#roti").val();
            var mroti = $("#miliroti").val();
            var rate = $("#product_rate").val();
            var vori_price = $("#product_price").val();
            /*when vori,ana,roti empty */
            if(ana>16 || ana == 16){
                $(this).addClass('bg-danger').val(0);
                $("#product_rate").val(0);
            }else{
                $(this).removeClass('bg-danger');
                if(vori == 0 && ana > 0 && roti == 0 && mroti == 0 ){
                    var per_ana_price = Number(vori_price)/16;
                    var total = Number(per_ana_price)*Number(ana);
                    $("#product_rate").val(total.toFixed(2));

                }else if(vori > 0 && ana > 0 && roti == 0 && mroti == 0 ) {
                    var per_ana_price = Number(vori_price)/16;
                    var total_ana = Number(vori)*16+Number(ana);
                    var total_ana_price = Number(per_ana_price) * Number(total_ana);
                    var price = total_ana_price.toFixed(2);
                    $("#product_rate").val(price);
                    console.log("To Fix fire : "+price);

                }else if(vori > 0 && ana > 0 && roti > 0 && mroti == 0){
                    $per_vori_roti_price = Number(vori_price)/(16*6);  /* per vori roti amount start */
                    $total_roti = Number(vori)*96+(Number(ana)*6)+Number(roti);
                    $total_roti_price = $total_roti*$per_vori_roti_price;
                    $("#product_rate").val($total_roti_price.toFixed(2));

                }else if(vori == 0 && ana > 0 && roti > 0 && mroti > 0 ){
                    $per_vori_roti_price = vori_price/960;
                    $total_mili_roti = (Number(ana)*6+Number(roti))*10+ Number(mroti);
                    $total = $total_mili_roti*$per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));
                    console.log("Total Mili Roti : "+$total_mili_roti+" Total Per Mili Roti Price : "+$per_vori_roti_price+" Total Price according to Mili Roti : "+$total);

                }else if(vori == 0 && ana > 0 && roti > 0 && mroti == 0 ){

                    $per_vori_roti_price = Number(vori_price)/96;
                    $total_roti = (Number(ana)*6+Number(roti));
                    $total = $total_roti*$per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));
                    console.log("Total Mili Roti : "+$total_roti+" Total Per Mili Roti Price : "+$per_vori_roti_price+" Total Price according to Mili Roti : "+$total);
                }
                else{
                    $per_vori_roti_price = Number(vori_price)/960;  /* per vori roti amount start */
                    $total_mili_roti = (Number(vori)*96+Number(ana)*6+Number(roti))*10+"."+Number(mroti);
                    $total = $total_mili_roti*$per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));
                    console.log("Total Vori = "+vori+" Vori * Mili Roti = "+$total_mili_roti+" Per Mili Roti Price : "+$per_vori_roti_price+" Total Price : "+$total);
                }
            }
        });

        $(document).on('keyup change','#roti',function(){
            var roti = $(this).val();
            var vori = $("#vori").val();
            var ana =$("#ana").val();
            var mroti = $("#miliroti").val();
            var rate = $("#product_rate").val();
            var vori_price = $("#product_price").val();
            /*when vori,ana,roti empty */
            if(roti > 6 || roti == 6){
                $(this).addClass("bg-danger").val(0);
                $("#product_rate").val(0);
            }
            else{
                $(this).removeClass('bg-danger');

                if(vori == 0 && ana == 0 && roti > 0 && mroti == 0 ){
                    var per_ana_price = Number(vori_price)/96;
                    var total = Number(per_ana_price)*Number(roti);
                    $("#product_rate").val(total.toFixed(2));

                }else if(vori > 0 && ana == 0 && roti > 0 && mroti == 0 ) {
                    var per_roti_price = Number(vori_price)/96;
                    var total_roti = Number(vori)*96+Number(roti);
                    var total_roti_price = Number(per_roti_price) * Number(total_roti);
                    $("#product_rate").val(total_roti_price.toFixed(2));

                    console.log("Total Roti : "+total_roti+" Per Roti Price : "+per_roti_price+" Total Roti Price : "+total_roti_price+" To Fixed Product Price : "+total_roti_price.toFixed(2));

                }else if(vori > 0 && ana > 0 && roti > 0 && mroti == 0){
                    $per_vori_roti_price = Number(vori_price)/96;  /* per vori roti amount start */
                    $total_roti = Number(vori)*96+(Number(ana)*6)+Number(roti);
                    $total_roti_price = $total_roti*$per_vori_roti_price;
                    $("#product_rate").val($total_roti_price.toFixed(2));

                    console.log("Total Roti : "+$total_roti+" Per Roti Price : "+$per_vori_roti_price+" Total Roti Price : "+$total_roti_price);

                }else if(vori > 0 && ana == 0 && roti >0 && mroti >0){

                    $per_vori_roti_price = Number(vori_price)/960;  /* per vori Mili-roti amount start */
                    $total_roti = Number(vori)*960+Number(roti)*10+Number(mroti);
                    $total_roti_price = $total_roti*$per_vori_roti_price;
                    $("#product_rate").val($total_roti_price.toFixed(2));
                    console.log("Total Mili Roti : "+$total_roti+" Per Mili Roti Price : "+$per_vori_roti_price+" Total Mili Roti Price : "+$total_roti_price);
                }

                else if(vori == 0 && ana == 0 && roti >0 && mroti >0){
                    $per_vori_roti_price = Number(vori_price)/960;  /* per vori Mili-roti amount start */
                    $total_roti = Number(roti)*10+Number(mroti);
                    $total_roti_price = $total_roti*$per_vori_roti_price;
                    $("#product_rate").val($total_roti_price.toFixed(2));
                    console.log("Total Mili Roti : "+$total_roti+" Per Mili Roti Price : "+$per_vori_roti_price+" Total Mili Roti Price : "+$total_roti_price);
                }
                else{
                    $per_vori_roti_price = Number(vori_price)/960;  /* per vori roti amount start */
                    $total_mili_roti = (Number(vori)*96+Number(ana)*6+Number(roti))*10+Number(mroti);
                    $total = $total_mili_roti*$per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));
                    console.log("Total Vori = "+vori+" Vori * Mili Roti = "+$total_mili_roti+" Per Mili Roti Price : "+$per_vori_roti_price+" Total Price : "+$total);
                }
            }
        });





        $(document).on('keyup change','#miliroti',function(){
            var mroti = $(this).val();
            var vori = $("#vori").val();
            var ana =$("#ana").val();
            var roti = $("#roti").val();

            var rate = $("#product_rate").val();
            var vori_price = $("#product_price").val();
            /*when vori,ana,roti empty */
            if(mroti == 5){
                $(this).removeClass('bg-danger');
                if(vori == 0 && ana == 0 && roti == 0 && mroti > 0 ){
                    $per_vori_roti_price = Number(vori_price)/(16*6*10);  /* per vori roti amount start */
                    $total_mili_roti = (Number(vori)*16*6+(Number(ana)*6)+Number(roti))*10+Number(mroti);
                    $total = $total_mili_roti*$per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));

                }else if(vori == 0 && ana == 0 && roti > 0 && mroti > 0 ) {
                    $per_vori_roti_price = Number(vori_price)/(16*6*10);  /* per vori roti amount start */
                    $total_mili_roti = (Number(vori)*16*6+(Number(ana)*6)+Number(roti))*10+Number(mroti);
                    $total = $total_mili_roti*$per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));

                }else if(vori == 0 && ana > 0 && roti > 0 && mroti > 0){
                    $per_vori_roti_price = Number(vori_price)/(16*6*10);  /* per vori roti amount start */
                    $total_mili_roti = (Number(vori)*16*6+(Number(ana)*6)+Number(roti))*10+Number(mroti);
                    $total = $total_mili_roti*$per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));

                }else{
                    $per_vori_roti_price = Number(vori_price)/960;  /* per vori roti amount start */
                    $total_mili_roti = (Number(vori)*96+Number(ana)*6+Number(roti))*10+Number(mroti);
                    $total = $total_mili_roti*$per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));

                    console.log("Total Vori = "+vori+" Vori * Mili Roti = "+$total_mili_roti+" Per Mili Roti Price : "+$per_vori_roti_price+" Total Price : "+$total);
                }/* Final Else Condition End Here */
            }else{
                $(this).addClass("bg-danger").val(0);
            }
        });

        /* Calculate according to Vori End */


        $(document).on('change','#payment_type',function(){
            var type = $(this).val();
            if(type == 'bkash'){
                $("#bkash_code").show(500);
            }else{
                $("#bkash_code").hide(500);
            }
        });
        /* onchange quantity for total amount */

        /*
         When someone try to increment and decrement product price and if there price value found less then zero then
         it will be Original Product Price automatically
         */

        $(document).on('keyup change','#product_price',function(){

            var mroti = $("#miliroti").val();
            var vori = $("#vori").val();
            var ana =$("#ana").val();
            var roti = $("#roti").val();
            var vori_price = $(this).val();

            if(vori_price<=0){
                $(this).closest('div').addClass('has-error');
                alert('Sir/Madam Product Price Never be Zero');
                $(this).val(global_price);
                $(this).closest('div').removeClass('has-error');
            }else{
                $("#product_rate").val(0);

                if(vori == 0 && ana == 0 && roti == 0 && mroti > 0 ){

                    $per_vori_roti_price = Number(vori_price)/960;  /* per vori roti amount start */
                    $total_mili_roti = $per_vori_roti_price*5;
                    $("#product_rate").val($total_mili_roti.toFixed(2));

                }else if(vori == 0 && ana == 0 && roti>0 && mroti > 0){

                    $per_vori_roti_price = Number(vori_price)/960;  /* per vori roti amount start */
                    $total_mili_roti = (Number(vori)*96+(Number(ana)*6)+Number(roti))*10+Number(mroti);
                    $total = $total_mili_roti*$per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));

                }else if(vori == 0 && ana>0 && roti>0 && mroti > 0){

                    $per_vori_roti_price = Number(vori_price)/960;  /* per vori roti amount start */
                    $total_mili_roti = (Number(ana)*6+Number(roti))*10+Number(mroti);
                    console.log("Total Mili Roti : "+$total_mili_roti);
                    $total = $total_mili_roti*$per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));

                }else if(vori ==0 && ana ==0 && roti>0 && mroti == 0){
                    $per_roti = Number(vori_price)/96;

                    $total = $per_roti * Number(roti);
                    $("#product_rate").val($total.toFixed(2));
                }else if(vori > 0 && ana ==0 && roti > 0 && mroti == 0){
                    $per_roti = Number(vori_price)/96;
                    $total_roti = Number(vori)*96+Number(roti);
                    $total = $total_roti * $per_roti;
                    $("#product_rate").val($total.toFixed(2));
                }else if(vori > 0 && ana >0 && roti > 0 && mroti == 0){
                    $per_roti = Number(vori_price)/96;
                    $total_roti = Number(vori)*96+Number(ana)*6+Number(roti);
                    $total = $total_roti * $per_roti;
                    $("#product_rate").val($total.toFixed(2));
                }else if(vori > 0 && ana >0 && roti == 0 && mroti == 0){
                    $per_roti = Number(vori_price)/16;
                    $total_roti = Number(vori)*16+Number(ana);
                    $total = $total_roti * $per_roti;
                    $("#product_rate").val($total.toFixed(2));
                }else{
                    $per_vori_roti_price = Number(vori_price)/960;  /* per vori roti amount start */
                    $tmv = vori*960;
                    $arm = Number(vori)*96+Number(ana)*6+Number(roti);
                    $tmr = Number($arm)*10;
                    $total_mili_roti = Number($tmv)+Number($arm)+5;
                    $total = Number($total_mili_roti)*Number($per_vori_roti_price);
                    $("#product_rate").val($total.toFixed(2));


                    console.log("Total vori Mili Roti : "+$tmv);
                    console.log("Total Ana : "+$arm);
                    console.log("Only Mili Roti Base : "+$tmr);
                    console.log("Final Total Mili Roti : "+$total_mili_roti);
                    console.log("Total Price : "+$total);
                }

            }
        });



        /* When someone try to increment and decrement product price and if there price value found less then zero then it will be Original Product Price automatically */

        /* if quantity change then it will be calculate with price and store total value inside rate start */

        $(document).on('keyup change','#product_qty',function(){
            var price = $("#product_price").val();
            var qty = $(this).val();
            if(qty<=0){
                $("#product_rate").val(price);
                $(this).closest('div').addClass('has-error');
                alert('Sir/Madam Product Quantity Never be Zero');
                alert(price);
                $(this).val(1);

                $(this).closest('div').removeClass('has-error');
            }

            var total = 0;
            total = $("#product_price").val()*qty;
            $("#product_rate").val(total);

        });

        /**/


        /* Product add to Cart / Bucket Start From Here */

        $(document).on('click','#add_bucket',function(){

            var id = $("#product_name").val();
            var product_price = $("#product_price").val();
            var ana = $("#ana").val();
            var roti = $("#roti").val();
            var miliroti = $("#miliroti").val();
            var vori = $("#vori").val();
            var productqty = $("#product_qty").val();
            var rate = $("#product_rate").val();

            if(product_price < 0 ||  rate <= 0){
                alert("Please Select Every single field before Add to Cart");
            }else{
                $.ajax({
                    method : "post",
                    url : "{{ route('purchase.addCart') }}",
                    data:{id:id,product_price:product_price,"_token":'{{ csrf_token() }}',ana:ana,roti:roti,miliroti:miliroti,vori:vori,product_qty:productqty,rate:rate},
                    dataType:'html',
                    success:function(response){
                        $("#vori").val(0);
                        $("#ana").val(0);
                        $("#roti").val(0);
                        $("#miliroti").val(0);

                        $("#product_name").val('').attr('selected',false);
                        $('.product_name_select2').select2();
                        $("#product_code").val('').attr('selected',false);
                        $('.product_code_select2').select2();

                        $("#product_price").val('');
                        $("#weight_amount").val("");
                        $("#product_qty").val("");
                        $("#size_id").val();
                        $("#color_id").val("");
                        $("#karat_id").val("");
                        $("#origin_id").val("");
                        $("#gold_type_id").val("");

                        $("#brand_id").val("");
                        $("#category_id").val("");
                        $("#subcategory_id").val("");

                        $("#all_cart_item").html(response);
                    },
                    error:function(err){
                        console.log(err);
                    }
                });
            }
        });
        /* Product add to Cart / Bucket End Here */

        /* onload added products fetch */

        function Carts(){
            $.ajax({
                method:"get",
                url : "{{ route('purchase.carts') }}",
                dataType:'html',
                success:function(response){
                    $("#all_cart_item").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        }
        /* onload added products fetch */

        /*  Cart Remove Start */
        $(document).on('click','#remove_item',function(){
            var token = '{{ csrf_token() }}';
            var rowId = $(this).attr('data-id');
            $.ajax({
                method:"post",
                url:'{{ route("purchase.cart-remove") }}',
                data:{rowId:rowId,"_token":token},
                dataType:"html",
                success:function(response){
                    $("#all_cart_item").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        });
        /*  Cart Remove End */

        /* Cart Product List Edit Start */
        $(document).on('click','#update_item',function(){
            var rowId = $(this).attr('data-id');
            var token = '{{ csrf_token() }}';



            $.ajax({
                method:"post",
                url:'{{ route("purchase.cart-edit") }}',
                data:{rowId:rowId,"_token":token},
                dataType:"json",
                success:function(response){

                    var parsedata = JSON.parse(response);

                    $("#pid").val(parsedata.id);

                    global_price = parsedata.product_price;
                    $('#product_code').val(parsedata.id).attr('selected',true);
                    $('.product_name_select2').val(parsedata.id).attr('selected',true);

                    $('.product_code_select2').select2({tags:true,maximumSelectionLength : 1});
                    $('.product_name_select2').select2({tags:true,maximumSelectionLength : 1});



                    $('#product_price').val(parsedata.product_price);
                    $("#product_qty").val(1);
                    $('#product_rate').val(parsedata.total);

                    $("#vori").val(parsedata.vori);
                    $("#ana").val(parsedata.ana);
                    $("#roti").val(parsedata.roti);
                    $("#miliroti").val(parsedata.miliroti);

                    $("#size_id").val(parsedata.size);
                    $("#color_id").val(parsedata.color);
                    $("#karat_id").val(parsedata.karat);
                    $("#origin_id").val(parsedata.origin);
                    $("#gold_type_id").val(parsedata.goldtype);
                    $("#brand_id").val(parsedata.brand);
                    $("#category_id").val(parsedata.category);
                    $("#subcategory_id").val(parsedata.subcategorie);


                },
                error:function(err){
                    console.log(err);
                }
            });
        });

        /* Cart Product List Edit End */

        /* update Process Start From here */

        $(document).on('click','#unique_cart_update',function(){

            var id = $("#product_name").val();
            var product_price = $("#product_price").val();
            var ana = $("#ana").val();
            var roti = $("#roti").val();
            var miliroti = $("#miliroti").val();
            var vori = $("#vori").val();
            var productqty = $("#product_qty").val();
            var rate = $("#product_rate").val();

            if( product_price == "" || cart_quantity==""){
                alert("Please Select Every single field before Add to Cart");
            }
            else{
                $.ajax({
                    method : "post",
                    url : "{{ route('purchase.UpdateCart') }}",
                    data:{id:id,rowId:rowId,product_price:product_price,"_token":'{{ csrf_token() }}',weight_id:weight_id,weight_amount:weight_amount,cart_quantity:cart_quantity},
                    data:{id:id,rowId:rowId,product_price:product_price,"_token":'{{ csrf_token() }}',ana:ana,roti:roti,miliroti:miliroti,vori:vori,product_qty:productqty,rate:rate},
                    dataType:'html',
                    success:function(response){
                        $(this).removeAttr("data-id");
                        $(this).text("Add To Bucket");
                        $(this).attr("class","btn btn-primary");
                        $(this).attr("id","add_bucket");

                        $("#unique_cart_update").text('Add To Cart').attr('id','add_bucket').removeAttr('data-id');

                        $("#product_name").val('').attr('selected',false);
                        $('.product_name_select2').select2({ tags:true,maximumSelectionLength : 1});

                        $("#product_code").val('').attr('selected',false);
                        $('.product_code_select2').select2({ tags:true,maximumSelectionLength : 1});

                        $("#product_price").val('');

                        $("#karat_id").val('').attr('selected',false);
                        $('.karat_id_select2').select2({ tags:true,maximumSelectionLength : 1});

                        $("#all_cart_item").html(response);
                    },
                    error:function(err){
                        console.log(err);
                    }
                });
            }
        });

        /* Update Process End From here */


        /* payment calculation start from here */

        $(document).on('keyup','#discount',function() {
            var total_amount = $("#total_amount").val();
            var discount = $(this).val();
            var balance = total_amount - discount;
            if(balance>0){
                $("#balance").css({"border": "2px red","background": "red","color": "#fff","font-size": "1.5rem","font-weight": "bold"});
            }else{
                $("#balance").css({"border": "2px green","background": "#fff","color": "#000","font-size": "1.5rem","font-weight": "bold"});
            }
            $("#balance").val(balance);

        });


        $(document).on('keyup','#paid',function() {
            var total_amount = $("#total_amount").val();
            var discount = $("#discount").val();
            var payable_amount = total_amount - discount;
            var total_value  = $(this).val();
            var bal = payable_amount - total_value;
            if(bal>0){
                $("#balance").css({"border": "2px red","background": "red","color": "#fff","font-size": "1.5rem","font-weight": "bold"});
            }else{
                $("#balance").css({"border": "2px green","background": "#fff","color": "#000","font-size": "1.5rem","font-weight": "bold"});
            }
            $("#balance").val(bal.toFixed(2));

        });
        /* Payment calculation end here */

        /* */

    </script>
@endsection