@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">New STOCK-OUT</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">STOCK-OUT</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">STOCK-OUT Management </h3>
                            </div>
                            <div class="panel-body">
                                {{-- Product Name search Button --}}
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                                        {{-- First Row For Product Search --}}
                                        <div class="row">

                                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a data-toggle="tab" href="#singlesale" class="btn btn-primary"
                                                           style="font-weight: bold;letter-spacing: 2px;">SIGLE SALE</a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#serialsale" class="btn btn-primary"
                                                           style="font-weight: bold;letter-spacing: 2px;">SERIAL
                                                            SALE</a>
                                                    </li>
                                                </ul>

                                                <div class="tab-content">
                                                    <div id="singlesale" class="tab-pane fade in active">
                                                        <br>
                                                        {{ Form::text('fbarcode',null,['class'=>'form-control sigleinput','id'=>'fbarcode','placeholder'=>'SINGLE BARCODE','onchange'=>'singlechange()'])}}

                                                    </div> <!-- Home End -->
                                                    <div id="serialsale" class="tab-pane fade">
                                                        <div class="row">
                                                            <br>
                                                            <div class="col-lg-6 col-sm-5 col-md-5 col-xs-12 singlediv">

                                                                {{ Form::text('fbarcode',null,['class'=>'form-control serialinput','id'=>'start_barcode','placeholder'=>'START BARCODE','onchange'=>'serialchange()'])}}

                                                            </div>

                                                            <div class="col-lg-6 col-sm-5 col-md-5 col-xs-12 singlediv">
                                                                {{ Form::text('fbarcode',null,['class'=>'form-control serialinput','id'=>'end_barcode','placeholder'=>'END BARCODE','onchange'=>'serialchange()'])}}
                                                            </div>
                                                        </div><!-- row End -->
                                                    </div><!-- menu1 End -->
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Row Search End --}}

                                        {{-- Added Product View  --}}
                                        <span style="display: block;height: 40px;width: 100%;background: #fff;"></span>

                                        <div class="row">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>SL</th>
                                                    <th>Barcode</th>
                                                    <th>Pro.Code</th>
                                                    <th>Name</th>
                                                    <th>Brand</th>
                                                    <th>Origin</th>
                                                    <th>Karat</th>
                                                    <th>Size</th>
                                                    <th>Color</th>
                                                    <th>Status</th>
                                                    <th>Price</th>
                                                    <th>Qty</th>
                                                    <th>Sub-total</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody id="saleitem">

                                                </tbody>
                                            </table>
                                        </div>
                                        {{-- Price Weight , Weight-amount Row End From Here --}}
                                    </div>{{-- left div end--}}

                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                                        {{-- Customer Information --}}
                                        <h4 class="bg-primary text-center" style="padding:10px 0;">Gold Maker
                                            Information</h4>
                                        <div class="row">
                                            {{ Form::open(['route'=>'stockout.goldmakergold','method'=>'post'])}}

                                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">

                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12  ">
                                                    <div class="{{ $errors->first('description') ? 'has-error' : ''}} form-group">
                                                        {{ Form::label('description','Description : ',['class'=>'bg-primary form-control']) }}

                                                        {{ Form::textarea('description',null,['class'=>'form-control','rows'=>'4']) }}
                                                        @if($errors->has('description'))
                                                            <strong class="has-error">
                                                                <span class="has-error"> {{ $errors->first('gold_maker_id')}} </span>
                                                            </strong>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">

                                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12  ">
                                                    <div class="{{ $errors->first('gold_maker_id') ? 'has-error' : ''}} form-group">
                                                        {{ Form::label('gold_maker_id','SELECT GOLD MAKER : ',['class'=>'bg-primary form-control']) }}

                                                        {{ Form::select('gold_maker_id',$GoldMakers,false,['class'=>'form-control customer_select2','multiple'=>true]) }}
                                                        @if($errors->has('gold_maker_id'))
                                                            <strong class="has-error">
                                                                <span class="has-error"> {{ $errors->first('gold_maker_id')}} </span>
                                                            </strong>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                <br>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary fa fa-save"> STORE
                                                        OUT
                                                    </button>
                                                </div>
                                            </div>

                                        </div> {{-- customer end --}}


                                    </div> {{-- Customer Information Row End --}}

                                </div>
                            </div>{{-- Right div end--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script>
        $(document).ready(function () {

            SaleCart();

            $("#bkash_code").hide();
            $('.customer_select2').select2({tags: true, maximumSelectionLength: 1,allowClear: true});

            $(".sigleinput").css({
                "background": "#25476a",
                "text-align": "center",
                "font-size": " 2rem",
                "padding": " 10px",
                "color": "#fff",
                "font-weight": "bold",
                "font-style": "italic"
            });
            $(".serialinput").css({
                "background": "#25476a",
                "text-align": "center",
                "font-size": " 2rem",
                "padding": " 10px",
                "color": "#fff",
                "font-weight": "bold",
                "font-style": "italic"
            });


            /* without number press block start */

            $('#fbarcode').bind('keypress', function (event) {
                var regex = new RegExp("^[0-9]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });


            $('#sbarcode').bind('keypress', function (event) {
                var regex = new RegExp("^[0-9]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });

            $('#ebarcode').bind('keypress', function (event) {
                var regex = new RegExp("^[0-9]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
            /* without number press block end here */
        });

        /* Single sale start */

        function singlechange() {
            var barcode = $("#fbarcode").val();
            $.ajax({
                method: "post",
                url: "{{ route('stock.barcode')}}",
                data: {barcode: barcode, "_token": "{{ csrf_token() }}"},
                dataType: "html",
                success: function (response) {

                    console.log(response);
                    $("#fbarcode").val("");

                    if (response.length == 12) {
                        $.notify("Already added this : " + response + " in Sale Cart", {
                            globalPosition: 'bottom right',
                            className: 'primary'
                        });
                        $("#rowid" + response).css({
                            "border": "4px red !important",
                            "overflow": "hidden",
                            "background": "#000",
                            "color": "white"
                        });
                    } else if (response.length == 24) {

                        $.notify("No Record found for this : " + response + " in Database", {
                            globalPosition: 'bottom right',
                            className: 'success'
                        });
                        alert("No Record Found for this ");

                    } else if (response.length == 14) {
                        $.notify("Already sold this : " + response + " product", {
                            globalPosition: 'bottom right',
                            className: 'success'
                        });
                        alert("Already sold this " + response + " product");
                    } else {
                        $("#saleitem").html(response);
                    }

                    // console.log(response.size[0]['name']);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        }

        /* Single Sale End */

        /* Serial Sale Start */

        function serialchange() {
            var start = $("#start_barcode").val();
            var end = $("#end_barcode").val();
            if (start != "" && end != "") {
                /* compare start is smaller then end value */
                if (start < end) {

                    /* SERIAL SALE AJAX REQUEST FIRE FROM HERE */
                    $.ajax({
                        method: "post",
                        url: "{{ route('stock.barcode-serial')}}",
                        data: {start: start, end: end, "_token": "{{ csrf_token() }}"},
                        dataType: "html",
                        success: function (response) {
                            $("#start_barcode").val("");
                            $("#end_barcode").val("");
                            $("#fbarcode").val("");

                            if (response.length == 12) {
                                $.notify("Already added this : " + response + " in Sale Cart", {
                                    globalPosition: 'bottom right',
                                    className: 'primary'
                                });
                                $("#rowid" + response).css({
                                    "border": "4px red !important",
                                    "overflow": "hidden",
                                    "background": "#000",
                                    "color": "white"
                                });
                            } else if (response.length == 14) {
                                $.notify("No Record found for this : " + response + " in Database", {
                                    globalPosition: 'bottom right',
                                    className: 'success'
                                });
                                alert("No Record Found for this ");
                            } else {
                                $("#saleitem").html(response);
                            }
                            // console.log(response.size[0]['name']);
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });

                    /* SERIAL SALE AJAX REQUEST END FROM HERE */
                } else {
                    alert(start + " should be smaller then " + end);
                }
            }

        }

        /* Serial Sale Start */

        function SaleCart() {
            $.ajax({
                url: "{{route('sale.find')}}",
                dataType: "html",
                success: function (response) {
                    $("#saleitem").html(response);
                },
                error: function (er) {
                    console.log(er);
                }
            });
        }

        /* Barcode Select and Fire every single Field Start  */

        $(document).on('change', '#barcode', function () {
            var barcode = $(this).val();

            $.ajax({
                method: "post",
                url: "{{ route('sale.barcode')}}",
                data: {barcode: barcode, "_token": "{{ csrf_token() }}"},
                dataType: "html",
                success: function (response) {
                    console.log(response);
                    $("#saleitem").html(response);
                    // console.log(response.size[0]['name']);
                },
                error: function (err) {
                    console.log(err);
                }
            });

        });

        /* Barcode Select and Fire every single Field Ebd  */

        /* item remove from list start */

        function itemremove(id) {
            var data = confirm("Press 'OK' for Delete/remove from Bucket List");
            if (data) {
                var rowId = $("#removelist" + id).attr('data-id');
                var token = '{{ csrf_token() }}';
                $.ajax({
                    method: "post",
                    url: "{{ route('sale.remove') }}",
                    data: {rowId: rowId, "_token": token},
                    dataType: "html",
                    success: function (response) {
                        $("#saleitem").html(response);
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            }

        }

        /* item remove from list end */

        $(document).on('change', '#payment_type', function () {
            var type = $(this).val();
            if (type == 'bkash') {
                $("#bkash_code").show(500).attr("class", "btn btn-primary form-control");
            } else {
                $("#bkash_code").hide(500);

            }
        });


        /* CUSTOMER INFORMATION FETCH AJAX REQUEST START */
        /* suppliers search start */

        $(document).on('change', '#customer_id', function () {
            $("#customer_name").val("");
            $("#customer_phone").val("");
            $("#customer_address").val("");

            var customer_id = $(this).val();
            var token = "{{ csrf_token() }}";
            $.ajax({
                method: "post",
                url: "{{route('customer.search')}}",
                data: {customer_id: customer_id, "_token": token},
                dataType: 'json',
                success: function (response) {

                    $("#customer_name").val(response[0].name);
                    $("#customer_phone").val(response[0].phone);
                    $("#customer_address").val(response[0].address);
                    console.log("Response List : " + response[0].id);
                },
                error: function (err) {
                    console.log("Error List : " + err);
                }
            });

        });

        /* suppliers search end */

        /* CUSTOMER INFORMATION FETCH AJAX REQUEST END */

    </script>
@endsection