@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Salary</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Salary</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">View Salary</h3>
                            </div>
                            <div class="panel-body">

                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <br>
                                    <br>
                                    <div class="form-group">
                                        {{ Form::label('cost_type_id','Cost Type : ',['class'=>'control-label'])}}
                                        {{ Form::select('cost_type_id',$costtypes,null,['placeholder'=>'All Cost','class'=>'form-control','id'=>'costtype']) }}
                                    </div>
                                    <div class="form-group">
                                        <div id="demo-dp-range">

                                            {{ Form::label("em","Select Date: ",['class'=>'label-control']) }}
                                            <div class="input-daterange input-group" id="datepicker">
                                                <input type="text" class="form-control" name="start" id="start" required placeholder="2019-01-01">
                                                <span class="input-group-addon">to</span>
                                                <input type="text" class="form-control" name="end" id="end" required placeholder="2019-12-31">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        {{ Form::button('CLICK AND VIEW COST REPORT ',['class'=>'form-control btn btn-primary','id'=>'query_cost']) }}
                                    </div>
                                </div>

                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <table class="table table-hover" id="costtable">
                                        <thead>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Amount</th>
                                            <th>Cost Date</th>
                                         </thead>
                                        <tbody id="cost_summary">

                                        </tbody>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function(){
            $("#datepicker").datepicker({
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });
            $("#salarydatepicker").datepicker();
            //$('#costtable').DataTable();
        });

        $(document).on('click','#query_cost',function () {

            var costtype = $("#costtype").val();
            var start = $("#start").val();
            var end = $("#end").val();
            var token = '{{ csrf_token() }}';
            var route = "{{ route('localcost.search') }}";
            if (start != "" || end !=''){
                if (start<end){
                    $.ajax({
                        method:"post",
                        url:route,
                        data:{start:start, end:end,cost_type_id:costtype,"_token":token},
                        dataType:"html",
                        success:function (response){
                            $("#cost_summary").html(response);
                            $('#costtable').DataTable();
                            console.log(response);

                        },
                        error:function (err) {
                            console.log(err);
                        }
                    });
                }else{
                    alert("Start should be larger then End");
                }

                /**/


            }else{
                alert("You must select a Date range");
            }
        });
        /* UPDATE Category END */
    </script>



@endsection