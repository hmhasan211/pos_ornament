@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Product</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Product</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Product</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                {{ Form::model($product_edit,['route'=>'product.update','method'=>'post','enctype'=>'multipart/form-data']) }}


                                    <input type="hidden" name="id" value="{{ $product_edit->id }}" class="form-control">

                                    <div class="col-lg-2 col-sm-2 col-xs-12 {{$errors->has('product_code') ? 'has-error' : ''}}">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        {{ Form::label('product','Product Code : ',['class'=>'control-label'])}}
                                        {{ Form::text('product_code',old('product_code'),['class'=>'form-control','id'=>'prod_code','placeholder'=>'Ex: G102'])}}
                                        @if ($errors->has('product_code'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('product_code') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <!--  PRODUCT NAME  -->
                                    <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('product_name') ? 'has-error' : ''}}">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        
                                        {{ Form::label('product','Product Name : ',['class'=>'control-label'])}}
                                        {{ Form::text('product_name',old('product_name'),['class'=>'form-control','placeholder'=>'Ex: Ring'])}}
                                        @if ($errors->has('product_name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('product_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <!-- / PRODUCT NAME  -->

                                    <!--  PRODUCT LOCAL NAME  -->
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        
                                        {{ Form::label('product','Product Local Name : ',['class'=>'control-label'])}}
                                        {{Form::hidden('product_local_name','product_local_name',['class'=>'form-control','placeholder'=>'Ex: Ring'])}}
                                        @if ($errors->has('product_local_name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('product_local_name') }}</strong>
                                            </span>
                                        @endif
                                    <!-- / PRODUCT LOCAL NAME  -->


                                    <!--  PRODUCT LOCAL NAME  -->
                                    <div class="col-lg-2 col-sm-2 col-xs-12 {{$errors->has('product_price') ? 'has-error' : ''}}">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        
                                        {{ Form::label('product','Price : ',['class'=>'control-label'])}}
                                        {{Form::text('product_price',old('product_price'),['class'=>'form-control','placeholder'=>'Ex: 5000 tk'])}}
                                        @if ($errors->has('product_price'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('product_price') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <!-- / PRODUCT LOCAL NAME  -->

                                     <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('brand_id') ? 'has-error' : ''}}">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        {{ Form::label('brand','Brand : ',['class'=>'control-label'])}}
                                        {{ Form::select('brand_id',$brand,null,['id'=>'brand_id','required','class'=>'form-control '])}}
                                    </div>



                                    <!--  PRODUCT SIZE NAME  -->
                                    <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('size_id') ? 'has-error' : ''}}">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        
                                        {{ Form::label('product','Size : ',['class'=>'control-label'])}}

                                        {{ Form::select('size_id', $size,null,['class'=>'form-control'])}}

                                    @if($errors->has('size_id'))
                                            <span class="help-block">
                                                <strong>{{$errors->first('size_id')}}</strong>
                                            </span>
                                        @endif
                                       
                                    </div>
                                    <!-- / PRODUCT SIZE NAME  -->


                                    <!--  PRODUCT color NAME  -->
                                    <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('color_id') ? 'has-error' : ''}}">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        
                                        {{ Form::label('color','Color : ',['class'=>'control-label'])}}
                                        
                                        {{ Form::select('color_id', $color,null,['class'=>'form-control']) }}
                                         @if($errors->has('color_id'))
                                            <span class="help-block">
                                                <strong>{{$errors->first('color_id')}}</strong>
                                            </span>
                                        @endif

                                    </div>
                                    <!-- / PRODUCT color NAME  -->

                                    <!--  PRODUCT karat NAME  -->
                                    <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('karat_id') ? 'has-error' : ''}}">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        {{ Form::label('karat','Karat : ',['class'=>'control-label'])}}
                                        
                                        {{ Form::select('karat_id', $karat,null,['class'=>'form-control']) }}
                                         @if($errors->has('karat_id'))
                                            <span class="help-block">
                                                <strong>{{$errors->first('karat_id')}}</strong>
                                            </span>
                                        @endif

                                    </div>
                                    <!-- / PRODUCT karat NAME  -->

                                    <!-- Origin Select Start -->

                                    <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('karat_id') ? 'has-error' : ''}}">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        {{ Form::label('origin','Origin : ',['class'=>'control-label'])}}
                                        
                                        {{ Form::select('origin_id', $origin,null,['class'=>'form-control']) }}
                                         @if($errors->has('origin_id'))
                                            <span class="help-block">
                                                <strong>{{$errors->first('origin_id')}}</strong>
                                            </span>
                                        @endif

                                    </div>

                                    <!-- Origin Select End -->

                                    <!-- categorie_id Select Start -->
                                    <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('categorie_id') ? 'has-error' : ''}}">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        {{ Form::label('Category','Category : ',['class'=>'control-label'])}}
                                        
                                        {{ Form::select('categorie_id', $category,null,['class'=>'form-control' ,'id'=>'categorie_id']) }}
                                         @if($errors->has('categorie_id'))
                                            <span class="help-block">
                                                <strong>{{$errors->first('categorie_id')}}</strong>
                                            </span>
                                        @endif

                                    </div>
                                    <!-- categorie_id Select End -->

                                    <!-- sub_categorie_id Select Start -->
                                    <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('categorie_id') ? 'has-error' : ''}}">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        {{ Form::label('Category','Sub-Category : ',['class'=>'control-label'])}}
                                        
                                        {{ Form::select('sub_categorie_id',[''],true,['class'=>'form-control','id'=>'sub_categorie_id']) }}
                                         @if($errors->has('sub_categorie_id'))
                                            <span class="help-block">
                                                <strong>{{$errors->first('sub_categorie_id')}}</strong>
                                            </span>
                                        @endif

                                    </div>

                                    <!-- sub_categorie_id Select End -->


                                    <!-- gold_type_id Select Start -->

                                    <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('gold_type_id') ? 'has-error' : ''}}">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        {{ Form::label('Gold Type','Gold Type : ',['class'=>'control-label'])}}
                                        
                                        {{ Form::select('gold_type_id', $gold_type,'Select Gold Type',['class'=>'form-control']) }}
                                         @if($errors->has('gold_type_id'))
                                            <span class="help-block">
                                                <strong>{{$errors->first('gold_type_id')}}</strong>
                                            </span>
                                        @endif

                                    </div>

                                    <!-- gold_type_id Select End -->

                                     <!--  PRODUCT File NAME  -->
                                    <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('product_image') ? 'has-error' : ''}}">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        {{ Form::label('image','Image : ',['class'=>'control-label'])}}
                                        
                                        {{ Form::file('product_image', ['class'=>'form-control']) }}

                                        @if($errors->has('product_image'))
                                            <span class="help-block">
                                                <strong>{{$errors->first('product_image')}}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <!-- / PRODUCT File NAME  -->

                                    
                                    <!--  PRODUCT details NAME  -->
                                    <div class="col-lg-6 col-sm-6 col-xs-12 {{$errors->has('product_details') ? 'has-error' : ''}}">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        {{ Form::label('image','Product Details : ',['class'=>'control-label'])}}
                                        
                                        {!! Form::textarea('product_details',null,['class'=>'form-control', 'rows' => 6, 'cols' => 40,'placeholder'=>'Product Details.......']) !!}

                                        @if($errors->has('product_details'))
                                            <span class="help-block">
                                                <strong>{{$errors->first('product_details')}}</strong>
                                            </span>
                                        @endif

                                    </div>
                                    <!-- / PRODUCT details NAME  -->

                                    {{--  old image view start  --}}

                                    <div class="col-lg-2 col-sm-2 col-xs-12">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        {{ Form::label('upimg','Uploaded Image : ',['class'=>'label-control'])}}
                                        <img src="{{ url('public/admin/product/upload/'.$product_edit->product_image)}}" alt="no image" class="form-control" style="height: 120px;width: 120px;">
                                    </div>

                                    {{--  old image end  --}}


                                    <div class="col-lg-12 col-sm-12 col-xs-12"><span style="display: block;height: 10px;width: 100%;background: #fff;"></span></div>

                                    <div class="col-lg-12 col-sm-12 col-xs-12">
                                        {{ Form::button('UPDATE PRODUCT',['type'=>'submit','class'=>'btn btn-primary']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){
        /* if product code empty then it will be fill up automatically start */

        if($("#prod_code").val() == "")
        {

            var code ='';
            var key = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            for (var i = 0; i < 3; i++)
                code += key.charAt(Math.floor(Math.random() * key.length));
            $("#prod_code").val("P"+code);
        }

        /* if product code empty then it will be fill up automatically End */

        setTimeout(function(){ 
            var category_id = $("#categorie_id").val();
            $.ajax({
                method:"post",
                url:"{{ route('purchase.subcategory') }}",
                data:{category_id:category_id,"_token":"{{ csrf_token() }}"},
                dataType:"html",
                success:function(response){
                    $("#sub_categorie_id").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        }, 200);

    });



    /* select Category and load sub-category automatically start */
        $(document).on('change','#categorie_id',function(){
            var category_id = $(this).val();
            $.ajax({
                method:"post",
                url:"{{ route('purchase.subcategory') }}",
                data:{category_id:category_id,"_token":"{{ csrf_token() }}"},
                dataType:"html",
                success:function(response){
                    $("#sub_categorie_id").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        });

        /* select category and load sub-category automatically end */


</script>
@endsection