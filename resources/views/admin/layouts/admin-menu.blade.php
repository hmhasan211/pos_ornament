<!--MAIN NAVIGATION-->
<!--===================================================-->
<nav id="mainnav-container">
    <div id="mainnav">
        <!--Menu-->
        <!--================================-->
        <div id="mainnav-menu-wrap">
            <div class="nano">
                <div class="nano-content">
                    <!--Shortcut buttons-->
                    <!--================================-->
                    <div id="mainnav-shortcut" class="hidden">
                        <ul class="list-unstyled shortcut-wrap">
                            <li class="col-xs-3" data-content="My Profile">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-mint">
                                        <i class="demo-pli-male"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Messages">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                        <i class="demo-pli-speech-bubble-3"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Activity">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                        <i class="demo-pli-thunder"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Lock Screen">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-purple">
                                        <i class="demo-pli-lock-2"></i>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!--================================-->
                    <!--End shortcut buttons-->
                    @php
                         $prefix=Request::route()->getPrefix();
                         $route=Route::current()->getName();
                    @endphp

                    <ul id="mainnav-menu" class="list-group">
                        <!--Category name-->
                        <li class="list-header">Navigation</li>

                        <!--Menu list item-->
                        <li class="{{($route=='home')? 'active-sub active':''}}">
                            <a href="{{url('/home')}}"><i class="demo-pli-home"></i>
                                <span class="menu-title">ড্যাশবোর্ড/Dashboard</span>
                            </a>
                        </li>

                        <!-- Class Menu Add -->

                        <li  class="{{($prefix=='/ProductManagement')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-shopping-cart"></i>
                                <span class="menu-title">প্রোডাক্ট ম্যানেজমেন্ট</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">

                                <li class="">
                                    <a href="{{route('product.add')}}"><i class="ion-merge"></i> নতুন প্রোডাক্ট/Add New Product</a>
                                </li>

                                <li class="">
                                    <a href="{{route('product.view')}}"><i class="ion-merge"></i>প্রোডাক্ট লিস্ট/View Products</a>
                                </li>


                                <li class="">
                                    <a href="{{route('brand.add')}}"><i class="ion-merge"></i>ব্র্যান্ড যোগ/দেখুন-Add/View Brand</a>
                                </li>

                                <li class="">
                                    <a href="{{route('category.add')}}"><i class="ion-merge"></i> ব্র্যান্ড ক্যাটাগরি যোগ/দেখুন-Add / View Category</a>
                                </li>

                                <li class="">
                                    <a href="{{route('sub-category.add')}}"><i class="ion-merge"></i> ব্র্যান্ড সাব-ক্যাটাগরি যোগ/দেখুন-Add / View Sub-Category</a>
                                </li>

                                <li class="">
                                    <a href="{{route('color.add')}}"><i class="ion-merge"></i> রং যোগ/দেখুন-Add / View Color</a>
                                </li>

                                <li class="">
                                    <a href="{{route('size.add')}}"><i class="ion-merge"></i>আকার যোগ/দেখুন-Add / View Size</a>
                                </li>

                                <li class="">
                                    <a href="{{route('karat.add')}}"><i class="ion-merge"></i>ক্যারেট যোগ/দেখুন-Add / View Karat</a>
                                </li>

                                <li class="">
                                    <a href="{{route('origin.add')}}"><i class="ion-merge"></i>অরিজিন যোগ/দেখুন-Add / View Origin</a>
                                </li>

                                <li class="">
                                    <a href="{{route('weight.add')}}"><i class="ion-merge"></i>ওজন যোগ/দেখুন-Add / View Weight</a>
                                </li>

                                <li class="">
                                    <a href="{{route('goldtype.add')}}"><i class="ion-merge"></i>স্বর্ণের ধরন-Add / View Gold Type</a>
                                </li>

                            </ul>
                        </li>

                        
                     

                        <li class="{{($prefix=='/PurchaseMangement')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-credit-card"></i>
                                <span class="menu-title"> খরিদ/Purchase</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class=""><a href="{{route('purchase.add')}}"><i class="ion-merge"></i> নতুন ক্রয়/Purchase</a></li>
                                <li class=""><a href="{{route('purchase.view')}}"><i class="ion-merge"></i> ক্রয় লিস্ট/Purchase History</a></li>
                                <li class=""><a href="{{route('invoice.view')}}"><i class="ion-merge"></i> চালান লিস্ট/Invoice History</a></li>
                                <li class=""><a href="{{route('invoice.due')}}"><i class="ion-merge"></i> বকেয়া চালান লিস্ট/Due-Invoice History</a></li>
                            </ul>
                        </li>


                        <li class="{{($prefix=='/SaleManagement')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-shopping-cart"></i>
                                <span class="menu-title">বিক্রয়/Sales</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class=""><a href="{{route('sale.barcodelist')}}"><i class="ion-merge"></i>বারকোড লিস্ট-Available Barcode List</a></li>
                                <li class=""><a href="{{route('sale.view')}}"><i class="ion-merge"></i>নতুন ‍বিক্রয়/Add Sale Item</a></li>
                                <li class=""><a href="{{route('sale.salesummary')}}"><i class="ion-merge"></i>বিক্রয় লিস্ট/View Sale Item</a></li>
                                <li class=""><a href="{{route('sale.invoice.due')}}"><i class="ion-merge"></i>চালান লিস্ট/Invoice History</a></li>
                            </ul>
                        </li>


                        <li class="{{($prefix=='/Stock')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-bar-chart"></i>
                                <span class="menu-title">মজুদ/Stock</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class=""><a href="{{route('stock.view')}}"><i class="ion-merge"></i>মজুদ লিস্ট/VIEW STOCK ITEM</a></li>
                                <li class=""><a href="{{route('stock.in')}}"><i class="ion-merge"></i> নতুন মজুদ সংযোজন/ADD IN STOCK FROM GOLD MAKER</a></li>
                                <li class=""><a href="{{route('stock.inview')}}"><i class="ion-merge"></i>মজুদের পরিমাণ/HISTORY IN STOCK</a></li>
                                <li class=""><a href="{{route('stock.out')}}"><i class="ion-merge"></i> মজুদ বর্হিগমন/STOCK OUT TO GOLD MAKER</a></li>
                                <li class=""><a href="{{route('stockout.view')}}"><i class="ion-merge"></i>মজুদ বর্হিগমন লিস্ট/HISTORY STOCK OUT</a></li>
                                <li class=""><a href="{{route('stock.barcodelist')}}"><i class="ion-merge"></i>বারকোড লিস্ট/BARCODE LIST</a></li>
                            </ul>
                        </li>


                        <li class="{{($prefix=='/Mortgage')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-bar-chart"></i>
                                <span class="menu-title">বন্ধক/Mortgage</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class=""><a href="{{route('mortgage.add')}}"><i class="ion-merge"></i> নতুন বন্ধক/ADD NEW MORTGAGE</a></li>
                                <li class=""><a href="{{route('mortgage.view')}}"><i class="ion-merge"></i> বন্ধক লিস্ট/VIEW MORTGAGE</a></li>
                                <li class=""><a href="{{route('mortgage.invoice.due')}}"><i class="ion-merge"></i> চালান লিস্ট/VIEW MORTGAGE INVOICE</a></li>
                                <li class=""><a href="{{route('sendtomahajan.add')}}"><i class="ion-merge"></i> মহাজনের নিকট পাঠান/Send to Mahajan</a></li>
                                <li class=""><a href="{{route('receivemahajan.add')}}"><i class="ion-merge"></i> মহাজনের নিকট হতে নিয়ে আসুন/Receive From Mahajan</a></li>
                                <li class=""><a href="{{route('paytomahajan.add')}}"><i class="ion-merge"></i> মহাজনকে সুদ ‍দিন/Pay Interest to Mahajan</a></li>
                                <li class=""><a href="{{route('receive_int_customer.add')}}"><i class="ion-merge"></i> কাস্টমার হতে সুদ নিন/Receive Interest from customer</a></li>
                                <li class=""><a href="{{route('mortgage.barcodelist')}}"><i class="ion-merge"></i> বারকোড লিস্ট/Barcodes List</a></li>
                            </ul>

                        </li>

                        {{-- Supplier Management start --}}


                        <li class="{{($prefix=='/SupplierManagement')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-user-secret"></i>
                                <span class="menu-title">সরবরাহকারী/Suppliers</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class=""><a href="{{route('supplier.add')}}"><i class="ion-merge"></i> নতুন সরবরাহকারী-Add/View Supplier</a></li>
                            </ul>
                        </li>

                        {{-- Supplier Management End --}}

                        {{-- Mahajan Management start --}}


                        <li class="{{($prefix=='/Mahajan')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-user-secret"></i>
                                <span class="menu-title">মহাজন/Banker</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class=""><a href="{{route('mahajan.add')}}"><i class="ion-merge"></i> নতুন মহাজন-Add/View Banker</a></li>
                            </ul>
                        </li>

                        {{-- Mahajan Management End --}}

                        {{-- Customer Management start --}}

                        <li class="{{($prefix=='/CustomerManagement')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-user-secret"></i>
                                <span class="menu-title">ক্রেতা/Customers</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class=""><a href="{{route('customer.add')}}"><i class="ion-merge"></i>নতুন ক্রেতা যোগ/দেখুন-Add/View Customer</a></li>
                            </ul>
                        </li>

                        {{-- Customer Management End --}}

                        {{-- GoldMaker Management start --}}

                        <li class="{{($prefix=='/GoldMakerManagement')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-diamond"></i>
                                <span class="menu-title">স্বর্ণকার/Gold Maker</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class=""><a href="{{route('goldmaker.add')}}"><i class="ion-merge"></i> নতুন স্বর্ণকার যোগ/দেখুন-Add/View Gold Maker</a></li>
                                <li class=""><a href="{{route('goldmaker.profiles')}}"><i class="ion-merge"></i>স্বর্ণকার প্রোফাইল-GOLD MAKER PROFILES</a></li>
                            </ul>
                        </li>

                        {{-- GoldMaker Management End --}}


                        {{-- additional Cost start  --}}

                        <li class="{{($prefix=='/localcost')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-money"></i>
                                <span class="menu-title">মনিহারি খরচ/Daily Stationary-Cost.</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class=""><a href="{{route('localcost.typestore')}}"><i class="ion-merge"></i>খরচের ধরণ যোগ/দেখুন Add / view Cost Type</a></li>
                                <li class=""><a href="{{route('localcost.add')}}"><i class="ion-merge"></i>নতুন খরচ-Add Cost Information</a></li>
                                <li class=""><a href="{{route('localcost.history')}}"><i class="ion-merge"></i> খরচের লিস্ট/Cost History</a></li>
                            </ul>
                        </li>



                        <li class="{{($prefix=='/Employee')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-user-secret"></i>
                                <span class="menu-title">কর্মচারী/Employee Manag.</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class=""><a href="{{route('employee.add')}}"><i class="ion-merge"></i>নতুন কর্মচারী যোগ/দেখুন-Add/View Employee</a></li>
                            </ul>
                        </li>


                        <li class="{{($prefix=='/Salary')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-money"></i>
                                <span class="menu-title">বেতন/Salary Mang.</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class=""><a href="{{route('salary.add')}}"><i class="ion-merge"></i> বেতন প্রদান/Salary Payment</a></li>
                                <li class=""><a href="{{route('salary.history')}}"><i class="ion-merge"></i> বেতন প্রদানের লিস্ট/Salary History</a></li>
                            </ul>
                        </li>


                        <li class="{{($prefix=='/MoneySummary')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-money"></i>
                                    <span class="menu-title">আয়-ব্যায়/Income - Expense Mng.</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class=""><a href="{{route('moneysummary.receiveadd')}}"><i class="ion-merge"></i>আয় যোগ করুন-Income/Receive Money</a></li>
                                <li class=""><a href="{{route('moneysummary.incomereport')}}"><i class="ion-merge"></i>আয় বিবরণী-Income Report</a></li>
                                <li class=""><a href="{{route('moneysummary.costmoney')}}"><i class="ion-merge"></i> ব্যয় যোগ করুন-Expense Money</a></li>
                                <li class=""><a href="{{route('moneysummary.expensereport')}}"><i class="ion-merge"></i>ব্যয় বিবরণী-Expense Report</a></li>
                            </ul>
                        </li>


                        {{-- additional Cost end  --}}
                    </ul>

                </div>
            </div>
        </div>
        <!--================================-->
        <!--End menu-->

    </div>
</nav>
<!--===================================================-->
<!--END MAIN NAVIGATION-->
