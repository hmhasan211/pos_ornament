@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Salary</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Salary</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">View Salary</h3>
                            </div>
                            <div class="panel-body">

                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <br>
                                    <br>
                                    <div class="form-group">
                                        {{ Form::label("em","Select Employee",['class'=>'label-control']) }}
                                        {{ Form::select('employee_id',$employe,true,['class'=>'form-control','id'=>'employee_id']) }}
                                    </div>



                                    <div class="form-group">
                                        <div id="demo-dp-range">
                                            {{ Form::label("em","Select Date",['class'=>'label-control']) }}
                                            <div class="input-daterange input-group" id="datepicker">
                                                <input type="text" class="form-control" name="start" id="start" required>
                                                <span class="input-group-addon">to</span>
                                                <input type="text" class="form-control" name="end" id="end" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        {{ Form::button('CLICK AND VIEW SALARY REPORT ',['class'=>'form-control btn btn-primary','id'=>'query_salary']) }}
                                    </div>
                                </div>

                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <table class="table table-hover" id="salarytable">
                                        <thead>
                                        <th>SL</th>
                                        <th>Employee Name</th>
                                        <th>Salary</th>
                                        <th>Paid Amount</th>
                                        <th>Status</th>
                                        <th>Payment Date</th>
                                        </thead>
                                        <tbody id="salary_summary"> </tbody>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function(){
            $('#salarytable').DataTable();

            $("#datepicker").datepicker();
            $("#salarydatepicker").datepicker();
        });

        $(document).on('click','#query_salary',function () {
            var employee_id = $("#employee_id").val();
            var start = $("#start").val();
            var end = $("#end").val();
            var token = '{{ csrf_token() }}';
            var route = "{{ route('salary.search') }}";
            if (start != "" || end !=''){
                if (start<end){
                    $.ajax({
                        method:"post",
                        url:route,
                        data:{employee_id:employee_id,start:start, end:end, "_token":token},
                        dataType:"html",
                        success:function (response){
                            $("#salary_summary").html(response);
                        },
                        error:function (err) {
                            console.log(err);
                        }
                    });
                }else{
                    alert("Start should be larger then End");
                }

                /**/


            }
        });
        /* UPDATE Category END */
    </script>



@endsection