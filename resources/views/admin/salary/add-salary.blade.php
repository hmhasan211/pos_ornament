@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Salary</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Salary</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add/View Salary</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    {{ Form::open(['route'=>'salary.store','method'=>'post']) }}
                                    <div class="form-group">
                                        {{ Form::label('category','Select Category',['class'=>'label-control']) }}
                                        {{ Form::select('employee_id',$employees,true,['class'=>'form-control']) }}
                                    </div>


                                    <div class="form-group {{$errors->has('paid_date') ? 'has-error' : ''}}">
                                        {{ Form::label('category','Payment Date',['class'=>'label-control']) }}
                                        {{ Form::text('paid_date',old('paid_date'),['class'=>'form-control','id'=>'datepicker']) }}
                                        @if ($errors->has('paid_date'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('paid_date') }}</strong>
                                            </span>
                                        @endif
                                    </div>



                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::label('SubCategory','Salary Amount : ',['class'=>'control-label'])}}
                                        {{Form::text('salary',old('salary'),['class'=>'form-control','placeholder'=>'Ex: Men-Ring'])}}
                                        @if ($errors->has('salary'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('salary') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('PAYMENT SALARY',['type'=>'submit','id'=>'saveCategory','class'=>'col-sm-5 btn btn-primary']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <table class="table table-hover" id="salarytable">
                                        <thead>
                                            <th>SL</th>
                                            <th>Employee Name</th>
                                            <th>Salary</th>
                                            <th>Payment Date</th>
                                        </thead>
                                        <tbody>
                                            @php $i=0; @endphp
                                            @foreach($salaries as $salary)
                                                <tr>
                                                    <td>{{ ++$i }}</td>
                                                    <td>{{ $salary->employee->name }}</td>
                                                    <td>{{ $salary->salary }}</td>
                                                    <td>{{ $salary->paid_date }}</td>
                                                </tr>
                                           @endforeach
                                        </tbody>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function(){
            $('#salarytable').DataTable();
            $("#datepicker").datepicker();
        });
        /* UPDATE Category END */
    </script>



@endsection