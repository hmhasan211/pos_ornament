@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Income</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Income</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">View Income</h3>
                            </div>
                            <div class="panel-body">

                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <br>
                                    <br>
                                    <br>
                                    <div class="form-group">
                                        {{ Form::select('status',['0'=>'SELECT REPORT TYPE','1'=>'INCOME/RECEIVE REPORT','2'=>'EXPENSE REPORT'],false,['class'=>'form-control','id'=>'status']) }}
                                    </div>


                                    <div class="form-group">
                                        <div id="demo-dp-range">
                                            {{ Form::label("em","Select Date",['class'=>'label-control']) }}
                                            <div class="input-daterange input-group" id="datepicker">
                                                <input type="text" class="form-control" name="start" id="start" required>
                                                <span class="input-group-addon">to</span>
                                                <input type="text" class="form-control" name="end" id="end" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {{ Form::button('CLICK AND VIEW INCOME REPORT ',['class'=>'form-control btn btn-primary','id'=>'query_salary']) }}
                                    </div>
                                </div>

                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <table class="table table-hover" id="salarytable">
                                        <thead>
                                            <tr>
                                                <th>SL</th>
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th>Type</th>
                                                <th>Amount</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        <tbody id="salary_summary"> </tbody>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function(){
            $('#salarytable').DataTable();
            $("#datepicker").datepicker({
                format:"yyyy-mm-dd",
                autoclose: true,
            });
            $("#salarydatepicker").datepicker();
        });

        $(document).on('click','#query_salary',function () {
            var status = $("#status").val();
            var start = $("#start").val();
            var end = $("#end").val();
            var token = '{{ csrf_token() }}';
            console.log('Start : '+start+" - "+end);
            var route = "{{ route('incomereport.search') }}";
            if (start != "" || end !='' || status !=0){
                    $.ajax({
                        method:"post",
                        url:route,
                        data:{status:status,start:start, end:end, "_token":token},
                        dataType:"html",
                        success:function (response){
                            $("#salary_summary").html(response);
                        },
                        error:function (err) {
                            console.log(err);
                        }
                    });
            }
        });
        /* UPDATE Category END */
    </script>



@endsection