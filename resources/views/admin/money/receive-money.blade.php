@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Receive Money</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Receive Money History</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Receive Money</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    {{ Form::open(['route'=>'moneysummary.store','method'=>'post','id'=>'GoldMakerForm','enctype'=>'Multipart/form-data']) }}

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::hidden('id',null,['class'=>'form-control','id'=>'id']) }}
                                        {{ Form::label('cost','Receive Type : ',['class'=>'control-label'])}}
                                        {{ Form::select('cost_type_id',$types,true,['class'=>'form-control']) }}

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        <br>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">

                                        {{ Form::label('GoldMakerphone','Receive From  : ',['class'=>'control-label'])}}
                                        {{ Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ex: John Doe','id'=>'name'])}}
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        <br>
                                    </div>



                                    <div class="col-lg-12 col-sm-12 {{$errors->has('phone') ? 'has-error' : ''}}">

                                        {{ Form::label('GoldMakerphone','Phone : ',['class'=>'control-label'])}}
                                        {{ Form::text('phone',old('phone'),['class'=>'form-control','placeholder'=>'+880-1***-******','id'=>'phone'])}}
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                        <br>
                                    </div>



                                    <div class="col-lg-12 col-sm-12 {{$errors->has('amount') ? 'has-error' : ''}}">

                                        {{ Form::label('GoldMakerphone','Receive Amount ( Tk ) : ',['class'=>'control-label'])}}
                                        {{ Form::text('amount',old('amount'),['class'=>'form-control','placeholder'=>'Ex: 100 tk','id'=>'amount'])}}
                                        @if ($errors->has('amount'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('amount') }}</strong>
                                            </span>
                                        @endif
                                        <br>
                                    </div>


                                    <div class="col-lg-12 col-sm-12 {{$errors->has('note') ? 'has-error' : ''}}">

                                        {{ Form::label('GoldMakerphone','Note : ',['class'=>'control-label'])}}
                                        {{ Form::textarea('note',old('note'),['class'=>'form-control','placeholder'=>'Ex: Money Receive Note.','id'=>'amount','rows'=>4,'cols'=>5])}}
                                        @if ($errors->has('note'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('note') }}</strong>
                                            </span>
                                        @endif
                                    </div>


                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                        {{ Form::button('SAVE RECEIVE TK',['type'=>'submit','id'=>'saveGoldMaker','class'=>'btn btn-primary']) }}


                                    </div>
                                    {{ Form::close() }}

                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                    <table class="table table-bordered table-striped" id="brandTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Type</th>
                                            <th>Amount</th>
                                            <th>Note</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0;$total=0; @endphp
                                        @foreach($received as $info)

                                            <tr>
                                                <td>{{++$i}}</td>
                                                <td>{{ $info->name }}</td>
                                                <td>{{ $info->phone }}</td>
                                                <td>{{ $info->cost->name }}</td>
                                                <td>{{$info->amount}}</td>
                                                <td>{{$info->note}}</td>
                                                <td> {{ $info->created_at }} </td>
                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#brandTable').DataTable();
            $("#cancel").hide();
        });

        /* edit request start */
        $(document).on('click','.edit',function(){
            $("#cancel").show(200);

            $("#cancel").attr("type","button");
            var id = $(this).attr('data-id');
            var name = $("#name"+id).attr('data-id');
            var phone = $("#phone"+id).attr('data-id');
            var address = $("#address"+id).attr('data-id');
            var route = '{{ route("goldmaker.update") }}';
            var img = $("#image"+id).attr('data-id');
            $("#name").val(name);
            $("#phone").val(phone);
            $("#address").val(address);
            $("#id").val(id);
            $('#GoldMakerForm').attr('action',route);
            $("#saveGold Maker").text("Update Gold Maker");
            $("#saveGold Maker").attr('class','btn btn-info');
            $("#cancel").attr('type','reset');

            $("#oldimage").attr('src',"{{url('public/admin/goldmaker')}}"+"/"+img);
        });

        $("#cancel").click(function(){
            var route = '{{ route("goldmaker.add") }}';
            $("#GoldMakerForm").attr('action',route);
            $("#id").val("");
            $("#saveGold Maker").text("Add Gold Maker");
            $("#saveGold Maker").attr('class','btn btn-primary');
            $(this).hide(299)
        });
        /* edit request end */
    </script>

@endsection