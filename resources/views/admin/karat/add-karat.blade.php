@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View karat</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">karat</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New karat</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <div class="col-lg-12 col-sm-12 {{$errors->has('karat_size') ? 'has-error' : ''}}">
                                        {{ Form::open(['route'=>'karat.store','method'=>'post','enctype'=>'multipart/form-data','id'=>'karatForm']) }}
                                        {{ Form::label('karat','Karat Size : ',['class'=>'control-label'])}}
                                        {{Form::text('karat_size',old('karat_size'),['class'=>'form-control','placeholder'=>'Ex: Mikimoto'])}}
                                        @if ($errors->has('karat_size'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('karat_size') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('SAVE KARAT',['type'=>'submit','id'=>'savekarat','class'=>'btn btn-primary']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                    <table class="table table-bordered table-striped" id="karatTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>karat</th>
                                            <th>Action </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                        @foreach($karat as $info)
                                            <tr >
                                                <td>{{++$i}}</td>
                                                <td >{{$info->karat_size}}</td>
                                                <td>
                                                    <a href="{{route('karat.edit',$info->id)}}" class="btn btn-sm btn-info edit" ><i class="demo-pli-pen-5"></i></a> ||
                                                    <button class="btn btn-sm btn-danger erase" data-id="{{$info->id}}" data-url="{{url('Karat/erase')}}"><i class="demo-pli-trash"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#karatTable').DataTable();

        });

        /* UPDATE karat END */

    </script>



@endsection