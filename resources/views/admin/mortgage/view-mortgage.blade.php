@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow"> View Mortgage</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Mortgage</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"> Mortgage</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                    <table class="table table-bordered table-striped" id="CategoryTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Inv.No</th>
                                            <th>Name</th>
                                            <th>Price </th>
                                            <th>Amount Due </th>
                                            <th>Interest </th>
                                            <th>Vori-Ana-Roti </th>
                                            <th>Gram</th>

                                            <th>Image</th>
                                            <th>Type</th>
                                           {{--  <th>Action</th> --}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; $vg=0;$ag=0;$rg=0;$mg=0;   $total=0; @endphp

                                        @foreach($mortgages as $report)

                                            <tr>
                                                <td> {{ ++$i }}</td>
                                                <td> {{ $report->mortgage_invoice->invoice_no }}</td>
                                                <td> {{ $report->Product->product_name }} </td>

                                                <td> {{ $report->total_price }} </td>
                                                <td> {{ $report->mortgage_invoice->total_balance }} </td>
                                                <td> {{ $report->mortgage_invoice->interest }} </td>
                                                <td> ভরি = {{ $report->vori}}  <br> আনা = {{  $report->ana }} <br>  রতি = {{ $report->roti }}                                                     @if($report->miliroti == 5)
                                                         - (1/2)
                                                    @else
                                                        ""
                                                    @endif
                                                </td>
                                                <td>
                                                    <?php

                                                        $vori = $report->vori;
                                                        $ana = $report->ana;
                                                        $roti = $report->roti;
                                                        $miliroti = $report->miliroti;



                                                        if($vori !=0){
                                                            $vg = 11.664*$vori;
                                                        }else{

                                                        }

                                                        if($ana !=0){
                                                            $ag = $ana*0.73;
                                                        }else{
                                                            $ag=0;
                                                        }

                                                        if($roti !=0){
                                                            $rg = $roti*0.1215;
                                                        }else{
                                                            $rg=0;
                                                        }

                                                        if($miliroti !=0){
                                                            $mg = $miliroti*0.01215;
                                                        }else{
                                                            $mg=0;
                                                        }

                                                       echo $vg+$ag+$rg+$mg." gm";


                                                    ?>
                                                </td>


                                                <td><img src="{{ url("public/admin/product/upload/".$report->Product->product_image) }}" style="height: 80px;width: 80px;"></td>
                                                <td> {{ $report->Product->gold_type->name }} </td>
                                                {{-- <td>
                                                    <button type="button" class="fa fa-edit btn btn-primary"></button>
                                                    ||
                                                    <button type="button" class="fa fa-edit btn btn-danger"></button>
                                                </td> --}}

                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#CategoryTable').DataTable();

        });

        $(document).on('click','.activestatus',function(){
            var id = $(this).attr('data-id');
            $('#product_status').modal('show');
            $("#status_id").val(id);
            console.log("Fire Console : "+$(this).attr('data-id'));
        });

        /* UPDATE Category END */

    </script>




@endsection