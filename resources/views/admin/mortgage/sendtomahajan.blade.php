@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Send product to Mahajan</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Mahajan</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Send New Product</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    {{ Form::open(['route'=>'sendtomahajan.store','method'=>'post','id'=>'supplierForm']) }}

                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12 {{$errors->has('mahajan_receipt_no') ? 'has-error' : ''}}">

                                            {{ Form::label('mahajan_receipt_no','Receipt No : ',['class'=>'control-label'])}}
                                            {{ Form::text('mahajan_receipt_no',null,['class'=>'form-control','placeholder'=>'Ex: Amount','id'=>'mh_receipt'])}}
                                            @if ($errors->has('mahajan_receipt_no'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('mahajan_receipt_no') }}</strong>
                                                </span>
                                            @endif
                                            <hr>
                                        </div>
                                        {{--<div class="col-lg-6 col-sm-12 {{$errors->has('invoice_no') ? 'has-error' : ''}}">
                                            {{ Form::label('invoice_id','Select Invoice : ',['class'=>'control-label'])}}
                                            {{ Form::hidden('status',1,['class'=>'form-control'])}}
                                            {{ Form::select('invoice_id',$invoice,null,['class'=>'form-control select2','placeholder'=>'Select Invoice Number','id'=>'invoice'])}}
                                            @if ($errors->has('invoice_no'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                            <hr>
                                        </div>--}}
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-12 {{$errors->has('mahajan_id') ? 'has-error' : ''}}">
                                            {{ Form::label('mahajan_id','Select Mahajan : ',['class'=>'control-label'])}}
                                            {{ Form::select('mahajan_id',$mahajan,null,['class'=>'form-control select2','placeholder'=>'Select Mahajan','id'=>'mahajan'])}}
                                            @if ($errors->has('mahajan_id'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('mahajan_id') }}</strong>
                                                </span>
                                            @endif
                                            <hr>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 {{$errors->has('employee_id') ? 'has-error' : ''}}">
                                            {{ Form::label('employee_id','Select Employee : ',['class'=>'control-label'])}}
                                            {{ Form::select('employee_id',$employee,null,['class'=>'form-control select2','placeholder'=>'Select Employee','id'=>'employee_id'])}}
                                            @if ($errors->has('employee_id'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('employee_id') }}</strong>
                                                </span>
                                            @endif
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-12 {{$errors->has('amount') ? 'has-error' : ''}}">

                                            {{ Form::label('amount','Amount : ',['class'=>'control-label'])}}
                                            {{ Form::number('amount',null,['class'=>'form-control','placeholder'=>'Ex: Amount','id'=>'amount'])}}
                                            @if ($errors->has('amount'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('amount') }}</strong>
                                                </span>
                                            @endif
                                            <hr>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 {{$errors->has('interest') ? 'has-error' : ''}}">

                                            {{ Form::label('interest','Interest : ',['class'=>'control-label'])}}
                                            {{ Form::number('interest',null,['class'=>'form-control','placeholder'=>'Ex: Amount','id'=>'interest'])}}
                                            @if ($errors->has('amount'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('interest') }}</strong>
                                                </span>
                                            @endif
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12 {{$errors->has('description') ? 'has-error' : ''}}">

                                            {{ Form::label('description','Product Description : ',['class'=>'control-label'])}}
                                            {{ Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Ex: Product Description','id'=>'pro_desc'])}}
                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-12 {{$errors->has('in_date') ? 'has-error' : ''}}">

                                            {{ Form::label('in_date','Send Date : ',['class'=>'control-label'])}}
                                            {{ Form::date('in_date',null,['class'=>'form-control common-datepicker','id'=>'in_date'])}}
                                            @if ($errors->has('in_date'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('in_date') }}</strong>
                                                </span>
                                            @endif
                                            <hr>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 {{$errors->has('out_date') ? 'has-error' : ''}}">

                                            {{ Form::label('out_date','Receive Date : ',['class'=>'control-label'])}}
                                            {{ Form::date('out_date',null,['class'=>'form-control common-datepicker','placeholder'=>'Ex: Amount','id'=>'interest'])}}
                                            @if ($errors->has('in_date'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('out_date') }}</strong>
                                                </span>
                                            @endif
                                            <hr>
                                        </div>
                                    </div>


                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('Send to Mahajan',['type'=>'submit','id'=>'send_to_mahajan','class'=>'btn btn-primary']) }}
                                        {{ Form::hidden('canceledit',"Cancel Edit",['type'=>'reset','id'=>'cancel','class'=>'btn btn-danger']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12 table-responsive">
                                    <table class="table table-bordered table-striped" id="brandTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Receipt No</th>
                                            <th>Mahajan Name</th>

                                            <th>amount</th>
                                            <th>Interest</th>
                                            <th>Send Date</th>
                                            <th>Receive Date</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                            @foreach($send as $info)
                                                <tr id="rowid{{$info->id}}" class="abcd">
                                                    <td>{{++$i}}</td>
                                                    <td id="receipt_no{{ $info->id }}" data-id="{{ $info->mahajan_receipt_no }}">{{$info->mahajan_receipt_no}}</td>
                                                    <td id="name{{ $info->id }}" data-id="{{ $info->mahajan->name }}">{{$info->mahajan->name}}</td>
                                                    <td id="amount{{ $info->id }}" data-id="{{ $info->amount }}">{{$info->amount}}</td>
                                                    <td id="interest{{ $info->id }}" data-id="{{ $info->interest }}">{{ $info->interest }}</td>
                                                    <td id="in_date{{ $info->id }}" data-id="{{ $info->in_date }}">{{$info->in_date}}</td>
                                                    <td id="out_date{{ $info->id }}" data-id="{{ $info->out_date }}">{{ $info->out_date }} </td>

                                                    <td>
                                                        <a href="{{route('sendtomahajan.view',$info->id)}}" class="btn btn-sm btn-info" data-id="{{ $info->id }}"><i class="fa fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        $('.select2').select2();
        $('.common-datepicker').datepicker({
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });

        $("#cancel").click(function(){
            var route = '{{ route("mahajan.add") }}';
            $("#supplierForm").attr('action',route);
            $("#id").val("");
            $("#savesupplier").text("Add Mahajan");
            $("#savesupplier").attr('class','btn btn-primary');
            $(this).hide(299)
        });
        /* edit request end */


    </script>

@endsection