@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Receive product From Mahajan</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Mahajan</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Receive Product</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    {{ Form::open(['route'=>'receive_from_mahajan_save','method'=>'post','id'=>'supplierForm']) }}

                                    <div class="col-lg-6 col-sm-12 {{$errors->has('mahajan_receipt_no') ? 'has-error' : ''}}">

                                        {{ Form::label('mahajan_receipt_no','Receipt No : ',['class'=>'control-label'])}}
                                        {{--{{ Form::select('mahajan_receipt_no',$invoice,null,['class'=>'form-control','placeholder'=>'Select Receipt No','id'=>'mh_receipt'])}}--}}
                                        <select name="mahajan_receipt_no" class="form-control select2" id="mh_receipt">
                                            <option>Select Receipt</option>
                                            @foreach($invoice as $inv)
                                                <option value="{{$inv->mahajan_receipt_no}}">{{$inv->mahajan_receipt_no.'---'.$inv->mahajan->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('mahajan_receipt_no'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('mahajan_receipt_no') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>


                                    <div class="col-lg-6 col-sm-12 {{$errors->has('mahajan_id') ? 'has-error' : ''}}">
                                        {{ Form::label('mahajan_id','Mahajan : ',['class'=>'control-label'])}}
                                        {{ Form::text('mohajan',null,['class'=>'form-control','placeholder'=>'Select Mahajan','id'=>'mahajan','disabled'])}}


                                        {{ Form::hidden('mahajan_id',old('mahajan_id'),['id'=>'mahajan_id']) }}
                                        @if ($errors->has('mahajan_id'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>


                                    <div class="col-lg-6 col-sm-12 {{$errors->has('amount') ? 'has-error' : ''}}">

                                        {{ Form::label('amount','Amount : ',['class'=>'control-label'])}}
                                        {{ Form::number('amount',null,['class'=>'form-control','placeholder'=>'Ex: Amount','id'=>'amount','disabled'])}}
                                        @if ($errors->has('amount'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('amount') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>



                                    <div class="col-lg-6 col-sm-12 {{$errors->has('interest') ? 'has-error' : ''}}">

                                        {{ Form::label('interest','Interest : ',['class'=>'control-label'])}}
                                        {{ Form::number('interest',null,['class'=>'form-control','placeholder'=>'Ex: Amount','id'=>'interest','disabled'])}}
                                        @if ($errors->has('amount'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('interest') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>
                                    <div class="col-lg-12 col-sm-12 {{$errors->has('description') ? 'has-error' : ''}}">

                                        {{ Form::label('description','Product Description : ',['class'=>'control-label'])}}
                                        {{ Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Ex: Product Description','id'=>'pro_desc','rows'=>3])}}
                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>

                                    <div class="col-lg-6 col-sm-12 {{$errors->has('employee_id') ? 'has-error' : ''}}">
                                        {{ Form::label('employee_id','Select Employee : ',['class'=>'control-label'])}}
                                        {{ Form::select('employee_id',$employee,null,['class'=>'form-control select2','id'=>'employee_id','placeholder'=>'Select employee'])}}
                                        @if ($errors->has('employee_id'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>

                                    <div class="col-lg-6 col-sm-12 {{$errors->has('out_date') ? 'has-error' : ''}}">

                                        {{ Form::label('out_date','Receive Date : ',['class'=>'control-label'])}}
                                        {{ Form::date('out_date',null,['class'=>'form-control common-datepicker','placeholder'=>'Ex: Amount','id'=>'interest'])}}
                                        @if ($errors->has('in_date'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('in_date') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>
                                    <div class="col-lg-6 col-sm-12 {{$errors->has('paid_amount') ? 'has-error' : ''}}">

                                        {{ Form::label('amount','Paid Amount : ',['class'=>'control-label'])}}
                                        {{ Form::number('paid_amount',null,['class'=>'form-control','placeholder'=>'Ex: Amount','id'=>'paid_amount'])}}
                                        @if ($errors->has('amount'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('paid_amount') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>

                                    <div class="col-md-6 col-xs-12">
                                        <br>
                                        {{ Form::button('Receive From Mahajan',['type'=>'submit','id'=>'send_to_mahajan','class'=>'btn btn-primary']) }}
                                        {{ Form::hidden('canceledit',"Cancel Edit",['type'=>'reset','id'=>'cancel','class'=>'btn btn-danger']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12 table-responsive">
                                    <table class="table table-bordered table-striped" id="brandTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Receipt No</th>
                                            <th>Mahajan Name</th>
                                            <th>amount</th>


                                            <th>Received Date</th>
                                            {{--<th>Action</th>--}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                        @foreach($receive as $info)

                                            <tr id="rowid{{$info->id}}" class="abcd">
                                                <td>{{++$i}}</td>
                                                <td id="receipt_no{{ $info->id }}"
                                                    data-id="{{ $info->mahajan_receipt_no }}">{{$info->mahajan_receipt_no}}</td>
                                                <td id="name{{ $info->id }}"
                                                    data-id="{{ $info->mahajan->name }}">{{$info->mahajan->name}}</td>
                                                <td id="amount{{ $info->id }}"
                                                    data-id="{{ $info->paid_amount }}">{{$info->paid_amount}}</td>


                                                <td id="out_date{{ $info->id }}"
                                                    data-id="{{ $info->out_date }}">{{ $info->out_date }} </td>

                                                {{--<td>
                                                    <a href="{{route('sendtomahajan.view',$info->id)}}"
                                                       class="btn btn-sm btn-info" data-id="{{ $info->id }}"><i
                                                                class="fa fa-edit"></i></a>
                                                </td>--}}
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        $('.common-datepicker').datepicker({
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });
        $('.select2').select2();

        $(document).on('change','#mh_receipt',function(){
            var id = $(this).val();
            var token = "{{ csrf_token() }}";
            $.ajax({
                method:"post",
                url:"{{route('get_mortgage_info')}}",
                data:{id:id,"_token":token},
                dataType:'json',
                success:function(response){
                    console.log(response.mahajan['name']);
                    $('#amount').val(response.amount);
                    $('#paid_amount').val(response.amount);
                    $('#interest').val(response.interest);
                    $('#pro_desc').val(response.description);
                    $('#mahajan').val(response.mahajan['name']);
                    $("#mahajan_id").val(response.mahajan_id);
                },
                error:function(err){console.log("Error List : "+err);}
            });

        });
        /* edit request end */


    </script>

@endsection