@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add New Mortgage</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Mortgage</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">

                    <div class="panel">

                        <div class="panel-heading">
                            <h3 class="panel-title">Add New Mortgage</h3>
                        </div>


                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                    <div class="row">

                                        <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('product_code') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product','Product Code : ',['class'=>'control-label'])}}
                                            {{ Form::select('product_code',$product_code,null,['class'=>'form-control product_code_select2','required','id'=>'product_code','multiple'])}}

                                        </div>
                                        <!--  PRODUCT NAME  -->
                                        <div class="col-lg-9 col-sm-9 col-xs-12 {{$errors->has('product_name') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product','Product Name ( পণ্যের নাম )   : ',['class'=>'control-label'])}}
                                            {{ Form::select('product_name',$product_name,null,['class'=>'form-control product_name_select2','required','id'=>'product_name','multiple'])}}
                                        </div>
                                        <!-- / PRODUCT NAME  -->
                                    </div> {{-- Product COde and Product Name Search Row End --}}


                                    <div class="row">
                                        <!-- Origin Select Start -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('Vori','Vori ( ভরি )  : ',['class'=>'control-label'])}}
                                            {{ Form::number('vori',0,['id'=>'vori','class'=>'form-control','min'=>0]) }}
                                        </div>
                                        {{-- vori end --}}


                                        {{-- Anaa start --}}
                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('anaa','Anaa ( আনা ) : ',['class'=>'control-label'])}}
                                            {{--{{ Form::number('anaa',0,['id'=>'ana','class'=>'form-control','min'=>0]) }}--}}

                                            <select class="form-control" id="ana">
                                                <option value="0">আনা সিলেক্ট করুন</option>
                                                <option value="1" data-image="{{ url('public/fontimage/01.jpg') }}">১
                                                    আনা
                                                </option>
                                                <option value="2" data-image="{{ url('public/fontimage/02.jpg') }}">২
                                                    আনা
                                                </option>
                                                <option value="3" data-image="{{ url('public/fontimage/03.jpg') }}">৩
                                                    আনা
                                                </option>
                                                <option value="4" data-image="{{ url('public/fontimage/04.jpg') }}">৪
                                                    আনা
                                                </option>
                                                <option value="5" data-image="{{ url('public/fontimage/05.jpg') }}">৫
                                                    আনা
                                                </option>
                                                <option value="6" data-image="{{ url('public/fontimage/06.jpg') }}">৬
                                                    আনা
                                                </option>
                                                <option value="7" data-image="{{ url('public/fontimage/07.jpg') }}">৭
                                                    আনা
                                                </option>
                                                <option value="8" data-image="{{ url('public/fontimage/08.jpg') }}">৮
                                                    আনা
                                                </option>
                                                <option value="9" data-image="{{ url('public/fontimage/09.jpg') }}">৯
                                                    আনা
                                                </option>
                                                <option value="10" data-image="{{ url('public/fontimage/10.jpg') }}">
                                                    ১০আনা
                                                </option>
                                                <option value="11" data-image="{{ url('public/fontimage/11.jpg') }}">১১
                                                    আনা
                                                </option>
                                                <option value="12" data-image="{{ url('public/fontimage/12.jpg') }}">১২
                                                    আনা
                                                </option>
                                                <option value="13" data-image="{{ url('public/fontimage/13.jpg') }}">
                                                    ১৩আনা
                                                </option>
                                                <option value="14" data-image="{{ url('public/fontimage/14.jpg') }}">১৪
                                                    আনা
                                                </option>
                                                <option value="15" data-image="{{ url('public/fontimage/15.jpg') }}">
                                                    ১৫ আনা
                                                </option>
                                            </select>

                                        </div>
                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('Roti','Roti ( রতি )  : ',['class'=>'control-label'])}}
                                            <select class="form-control" id="roti">
                                                <option value="0">রতি সিলেক্ট করুন</option>
                                                <option value="1" data-image="{{ url('public/fontimage/roti/1.jpg') }}">
                                                    ১ রতি
                                                </option>
                                                <option value="2" data-image="{{ url('public/fontimage/roti/2.jpg') }}">
                                                    ২ রতি
                                                </option>
                                                <option value="3" data-image="{{ url('public/fontimage/roti/3.jpg') }}">
                                                    ৩ রতি
                                                </option>
                                                <option value="4" data-image="{{ url('public/fontimage/roti/4.jpg') }}">
                                                    ৪ রতি
                                                </option>
                                                <option value="5" data-image="{{ url('public/fontimage/roti/5.jpg') }}">
                                                    ৫ রতি
                                                </option>

                                            </select>
                                        </div>

                                    </div> {{-- Upper Row End here --}}

                                    {{-- Product Qty Price and Rate Here --}}

                                    <div class="row">
                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('mili',' আধা রতি  : ',['class'=>'label-control']) }}

                                            <select class="form-control" id="miliroti">
                                                <option value="0">মিলি রতি সিলেক্ট করুন</option>
                                                <option value="1"
                                                        data-image="{{ url('public/fontimage/miliroti/1.jpg') }}">
                                                    ১ পয়েন্ট
                                                </option>
                                                <option value="2"
                                                        data-image="{{ url('public/fontimage/miliroti/2.jpg') }}">
                                                    ২ পয়েন্ট
                                                </option>
                                                <option value="3"
                                                        data-image="{{ url('public/fontimage/miliroti/3.jpg') }}">
                                                    ৩ পয়েন্ট
                                                </option>
                                                <option value="4"
                                                        data-image="{{ url('public/fontimage/miliroti/4.jpg') }}">
                                                    ৪ পয়েন্ট
                                                </option>
                                                <option value="5"
                                                        data-image="{{ url('public/fontimage/miliroti/5.jpg') }}">
                                                    ৫ পয়েন্ট
                                                </option>

                                                <option value="6"
                                                        data-image="{{ url('public/fontimage/miliroti/6.jpg') }}">
                                                    ৬ পয়েন্ট
                                                </option>
                                                <option value="7"
                                                        data-image="{{ url('public/fontimage/miliroti/7.jpg') }}">
                                                    ৭ পয়েন্ট
                                                </option>
                                                <option value="8"
                                                        data-image="{{ url('public/fontimage/miliroti/8.jpg') }}">
                                                    ৮ পয়েন্ট
                                                </option>
                                                <option value="9"
                                                        data-image="{{ url('public/fontimage/miliroti/9.jpg') }}">
                                                    ৯ পয়েন্ট
                                                </option>

                                            </select>

                                        </div>
                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product','Gram : ',['class'=>'control-label'])}}
                                            {{ Form::number('gram',old('gram'),['id'=>'gram','class'=>'form-control','placeholder'=>'Ex: 5 Gm','readonly'])}}
                                            {{Form::hidden('product_qty','1',['id'=>'product_qty','required','class'=>'form-control','placeholder'=>'Ex: 10'])}}
                                        </div>
                                        {{--<div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>

                                            {{ Form::label('productqty','Quantity : ',['class'=>'control-label'])}}
                                            {{Form::number('prdocut_qty','1',['id'=>'product_qty','required','class'=>'form-control','placeholder'=>'Ex: 10'])}}
                                        </div>--}}
                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product_price','Price : ',['class'=>'control-label'])}}
                                            {{Form::number('product_price',old('product_price'),['id'=>'product_price','required','class'=>'form-control','placeholder'=>'Ex: 5000 tk','min'=>1])}}
                                        </div>
                                    </div>
                                    <div class="row">


                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product_price_total','Total Price',['class'=>'control-label'])}}
                                            {{Form::number('product_total',old('product_price_total'),['id'=>'product_rate','required','class'=>'form-control','placeholder'=>'Ex: 5000 tk','min'=>1])}}
                                        </div>
                                    </div>
                                    {{-- Roti end --}}


                                    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        <div class="form-group">
                                            <button class="btn btn-info form-control" type="button" id="add_bucket">
                                                Add To Bucket
                                            </button>
                                        </div>
                                    </div>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <h1 class="text-center bg-primary">Bucket List</h1>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>নাম</th>
                                            <th>গ্রাম</th>
                                            <th>পরিমাণ</th>
                                            <th>টাকা</th>
                                            <th>মুছুন</th>
                                        </tr>
                                        </thead>
                                        <tbody id="all_cart_item">

                                        </tbody>
                                    </table>
                                    <hr>


                                </div>

                                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 pull-right">



                                    {{-- suplier information added start from here --}}

                                    {{ Form::open(['route'=>'mortgage.save','method'=>'post','enctype'=>'multipart/form-data']) }}
                                    <div class="row">
                                        <h4 class="text-center bg-primary" style="padding: 8px 0">Customer Information
                                            : </h4>
                                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                            {{ Form::label('supplier_id','Search Customer',['class'=>'label-control']) }}
                                            {{ Form::select('customer_id',$customers,false,['id'=>'supplier_id','class'=>'form-control supplier_id_select2','multiple'=>'multiple']) }}
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12 {{ $errors->has('supplier_name') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('name','customer Name : ',['class'=>'label-control']) }}
                                            {{ Form::text('name',null,['class'=>'form-control','id'=>'supplier_name','placeholder'=>'Ex. Mr.xyz']) }}

                                            @if($errors->has('supplier_name'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('supplier_name') }}</strong>
                                                </span>
                                            @endif

                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12 {{ $errors->has('phone') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('phone','Customer Phone : ',['class'=>'label-control']) }}
                                            {{ Form::text('phone',null,['class'=>'form-control','placeholder'=>'Ex : 923584596','id'=>'supplier_phone']) }}
                                            @if($errors->has('phone'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{-- supplier image start --}}
                                        <div class="col-lg-6 col-sm-6 col-xs-12 {{ $errors->has('supplierimage') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('image','Customer Image : ',['class'=>'label-control']) }}
                                            {{ Form::file('image',['class'=>'form-control']) }}
                                            @if($errors->has('supplierimage'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('supplierimage') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        {{-- supplier image end --}}

                                        {{-- supplier image start --}}
                                        <div class="col-lg-6 col-sm-6 col-xs-12 {{ $errors->has('supplierimage') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('customerphone','Old Image : ',['class'=>'label-control']) }}
                                            <img class="form-control" height="80" width="80">
                                        </div>
                                        {{-- supplier image end --}}

                                        <div class="col-lg-12 col-sm-12 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('customeraddress','Supplier Address : ',['class'=>'label-control']) }}
                                            {{ Form::textarea('Supplier_address',null,['id'=>'supplier_address','class'=>'form-control','placeholder'=>'New York City','rows'=>5,'cols'=>'10']) }}
                                        </div>
                                    </div>


                                    {{-- Suplier information added end from here --}}

                                    <div class="row">
                                        <h4 class="text-center bg-primary" style="padding: 8px 0">Buy Information
                                            : </h4>
                                        <div class="col-lg-12 col-sm-12 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            <div class="row">
                                                {{ Form::label('customername','Customer Note : ',['class'=>'label-control']) }}
                                                {{ Form::textarea('sale_note','Thank you so much for stay with Ringer-Soft jewellery Shop',['class'=>'form-control','rows'=>2,'cols'=>8,'placeholder'=>'Thank you so much for buy from here']) }}
                                            </div>
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            <div class="row">
                                                {{ Form::label('customername','Payment Type : ',['class'=>'label-control']) }}
                                                {{ Form::select('payment_type',[''=>'SELECT PAYMENT TYPE','cash'=>'CASH','bkash'=>'Bkash','card'=>'Credit Card','check'=>'Bank Check'],false,['class'=>'form-control','id'=>'payment_type','required']) }}
                                                <hr>
                                                {{ Form::text('bkash_code','',['class'=>'form-control bg-primary text-center','id'=>'bkash_code','placeholder'=>'Ex: T5x1G2']) }}
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-sm-12 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            <table class="table-hover table table-bordered table-responsive">
                                                <tbody>


                                                {{-- Total amount of cart loop end from here --}}
                                                <tr>
                                                    <td><span class="btn btn-primary">Total Amount : </span></td>
                                                    <td>
                                                        {{ Form::number('total_amount',null,['class'=>'form-control','id'=>'total_amount','placeholder'=>'100 tk','readonly']) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="btn btn-primary">Total Less : </span></td>
                                                    <td>
                                                        {{ Form::number('less',null,['class'=>'form-control','id'=>'less','placeholder'=>'100 tk']) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="btn btn-primary"> Total Paid : </span></td>
                                                    <td>
                                                        {{ Form::text('total_paid',null,['class'=>'form-control','id'=>'paid','placeholder'=>'Total Paid : 100 tk','required']) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="btn btn-primary"> Total Interest : </span></td>
                                                    <td>
                                                        {{ Form::text('total_interest',null,['class'=>'form-control','id'=>'total_interest','placeholder'=>'1200','required']) }}
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td><span class="btn btn-primary"> Total Amount to be paid : </span></td>
                                                    <td>
                                                        {{ Form::number('total_balance',null,['class'=>'form-control','id'=>'balance','placeholder'=>' 100 tk','readonly']) }}
                                                    </td>
                                                </tr>


                                                <tr id="payment_date">
                                                    <td><span class="btn btn-primary"> Payment Date : </span></td>
                                                    <td>
                                                        <input type="text" name="payment_deadline"
                                                               class="form-control pull-right pdate" id="datepicker">
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        {{ Form::button('Add Mortgage ',['type'=>'submit','class'=>'btn btn-primary']) }}
                                                    </td>
                                                </tr>

                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                    {{--/ Buy and Supllier Information end here  /--}}


                                </div>
                            </div>
                        </div><!--  Panel Body Close -->

                    </div><!-- Panel Close -->

                </div><!-- Row Close -->
            </div><!-- Page Content Close -->
        </div>
    </div>



    <script>
        var global_price;
        var amount = 0;
        var amount = "{{ Cart::total() }}";

        $(document).ready(function () {

            $("#datepicker").datepicker({
                minDate: 0,
            });
            $("#pmt_date").datepicker({
                minDate: 0,
                format: 'yyyy/mm/dd',
            });

            $("#payment_date").hide();

            $("#ana").msDropdown();
            $("#roti").msDropdown();
            $("#miliroti").msDropdown();

            /* at first auto hide weight amount ... it will be show when weight value found either it will be hide */
            $("#tamount").val(amount);
            $("#weight_info").css("display", "none");
            /* at first auto hide weight amount ... it will be show when weight value found either it will be hide */

            /* All added cart items showing with this functions */

            Carts();

            /* All added cart items showing with this functions */

            global_price = 0;
            product_code();
            $("#bkash_code").hide();
            $(".product_code_select2").select2({
                maximumSelectionLength: 1
            });

            $(".weight_id_select2").select2({tags: true, maximumSelectionLength: 1});
            $(".product_name_select2").select2({maximumSelectionLength: 1});

            $(".supplier_id_select2").select2({tags: true, maximumSelectionLength: 1});

        });

        /* when product code empty then automatic other field will be empty */




        /* suppliers search start */

        $(document).on('change', '#supplier_id', function () {
            var supplier_id = $(this).val();
            var token = "{{ csrf_token() }}";
            $.ajax({
                method: "post",
                url: "{{route('supplier.search')}}",
                data: {supplier_id: supplier_id, "_token": token},
                dataType: 'json',
                success: function (response) {
                    $("#supplier_name").val(response[0].name);
                    $("#supplier_phone").val(response[0].phone);
                    $("#supplier_address").val(response[0].address);
                    console.log("Response List : " + response[0].id);
                },
                error: function (err) {
                    console.log("Error List : " + err);
                }
            });

        });

        /* suppliers search end */

        function product_code() {
            var p_code = $("#product_code").val();
            if (p_code == "") {
                $('#product_name').val();
                $('#product_price').val();
                $('#product_local_name').val();
                $('#product_details').val();
                /* product price multple quantity equal Total Price */
                $("#tamount").val();
            }
        }

        /*
         ========== product information search with Product Code ===========
         */

        $(document).on('change', '#product_code', function () {
            var product_code_id = $(this).val();
            var token = "{{ csrf_token() }}";
            if (product_code_id != '') {
                $.ajax({
                    method: "post",
                    url: "{{ route('product.find') }}",
                    data: {product_code_id: product_code_id, "_token": token},
                    dataType: "json",
                    success: function (response) {

                        var parsedata = JSON.parse(response);

                        console.log(parsedata.brand);

                        $("#pid").val(parsedata.id);

                        $('.product_code_select2').select2({tags: true, maximumSelectionLength: 1});

                        global_price = parsedata.product_price;

                        $('#product_name').val(parsedata.id).attr('selected', true);

                        $('.product_name_select2').select2({tags: true, maximumSelectionLength: 1});


                        $('#product_price').val(parsedata.product_price);
                        $("#product_qty").val(1);

                        $("#size_id").val(parsedata.size);
                        $("#color_id").val(parsedata.color);
                        $("#karat_id").val(parsedata.karat);
                        $("#origin_id").val(parsedata.origin);
                        $("#gold_type_id").val(parsedata.goldtype);


                        $("#brand_id").val(parsedata.brand);
                        $("#category_id").val(parsedata.category);
                        $("#subcategory_id").val(parsedata.subcategorie);
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            } else {
                product_code();
            }
        });

        /*
         ========== product information search with Product Code ===========
         */
        /* Calculate according to Vori start */

        $(document).on('keyup change', '#vori', function () {
            var vori = $(this).val();
            var ana = $("#ana").val();
            var roti = $("#roti").val();
            var mroti = $("#miliroti").val();
            var rate = $("#product_rate").val();
            var vori_price = $("#product_price").val();
            if (vori == 0) {
                $(this).addClass("bg-danger").val(0);
                $("#product_rate").val(0);
            }
            else {
                $(this).removeClass("bg-danger");
                if (vori > 0 && ana == 0 && roti == 0 && mroti == 0) {
                    var total = Number(vori) * Number(vori_price);

                    $("#product_rate").val(total.toFixed(2));
                    var gram = vori * 11.664;
                    $("#gram").val(gram);

                } else if (vori > 0 && ana > 0 && roti == 0 && mroti == 0) {
                    var per_ana_price = Number(vori_price) / 16;
                    var total_ana = Number(vori) * 16 + Number(ana);
                    var total_ana_price = Number(per_ana_price) * Number(total_ana);
                    $("#product_rate").val(total_ana_price.toFixed(2));

                    var gram_ana = 11.664 / 16;
                    var total_gram_ana = gram_ana * ana;
                    var t = Number(vori) * 11.664 + Number(total_gram_ana);
                    $("#gram").val(t.toFixed(2));


                    console.log("Per Ana Gram = " + gram_ana);
                    console.log("Total Ana Gram = " + total_gram_ana);


                } else if (vori > 0 && ana > 0 && roti > 0 && mroti == 0) {

                    $per_vori_roti_price = Number(vori_price) / (16 * 6);
                    /* per vori roti amount start */
                    $total_roti = Number(vori) * 16 * 6 + (Number(ana) * 6) + Number(roti);
                    $total_roti_price = $total_roti * $per_vori_roti_price;
                    $("#product_rate").val($total_roti_price.toFixed(2));


                    var t = Number(vori) * 11.664 + Number(ana) * 0.73 + Number(roti) * 0.1215;

                    $("#gram").val(t.toFixed(2));
                    console.log("Total Gram : " + t);
                }
                else if (vori > 0 && ana == 0 && roti > 0 && mroti == 0) {

                    $per_vori_roti_price = Number(vori_price) / 96;
                    /* per vori roti amount start */
                    $total_roti = Number(vori) * 96 + Number(roti);
                    $total_roti_price = $total_roti * $per_vori_roti_price;
                    $("#product_rate").val($total_roti_price.toFixed(2));

                    console.log("Total Roti :" + $total_roti + " Per Roti Price : " + $per_vori_roti_price + " Total Price : " + $total_roti_price);
                    var t = Number(vori) * 11.664 + Number(roti) * 0.1215;
                    $("#gram").val(t.toFixed(2));
                    console.log("Total Gram : " + t);


                }
                else if (vori > 0 && ana == 0 && roti == 0 && mroti > 0) {

                    $per_vori_miliroti_price = Number(vori_price) / 960;
                    /* per vori roti amount start */
                    $total_roti = Number(vori) * 960 + Number(mroti);
                    $total_roti_price = $total_roti * $per_vori_miliroti_price;
                    $("#product_rate").val($total_roti_price.toFixed(2));
                    console.log("Total Mili Roti :" + $total_roti + " Per Mili Roti Price : " + $per_vori_miliroti_price + " Total Price : " + $total_roti_price);


                    var t = 11.664 * Number(vori) + Number(mroti) * Number(0.01215);

                    $("#gram").val(t.toFixed(2));
                    console.log("Final : " + t);


                }
                else {
                    $per_vori_roti_price = Number(vori_price) / 960;
                    /* per vori roti amount start */
                    $total_mili_roti = (Number(vori) * 96 + Number(ana) * 6 + Number(roti)) * 10 + "." + Number(mroti);
                    $total = $total_mili_roti * $per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));
                    console.log("Total Vori = " + vori + " Vori * Mili Roti = " + $total_mili_roti + " Per Mili Roti Price : " + $per_vori_roti_price + " Total Price : " + $total);

                    var t = 11.664 * Number(vori) + Number(ana) * Number(0.73) + Number(roti) * Number(0.1215) + Number(mroti) * Number(0.01215);
                    $("#gram").val(t.toFixed(2));
                    console.log("Final : " + t);
                }
            }
        });

        $(document).on('change', '#ana', function () {
            var ana = $(this).val();
            var vori = $("#vori").val();
            var roti = $("#roti").val();
            var mroti = $("#miliroti").val();
            var rate = $("#product_rate").val();
            var vori_price = $("#product_price").val();
            /*when vori,ana,roti empty */
            if (ana > 16 || ana == 16) {
                $(this).addClass('bg-danger').val(0);
                $("#product_rate").val(0);
            } else {
                $(this).removeClass('bg-danger');
                if (vori == 0 && ana > 0 && roti == 0 && mroti == 0) {
                    var per_ana_price = 0;
                    per_ana_price = Number(vori_price) / 16;
                    var total = Number(per_ana_price) * Number(ana);
                    $("#product_rate").val(total.toFixed(2));

                    var t = Number(ana) * Number(0.73);
                    $("#gram").val(t.toFixed(2));
                    console.log("Final : " + t);

                } else if (vori > 0 && ana > 0 && roti == 0 && mroti == 0) {
                    var per_ana_price = Number(vori_price) / 16;
                    var total_ana = Number(vori) * 16 + Number(ana);
                    var total_ana_price = Number(per_ana_price) * Number(total_ana);
                    var price = total_ana_price.toFixed(2);
                    $("#product_rate").val(price);
                    console.log("To Fix fire : " + price);

                    var t = 11.664 * Number(vori) + Number(ana) * Number(0.73);
                    $("#gram").val(t.toFixed(2));
                    console.log("Final : " + t);


                } else if (vori > 0 && ana > 0 && roti > 0 && mroti == 0) {
                    $per_vori_roti_price = Number(vori_price) / (16 * 6);
                    /* per vori roti amount start */
                    $total_roti = Number(vori) * 96 + (Number(ana) * 6) + Number(roti);
                    $total_roti_price = $total_roti * $per_vori_roti_price;
                    $("#product_rate").val($total_roti_price.toFixed(2));

                    var t = 11.664 * Number(vori) + Number(ana) * Number(0.73) + Number(roti) * Number(0.1215);
                    $("#gram").val(t.toFixed(2));
                    console.log("Final : " + t);


                } else if (vori == 0 && ana > 0 && roti > 0 && mroti > 0) {
                    $per_vori_roti_price = vori_price / 960;
                    $total_mili_roti = (Number(ana) * 6 + Number(roti)) * 10 + Number(mroti);
                    $total = $total_mili_roti * $per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));
                    console.log("Total Mili Roti : " + $total_mili_roti + " Total Per Mili Roti Price : " + $per_vori_roti_price + " Total Price according to Mili Roti : " + $total);


                    var t = Number(roti) * Number(0.1215) + Number(mroti) * Number(0.01215);
                    $("#gram").val(t.toFixed(2));
                    console.log("Final : " + t);


                } else if (vori == 0 && ana > 0 && roti > 0 && mroti == 0) {

                    $per_vori_roti_price = Number(vori_price) / 96;
                    $total_roti = (Number(ana) * 6 + Number(roti));
                    $total = $total_roti * $per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));
                    console.log("Total Mili Roti : " + $total_roti + " Total Per Mili Roti Price : " + $per_vori_roti_price + " Total Price according to Mili Roti : " + $total);

                    var t = Number(ana) * Number(0.73) + Number(roti) * Number(0.1215);
                    $("#gram").val(t.toFixed(2));
                    console.log("Final : " + t);


                }
                else {
                    $per_vori_roti_price = Number(vori_price) / 960;
                    /* per vori roti amount start */
                    $total_mili_roti = (Number(vori) * 96 + Number(ana) * 6 + Number(roti)) * 10 + "." + Number(mroti);
                    $total = $total_mili_roti * $per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));
                    console.log("Total Vori = " + vori + " Vori * Mili Roti = " + $total_mili_roti + " Per Mili Roti Price : " + $per_vori_roti_price + " Total Price : " + $total);

                    var t = 11.664 * Number(vori) + Number(ana) * Number(0.73) + Number(roti) * Number(0.1215) + Number(mroti) * Number(0.01215);
                    $("#gram").val(t.toFixed(2));
                    console.log("Final : " + t);


                }
            }
        });

        $(document).on('keyup change', '#roti', function () {
            var roti = $(this).val();
            var vori = $("#vori").val();
            var ana = $("#ana").val();
            var mroti = $("#miliroti").val();
            var rate = $("#product_rate").val();
            var vori_price = $("#product_price").val();
            /*when vori,ana,roti empty */
            if (roti > 6 || roti == 6) {
                $(this).addClass("bg-danger").val(0);
                $("#product_rate").val(0);
            }
            else {
                $(this).removeClass('bg-danger');

                if (vori == 0 && ana == 0 && roti > 0 && mroti == 0) {
                    var per_ana_price = Number(vori_price) / 96;
                    var total = Number(per_ana_price) * Number(roti);
                    $("#product_rate").val(total.toFixed(2));

                    var t = Number(roti) * Number(0.1215);
                    $("#gram").val(t.toFixed(2));
                    console.log("Final : " + t);


                } else if (vori > 0 && ana == 0 && roti > 0 && mroti == 0) {
                    var per_roti_price = Number(vori_price) / 96;
                    var total_roti = Number(vori) * 96 + Number(roti);
                    var total_roti_price = Number(per_roti_price) * Number(total_roti);
                    $("#product_rate").val(total_roti_price.toFixed(2));

                    console.log("Total Roti : " + total_roti + " Per Roti Price : " + per_roti_price + " Total Roti Price : " + total_roti_price + " To Fixed Product Price : " + total_roti_price.toFixed(2));


                    var t = 11.664 * Number(vori) + Number(roti) * Number(0.1215);
                    $("#gram").val(t.toFixed(2));
                    console.log("Final : " + t);


                } else if (vori > 0 && ana > 0 && roti > 0 && mroti == 0) {
                    $per_vori_roti_price = Number(vori_price) / 96;
                    /* per vori roti amount start */
                    $total_roti = Number(vori) * 96 + (Number(ana) * 6) + Number(roti);
                    $total_roti_price = $total_roti * $per_vori_roti_price;
                    $("#product_rate").val($total_roti_price.toFixed(2));

                    console.log("Total Roti : " + $total_roti + " Per Roti Price : " + $per_vori_roti_price + " Total Roti Price : " + $total_roti_price);


                    var t = 11.664 * Number(vori) + Number(ana) * Number(0.73) + Number(roti) * Number(0.1215);
                    $("#gram").val(t.toFixed(2));
                    console.log("Final : " + t);


                } else if (vori > 0 && ana == 0 && roti > 0 && mroti > 0) {

                    $per_vori_roti_price = Number(vori_price) / 960;
                    /* per vori Mili-roti amount start */
                    $total_roti = Number(vori) * 960 + Number(roti) * 10 + Number(mroti);
                    $total_roti_price = $total_roti * $per_vori_roti_price;
                    $("#product_rate").val($total_roti_price.toFixed(2));
                    console.log("Total Mili Roti : " + $total_roti + " Per Mili Roti Price : " + $per_vori_roti_price + " Total Mili Roti Price : " + $total_roti_price);

                    var t = 11.664 * Number(vori) + Number(roti) * Number(0.1215) + Number(mroti) * Number(0.01215);
                    $("#gram").val(t.toFixed(2));
                    console.log("Final : " + t);

                }

                else if (vori == 0 && ana == 0 && roti > 0 && mroti > 0) {
                    $per_vori_roti_price = Number(vori_price) / 960;
                    /* per vori Mili-roti amount start */
                    $total_roti = Number(roti) * 10 + Number(mroti);
                    $total_roti_price = $total_roti * $per_vori_roti_price;
                    $("#product_rate").val($total_roti_price.toFixed(2));
                    console.log("Total Mili Roti : " + $total_roti + " Per Mili Roti Price : " + $per_vori_roti_price + " Total Mili Roti Price : " + $total_roti_price);


                    var t = Number(roti) * Number(0.1215) + Number(mroti) * Number(0.01215);
                    $("#gram").val(t.toFixed(2));
                    console.log("Final : " + t);
                }
                else {
                    $per_vori_roti_price = Number(vori_price) / 960;
                    /* per vori roti amount start */
                    $total_mili_roti = (Number(vori) * 96 + Number(ana) * 6 + Number(roti)) * 10 + Number(mroti);
                    $total = $total_mili_roti * $per_vori_roti_price;
                    $("#product_rate").val($total.toFixed(2));
                    console.log("Total Vori = " + vori + " Vori * Mili Roti = " + $total_mili_roti + " Per Mili Roti Price : " + $per_vori_roti_price + " Total Price : " + $total);


                    var t = 11.664 * Number(vori) + Number(ana) * Number(0.73) + Number(roti) * Number(0.1215) + Number(mroti) * Number(0.01215);
                    $("#gram").val(t.toFixed(2));
                    console.log("Final : " + t);
                }
            }
        });


        $(document).on('keyup change', '#miliroti', function () {
            var mroti = $(this).val();
            var vori = $("#vori").val();
            var ana = $("#ana").val();
            var roti = $("#roti").val();

            var rate = $("#product_rate").val();
            var vori_price = $("#product_price").val();
            /*when vori,ana,roti empty */

            if (vori == 0 && ana == 0 && roti == 0 && mroti > 0) {
                $per_vori_roti_price = Number(vori_price) / (16 * 6 * 10);
                /* per vori roti amount start */
                $total_mili_roti = (Number(vori) * 16 * 6 + (Number(ana) * 6) + Number(roti)) * 10 + Number(mroti);
                $total = $total_mili_roti * $per_vori_roti_price;
                $("#product_rate").val($total.toFixed(2));

                var t = Number(mroti) * Number(0.01215);
                $("#gram").val(t.toFixed(2));
                console.log("Final : " + t);

            } else if (vori == 0 && ana == 0 && roti > 0 && mroti > 0) {
                $per_vori_roti_price = Number(vori_price) / (16 * 6 * 10);
                /* per vori roti amount start */
                $total_mili_roti = (Number(vori) * 16 * 6 + (Number(ana) * 6) + Number(roti)) * 10 + Number(mroti);
                $total = $total_mili_roti * $per_vori_roti_price;
                $("#product_rate").val($total.toFixed(2));

                var t = Number(roti) * Number(0.1215) + Number(mroti) * Number(0.01215);
                $("#gram").val(t.toFixed(2));
                console.log("Final : " + t);

            } else if (vori == 0 && ana > 0 && roti > 0 && mroti > 0) {
                $per_vori_roti_price = Number(vori_price) / (16 * 6 * 10);
                /* per vori roti amount start */
                $total_mili_roti = (Number(vori) * 16 * 6 + (Number(ana) * 6) + Number(roti)) * 10 + Number(mroti);
                $total = $total_mili_roti * $per_vori_roti_price;
                $("#product_rate").val($total.toFixed(2));

                var t = Number(ana) * Number(0.73) + Number(roti) * Number(0.1215) + Number(mroti) * Number(0.01215);
                $("#gram").val(t.toFixed(2));
                console.log("Final : " + t);

            } else {
                $per_vori_roti_price = Number(vori_price) / 960;
                /* per vori roti amount start */
                $total_mili_roti = (Number(vori) * 96 + Number(ana) * 6 + Number(roti)) * 10 + Number(mroti);
                $total = $total_mili_roti * $per_vori_roti_price;
                $("#product_rate").val($total.toFixed(2));

                console.log("Total Vori = " + vori + " Vori * Mili Roti = " + $total_mili_roti + " Per Mili Roti Price : " + $per_vori_roti_price + " Total Price : " + $total);

                var t = 11.664 * Number(vori) + Number(ana) * Number(0.73) + Number(roti) * Number(0.1215) + Number(mroti) * Number(0.01215);
                $("#gram").val(t.toFixed(2));
                console.log("Final : " + t);


            }
            /* Final Else Condition End Here */

        });

        /* Calculate according to Vori End */
        /* if select product name */
        /*
         ========== product information search with Product Name ===========
         */

        $(document).on('change', '#product_name', function () {
            var product_code_id = $(this).val();
            var token = "{{ csrf_token() }}";
            if (product_code_id != '') {
                $.ajax({
                    method: "post",
                    url: "{{ route('product.find') }}",
                    data: {product_code_id: product_code_id, "_token": token},
                    dataType: "json",
                    success: function (response) {

                        var parsedata = JSON.parse(response);

                        console.log(parsedata.brand);

                        $("#pid").val(parsedata.id);

                        global_price = parsedata.product_price;

                        $('#product_name').val(parsedata.id);

                        $('#product_code').val(parsedata.id).attr('selected', true);
                        $('.product_code_select2').select2({tags: true, maximumSelectionLength: 1});
                        $('.product_name_select2').select2({tags: true, maximumSelectionLength: 1});
                        $('#product_price').val(parsedata.product_price);

                        $("#size_id").val(parsedata.size);
                        $("#color_id").val(parsedata.color);
                        $("#karat_id").val(parsedata.karat);
                        $("#origin_id").val(parsedata.origin);
                        $("#gold_type_id").val(parsedata.goldtype);

                        $("#brand_id").val(parsedata.brand);
                        $("#category_id").val(parsedata.category);
                        $("#subcategory_id").val(parsedata.subcategorie);
                        $("#product_qty").val(1);

                        $("#vori").val(0);
                        $("#product_rate").val(0);
                        /* product price multple quantity equal Total Price */
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            } else {
                product_code();
            }
        });

        /*
         ========== product information search with Product Name ===========
         */




        /* onchange quantity for total amount */

        /*
         When someone try to increment and decrement product price and if there price value found less then zero then
         it will be Original Product Price automatically
         */



        /* When someone try to increment and decrement product price and if there price value found less then zero then it will be Original Product Price automatically */

        /* if quantity change then it will be calculate with price and store total value inside rate start */

        $(document).on('keyup change', '#product_qty', function () {
            var price = $("#product_price").val();
            var qty = $(this).val();
            if (qty <= 0) {
                $("#product_rate").val(price);
                $(this).closest('div').addClass('has-error');
                alert('Sir/Madam Product Quantity Never be Zero');
                alert(price);
                $(this).val(1);

                $(this).closest('div').removeClass('has-error');
            }

            var total = 0;
            total = $("#product_price").val() * qty;
            $("#product_rate").val(total);

        });

        /**/


        /* Product add to Cart / Bucket Start From Here */

        $(document).on('click', '#add_bucket', function () {
            var id = $("#product_name").val();
            var product_price = $("#product_price").val();
            var ana = $("#ana").val();
            var roti = $("#roti").val();
            var miliroti = $("#miliroti").val();
            var vori = $("#vori").val();
            var gram = $("#gram").val();
            var productqty = $("#product_qty").val();
            var interest = $("#interest").val();
            var pmt_date = $("#pmt_date").val();
            var total_product_price = $("#product_rate").val();
            if (product_price < 0) {
                alert("Please Select Every single field before Add to Cart");
            } else {
                $.ajax({
                    method: "post",
                    url: "{{ route('mortgage.addCart') }}",
                    data: {
                        id: id,
                        product_price: product_price,
                        "_token": '{{ csrf_token() }}',
                        ana: ana,
                        roti: roti,
                        miliroti: miliroti,
                        vori: vori,
                        gram: gram,
                        product_qty: productqty,
                        interest: interest,
                        pmt_date: pmt_date,
                        total_product_price: total_product_price
                    },
                    dataType: 'html',
                    success: function (response) {
                        $("#all_cart_item").html(response);


                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            }
        });
        /* Product add to Cart / Bucket End Here */
        $(document).on('change','#payment_type',function(){
            var type = $(this).val();
            if(type == 'bkash' || type=="rocket" || type=="check" || type=="card"){
                $("#bkash_code").show(500);
            }else{
                $("#bkash_code").hide(500);
            }
        });
        /* onload added products fetch */

        function Carts() {
            $.ajax({
                method: "get",
                url: "{{ route('mortgage.carts') }}",
                dataType: 'html',
                success: function (response) {
                    $("#all_cart_item").html(response);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        }
        /* onload added products fetch */

        /*  Cart Remove Start */
        $(document).on('click', '#remove_item', function () {
            var token = '{{ csrf_token() }}';
            var rowId = $(this).attr('data-id');
            $.ajax({
                method: "post",
                url: '{{ route("mortgage.cart-remove") }}',
                data: {rowId: rowId, "_token": token},
                dataType: "html",
                success: function (response) {
                    $("#all_cart_item").html(response);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        });
        /*  Cart Remove End */

        /* Cart Product List Edit Start */
        $(document).on('click', '#update_item', function () {
            var rowId = $(this).attr('data-id');
            var token = '{{ csrf_token() }}';


            $.ajax({
                method: "post",
                url: '{{ route("purchase.cart-edit") }}',
                data: {rowId: rowId, "_token": token},
                dataType: "json",
                success: function (response) {

                    var parsedata = JSON.parse(response);

                    $("#pid").val(parsedata.id);

                    global_price = parsedata.product_price;
                    $('#product_code').val(parsedata.id).attr('selected', true);
                    $('.product_name_select2').val(parsedata.id).attr('selected', true);

                    $('.product_code_select2').select2({tags: true, maximumSelectionLength: 1});
                    $('.product_name_select2').select2({tags: true, maximumSelectionLength: 1});


                    $('#product_price').val(parsedata.product_price);
                    $("#product_qty").val(1);
                    $('#product_rate').val(parsedata.total);

                    $("#vori").val(parsedata.vori);
                    $("#ana").val(parsedata.ana);
                    $("#roti").val(parsedata.roti);
                    $("#miliroti").val(parsedata.miliroti);

                    $("#size_id").val(parsedata.size);
                    $("#color_id").val(parsedata.color);
                    $("#karat_id").val(parsedata.karat);
                    $("#origin_id").val(parsedata.origin);
                    $("#gold_type_id").val(parsedata.goldtype);
                    $("#brand_id").val(parsedata.brand);
                    $("#category_id").val(parsedata.category);
                    $("#subcategory_id").val(parsedata.subcategorie);


                },
                error: function (err) {
                    console.log(err);
                }
            });
        });

        /* Cart Product List Edit End */

        /* update Process Start From here */

        $(document).on('click', '#unique_cart_update', function () {

            var id = $("#product_name").val();
            var product_price = $("#product_price").val();
            var ana = $("#ana").val();
            var roti = $("#roti").val();
            var miliroti = $("#miliroti").val();
            var vori = $("#vori").val();
            var productqty = $("#product_qty").val();
            var rate = $("#product_rate").val();

            if (product_price == "" || cart_quantity == "") {
                alert("Please Select Every single field before Add to Cart");
            }
            else {
                $.ajax({
                    method: "post",
                    url: "{{ route('purchase.UpdateCart') }}",
                    data: {
                        id: id,
                        rowId: rowId,
                        product_price: product_price,
                        "_token": '{{ csrf_token() }}',
                        weight_id: weight_id,
                        weight_amount: weight_amount,
                        cart_quantity: cart_quantity
                    },
                    data: {
                        id: id,
                        rowId: rowId,
                        product_price: product_price,
                        "_token": '{{ csrf_token() }}',
                        ana: ana,
                        roti: roti,
                        miliroti: miliroti,
                        vori: vori,
                        product_qty: productqty,
                        rate: rate
                    },
                    dataType: 'html',
                    success: function (response) {
                        $(this).removeAttr("data-id");
                        $(this).text("Add To Bucket");
                        $(this).attr("class", "btn btn-primary");
                        $(this).attr("id", "add_bucket");

                        $("#unique_cart_update").text('Add To Cart').attr('id', 'add_bucket').removeAttr('data-id');

                        $("#product_name").val('').attr('selected', false);
                        $('.product_name_select2').select2({tags: true, maximumSelectionLength: 1});

                        $("#product_code").val('').attr('selected', false);
                        $('.product_code_select2').select2({tags: true, maximumSelectionLength: 1});

                        $("#product_price").val('');

                        $("#karat_id").val('').attr('selected', false);
                        $('.karat_id_select2').select2({tags: true, maximumSelectionLength: 1});

                        $("#all_cart_item").html(response);
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            }
        });

        /* Update Process End From here */


        /* payment calculation start from here */

        $(document).on('keyup', '#less', function () {
            var total_amount = $("#total_amount").val();
            var less = $(this).val();
            var balance = total_amount - less;
            if (balance > 0) {
                $("#balance").css({
                    "border": "2px red",
                    "background": "red",
                    "color": "#fff",
                    "font-size": "1.5rem",
                    "font-weight": "bold"
                });
            } else {
                $("#balance").css({
                    "border": "2px green",
                    "background": "#fff",
                    "color": "#000",
                    "font-size": "1.5rem",
                    "font-weight": "bold"
                });
            }
            $("#balance").val(balance.toFixed(2));
        });
        var z = 0;


        $(document).on('keyup change', '#total_less', function () {
            var total_amount = $("#total_amount").val();
            var total_discount = $("#discount").val();

            var total_less = $(this).val();

            var after_discount = Number(total_amount) - Number(total_discount);
            var after_less = Number(after_discount) - Number(total_less);
            $("#balance").val(after_less.toFixed(2));
            console.log("Total : " + total_amount + " Discount : " + total_discount + "After Discount : " + after_discount + " Total Less : " + total_less + " Payable Amount : " + after_less);
        });


        $(document).on('keyup', '#paid', function () {
            var total_paid = $(this).val();
            var total_less = $('#less').val();
            var total_amount = $("#total_amount").val();
            var amount_due = Number(total_amount) - Number(total_less)-Number(total_paid);
            if (amount_due > 0) {
                $("#payment_date").show(500);
            } else {
                $("#payment_date").hide(500);
            }
            $("#balance").val(amount_due.toFixed(2));

        });
        /* Payment calculation end here */

        /* */

    </script>
@endsection