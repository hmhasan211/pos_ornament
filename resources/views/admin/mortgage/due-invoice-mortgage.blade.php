@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow"> View Mortgage Invoice</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Due Mortgage</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"> Due Mortgage</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                    <table class="table table-bordered table-striped table-responsive" id="CategoryTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Invoice No</th>
                                            <th>Amount</th>
                                            <th>Discount </th>
                                            <th>After Discount+Less</th>
                                            <th>Balance</th>
                                            <th>Interest</th>
                                            <th>Paid</th>
                                            <th> বাকী টাকা পরিষধের তারিখ ঃ  </th>
                                            <th>Due/Change date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0;

                                        @endphp

                                        @foreach($invoice as $invoice_info)

                                            @php
                                                $total = 0; 
                                                
                                                $amount_makingcharge = $invoice_info->total_amount;

                                                $discount_less = $invoice_info->total_less;

                                                $balance = $amount_makingcharge - $discount_less;

                                            @endphp

                                            @foreach($invoice_info->cash_books as $data)
                                                @php
                                                    $total+=$data->expense
                                                @endphp
                                            @endforeach

                                            @php  $duetk = $balance-$total;   @endphp


                                           @if( $duetk !=0)
                                                <tr>
                                                    <td>{{ ++$i }}</td>
                                                    <td>{{ $invoice_info->invoice_no }}</td>
                                                    <td id="tamount{{$invoice_info->id}}" data-id="{{ $amount_makingcharge }}">

                                                        {{ $invoice_info->total_amount }}


                                                    </td>

                                                    <td id="tdiscount{{ $invoice_info->id }}" data-id="{{ $invoice_info->total_discount }}">
                                                      
                                                        {{ $invoice_info->total_discount }}

                                                    </td>

                                                    <td id="afterdiscount{{ $invoice_info->id }}" data-id="{{ $amount_makingcharge- $invoice_info->total_discount }}">
                                                     {{ $amount_makingcharge- $invoice_info->total_discount- $invoice_info->total_less }}
                                                    </td>

                                                    <td id="balance{{  $invoice_info->id }}" data-id="{{ $duetk }}"> 
                                                        {{ $duetk  }}
                                                    </td>
                                                    <td id="tinterest{{ $invoice_info->id }}" data-id="{{ $invoice_info->total_interest }}"> {{ $invoice_info->total_interest or "N/A" }} </td>
                                                    <td id="tpaid{{ $invoice_info->id }}" data-id="{{ $total }}"> {{ $total  }} </td>

                                                    <td id="invoice_date{{$invoice_info->id}}" data-id="{{$invoice_info->payment_deadline}}">
                                                      {{ \Carbon\Carbon::parse($invoice_info->payment_deadline)->diffInDays() }} দিন পরে বাকী টাকা পরিশোধ করবে 
                                                      
                                                    </td>

                                                    <td>
                                                        <button type="button" title="Pay due amount" id="dueamountpay" data-id="{{ $invoice_info->id }}" class="fa fa-money btn btn-info"></button>
                                                        <button type="button" title="Change payment date" id="changedate" data-id="{{ $invoice_info->id }}" class="fa fa-edit btn btn-info"></button>
                                                    </td>
                                                </tr>
                                           @endif 
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--Large Bootstrap Modal-->
    <!--===================================================-->
    <div id="duypayment" class="modal fade" tabindex="-1">
        <div class="modal-dialog  animated swing">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title" id="myLargeModalLabel">Due Payment</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        {{ Form::hidden('invoice_id',null,['id'=>'invoice_id']) }}
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('TotalAmount','Total Amount',['class'=>'label-control']) }}
                                {{ Form::number('totalamount',null,['id'=>'totalamount','class'=>'form-control','readonly']) }}
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('Total Discount','Total Discount',['class'=>'label-control']) }}
                                {{ Form::number('totaldiscount',null,['id'=>'totaldiscount','class'=>'form-control','readonly']) }}
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('Paid','After Discount',['class'=>'label-control']) }}
                                {{ Form::number('totalpaid',null,['id'=>'afterdiscount','class'=>'form-control','readonly']) }}
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('Paid','Total Paid',['class'=>'label-control']) }}
                                {{ Form::number('totalamount',null,['id'=>'totalpaid','class'=>'form-control','readonly']) }}
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('PayAmount','Need to Pay',['class'=>'label-control']) }}
                                {{ Form::number('needtopay',null,['id'=>'needtopay','class'=>'form-control','readonly']) }}
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('PayAmount','Payment',['class'=>'label-control']) }}
                                {{ Form::text('lastpay',null,['id'=>'lastpay','class'=>'form-control','placeholder'=>'Enter Current Payment']) }}
                            </div>
                        </div>

                        <div class="col-lg-5 col-sm-5 col-md-5 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('lessamount','Less',['class'=>'label-control']) }}
                                {{ Form::number('lesspay',null,['id'=>'less','class'=>'form-control']) }}
                            </div>
                        </div>

                        <div class="col-lg-5 col-sm-5 col-md-5 col-xs-12" id="mortgage_interest">
                            <div class="form-group">
                                {{ Form::label('total_interest','Mortgage Interest',['class'=>'label-control']) }}
                                {{ Form::number('total_interest',null,['id'=>'tot_interest','class'=>'form-control','placeholder'=>'Mortgage interest']) }}
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button data-bb-handler="cancel" type="button" class="btn btn-default">Cancel</button>
                    <button  type="button" class="btn btn-primary duepayment">Due Payment</button>
                </div>

            </div>
        </div>
    </div>
    <!--===================================================-->
    <!--End Large Bootstrap Modal-->

    <!--Change date Bootstrap Modal-->
    <!--===================================================-->
    <div id="changedatemodal" class="modal fade" tabindex="-1">
        <div class="modal-dialog  animated swing">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                    <h4 class="modal-title" id="myLargeModalLabel">Change Payment Date</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        {{ Form::hidden('invoice_id',null,['id'=>'invoice_id']) }}


                        <div class="col-lg-offset-2 col-lg-8 col-sm-12 col-md-12 col-xs-12 col-lg-offset-2">
                            <div class="form-group">
                                {{ Form::label('payment_deadline','Change Payment Date',['class'=>'label-control']) }}
                                {{ Form::text('payment_deadline',null,['id'=>'pmtdate','class'=>'form-control']) }}
                            </div>
                        </div>



                    </div>
                </div>

                <div class="modal-footer">
                    <button data-bb-handler="cancel" type="button" class="btn btn-default">Cancel</button>
                    <button  type="button" class="btn btn-primary" id="btn-change-date">Change Payment Date</button>
                </div>

            </div>
        </div>
    </div>
    <!--===================================================-->
    <!--End change date Bootstrap Modal-->
    <script>

        $(function(){
            $('#CategoryTable').DataTable();
        });


        $(document).on('click','#dueamountpay',function(){
            var id = $(this).attr('data-id');
            $("#invoice_id").val(id);
            var tamount = $("#tamount"+id).attr('data-id');
            var tdiscount = $("#tdiscount"+id).attr('data-id');
            var afterdiscount = $("#afterdiscount"+id).attr('data-id');
            var balance = $("#balance"+id).attr('data-id');
            var tpaid = $("#tpaid"+id).attr('data-id');
            var tinterest = $("#tinterest"+id).attr('data-id');
            if(tinterest==''){
                $('#mortgage_interest').hide();
            }else{
                $('#mortgage_interest').show();
            }


            $("#totalamount").val(tamount);
            $("#totaldiscount").val(tdiscount);
            $("#needtopay").val(balance);
            $("#afterdiscount").val(afterdiscount);
            $("#totalpaid").val(tpaid);
            $("#tot_interest").val(tinterest);
            $("#duypayment").modal('show');
        });

        <!-- to change payment date Start-->
        $(document).on('click','#changedate',function () {
            var id = $(this).attr('data-id');
            $("#invoice_id").val(id);
            var inv_date = $("#invoice_date"+id).attr('data-id');
            $("#pmtdate").val(inv_date);
            $('#pmtdate').datepicker('setDate',inv_date);
            $("#changedatemodal").modal('show');
        });

        $(document).on('click','#btn-change-date',function () {
            var changedate = $("#pmtdate").val();
            var invoice_id = $("#invoice_id").val();
            $.ajax({
                method:"post",
                url:"{{ route('invoice.changedate') }}",
                data:{invoice_id:invoice_id,payment_deadline:changedate,"_token":"{{ csrf_token() }}"},
                dataType:'json',
                success:function (done) {
                    console.log(done);
                    //location.reload(true);
                },error:function (err) {
                    console.log(err);

                }
            });
            $("#changedatemodal").modal('hide');
            location.reload(true);
        });

        <!-- to change payment date Ends-->
        var last_input = 0;

        $(document).on('keyup','#lastpay',function () {
            var amount = Number($(this).val());
            var needtopay = Number($("#needtopay").val());
            var invoice_id = $("#invoice_id").val();

            if(amount == " " || amount<0 ){
                $(this).val("");
                alert("White space or Less then 1 Not allowed");
            }else{
                if(needtopay>=amount ){
                    last_input = amount;
                }else{
                    $(this).val(last_input);
                    alert("Sir You have to pay only "+needtopay);
                }
            }
        });

        /* Apply for Due Payment start */

        $(document).on('click','.duepayment',function () {
            var amount = $("#lastpay").val();
            var invoice_id = $("#invoice_id").val();
            var less = $("#less").val();
            var interest = $("#tot_interest").val();
            $.ajax({
                method:"post",
                url:"{{ route('invoice.duepayment') }}",
                data:{invoice_id:invoice_id,total_interest:interest,amount:amount,"_token":"{{ csrf_token() }}","less":less},
                dataType:'json',
                success:function (done) {
                    console.log(done);
                    location.reload();
                },error:function (err) {
                    console.log(err);
                }
            });
        });

        /* Apply for Due Payment End */

        $(document).on('click','.activestatus',function(){
            var id = $(this).attr('data-id');
            $('#product_status').modal('show');
            $("#status_id").val(id);
            console.log("Fire Console : "+$(this).attr('data-id'));
        });

        /* UPDATE Category END */

    </script>




@endsection