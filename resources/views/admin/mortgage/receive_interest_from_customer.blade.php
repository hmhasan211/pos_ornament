@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Receive Interest From Customer</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Mahajan</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Receive Interest</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    {{ Form::open(['route'=>'rcv_int_save','method'=>'post']) }}

                                    <div class="row">
                                        <div class="col-lg-6 col-sm-12 {{$errors->has('mahajan_receipt_no') ? 'has-error' : ''}}">

                                            {{ Form::label('mahajan_receipt_no','Receipt No : ',['class'=>'control-label'])}}
                                            <select name="invoice_no" class="form-control select2" id="mh_receipt">
                                                <option>Select Receipt</option>
                                                @foreach($invoice as $inv)
                                                    <option value="{{$inv->id}}">{{$inv->invoice_no.'---'.$inv->customer->name}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('receipt_no'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('mahajan_receipt_no') }}</strong>
                                                </span>
                                            @endif
                                            <hr>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 {{$errors->has('mahajan_id') ? 'has-error' : ''}}">
                                            {{ Form::label('mahajan_id','Customer : ',['class'=>'control-label'])}}
                                            {{ Form::text('customer',null,['class'=>'form-control','placeholder'=>'Select Mahajan','id'=>'customer','disabled'])}}


                                            {{ Form::hidden('customer_id',old('customer_id'),['id'=>'customer_id']) }}
                                            @if ($errors->has('mahajan_id'))
                                                <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                            <hr>
                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="col-lg-6 col-sm-12 {{$errors->has('employee_id') ? 'has-error' : ''}}">
                                            {{ Form::label('employee_id','Select Employee : ',['class'=>'control-label'])}}
                                            {{ Form::select('employee_id',$employee,null,['class'=>'form-control select2','placeholder'=>'Select Employee','id'=>'employee_id'])}}
                                            @if ($errors->has('employee_id'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('employee_id') }}</strong>
                                                </span>
                                            @endif
                                            <hr>
                                        </div>
                                        <div class="col-lg-6 col-sm-12 {{$errors->has('interest') ? 'has-error' : ''}}">

                                            {{ Form::label('interest','Interest : ',['class'=>'control-label'])}}
                                            {{ Form::number('interest',null,['class'=>'form-control','placeholder'=>'Ex: Amount','id'=>'interest'])}}
                                            @if ($errors->has('interest'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('interest') }}</strong>
                                                </span>
                                            @endif
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">


                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12 {{$errors->has('description') ? 'has-error' : ''}}">

                                            {{ Form::label('description','Product Description : ',['class'=>'control-label'])}}
                                            {{ Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Ex: Product Description','id'=>'pro_desc'])}}
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-12 {{$errors->has('pmt_date') ? 'has-error' : ''}}">

                                            {{ Form::label('pmt_date','Receipt Date : ',['class'=>'control-label'])}}
                                            {{ Form::text('pmt_date',null,['class'=>'form-control common-datepicker','id'=>'in_date'])}}
                                            @if ($errors->has('pmt_date'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('pmt_date') }}</strong>
                                                </span>
                                            @endif
                                            <hr>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <br>
                                            {{ Form::button('Receive From Customer',['type'=>'submit','id'=>'pay_to_mahajan','class'=>'btn btn-primary']) }}

                                        </div>
                                    </div>



                                    {{ Form::close() }}
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12 table-responsive">
                                    <table class="table table-bordered table-striped" id="brandTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Receipt No</th>
                                            <th>Customer</th>
                                            <th>Interest</th>
                                            <th>Receipt Date</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                            @foreach($send as $info)
                                                <tr id="rowid{{$info->id}}" class="abcd">
                                                    <td>{{++$i}}</td>
                                                    <td id="receipt_no{{ $info->id }}" data-id="{{ $info->invoice->invoice_no }}">{{$info->invoice->invoice_no}}</td>
                                                    <td id="receipt_no{{ $info->id }}" data-id="{{ $info->customer->name }}">{{$info->customer->name}}</td>
                                                    <td id="interest{{ $info->id }}" data-id="{{ $info->income }}">{{ $info->income }}</td>
                                                    <td id="in_date{{ $info->id }}" data-id="{{ $info->pmt_date }}">{{$info->pmt_date}}</td>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        $('.common-datepicker').datepicker({
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });
        $('.select2').select2();

        $(document).on('change','#mh_receipt',function(){
            var id = $(this).val();
            var token = "{{ csrf_token() }}";
            $.ajax({
                method:"post",
                url:"{{route('get_mortgage_invoice')}}",
                data:{id:id,"_token":token},
                dataType:'json',
                success:function(response){
                    $('#amount').val(response.amount);

                    $('#interest').val(response.interest);

                    $('#customer').val(response.customer['name']);
                    $("#customer_id").val(response.customer_id);

                },
                error:function(err){console.log("Error List : "+err);}
            });

        });


    </script>

@endsection