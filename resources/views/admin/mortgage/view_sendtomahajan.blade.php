@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow"> View Items Sent to Mahajan</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Mahajan</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"> Sent to Mahajan</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 table-responsive">
                                    <table class="table table-bordered table-striped" id="CategoryTable">
                                        <tr>
                                            <th>Mahajan Receipt No</th>
                                            <td id="name{{ $info->id }}" data-id="{{ $info->mahajan_receipt_no }}">{{$info->mahajan_receipt_no}}</td>
                                        </tr>
                                        <tr>
                                            <th>Mahajan Name</th>
                                            <td id="name{{ $info->id }}" data-id="{{ $info->mahajan->name }}">{{$info->mahajan->name}}</td>
                                        </tr>
                                        <tr>
                                            <th>Employee name </th>
                                            <td id="employee{{ $info->id }}" data-id="{{ $info->employee->name }}">{{$info->employee->name}}</td>
                                        </tr>

                                        <tr>
                                            <th>amount</th>
                                            <td id="amount{{ $info->id }}" data-id="{{ $info->amount }}">{{$info->amount}}</td>
                                        </tr>
                                        <tr>
                                            <th>interest</th>
                                            <td id="interest{{ $info->id }}" data-id="{{ $info->interest }}"><input type="number" id="interest_update{{ $info->id }}" name="interest" value="{{$info->interest}}"></td>
                                        </tr>
                                        <tr>
                                            <th >description</th>
                                            <td id="description{{ $info->id }}" data-id="{{ $info->description }}">{{$info->description}}</td>
                                        </tr>
                                        <tr>
                                            <th>In Date</th>
                                            <td id="in_date{{ $info->id }}" data-id="{{ $info->in_date }}">{{$info->in_date}}</td>
                                        </tr>
                                        <tr>
                                            <th>Out Date</th>
                                            <td id="out_date{{ $info->id }}" data-id="{{ $info->out_date }}"><input type="date" class="common-datepicker" id="outdate_update{{ $info->id }}" name="out_date" value="{{$info->out_date}}"> </td>
                                        </tr>

                                   </table>
                                    <a href="{{url()->previous()}}" class="btn btn-primary">Back to previous page</a>||<a class="btn btn-info edit" data-id="{{ $info->id }}">Update Changes</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){

            $('#brandTable').DataTable();
            $('.common-datepicker').datepicker({
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });
            $("#canceledit").hide();

        });

        /* edit request start */
        $(document).on('click','.edit',function(){
            $("#cancel").attr("type","button");
            var conf = confirm("Are you sure to update?");
            if(conf) {
                var id = $(this).attr('data-id');
                var out_date = $('#outdate_update' + id).val();
                var interest = $('#interest_update' + id).val();
                var token = "{{ csrf_token() }}";
                $.ajax({
                    method: "post",
                    url: "{{route('sendtomahajan.update')}}",
                    data: {id: id, out_date: out_date, interest: interest, "_token": token},
                    dataType: 'json',
                    success: function (response) {
                        $.notify("Changes updated Successfully", {
                            globalPosition: 'bottom right',
                            className: 'success'
                        });

                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            }else{
                location.reload();
            }
        });


        $("#cancel").click(function(){
            var route = '{{ route("mahajan.add") }}';
            $("#supplierForm").attr('action',route);
            $("#id").val("");
            $("#savesupplier").text("Add Mahajan");
            $("#savesupplier").attr('class','btn btn-primary');
            $(this).hide(299)
        });
        /* edit request end */


    </script>




@endsection