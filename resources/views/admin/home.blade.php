
@extends('admin.layouts.admin')
@section('content')
    <!--TIPS-->
    <!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->
        <div class="boxed">

            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                <div id="page-head">
                    <div class="pad-all text-center">
                        <h3>Welcome back to the Dashboard.</h3>
                        <p>Scroll down to see quick links and overviews of your Server, To do list, Order status or get
                            some Help using Nifty.</p>
                    </div>
                </div>


                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="panel panel-warning panel-colorful media middle pad-all">
                                <div class="media-left">
                                    <div class="pad-hor">
                                        <i class="demo-pli-checked-user icon-5x"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <p class="text-2x mar-no text-semibold">241</p>
                                    <p class="mar-no">Employee</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-info panel-colorful media middle pad-all">
                                <div class="media-left">
                                    <div class="pad-hor">
                                        <i class="demo-pli-file-zip icon-5x"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <p class="text-2x mar-no text-semibold">241</p>
                                    <p class="mar-no">Files</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-mint panel-colorful media middle pad-all">
                                <div class="media-left">
                                    <div class="pad-hor">
                                        <i class="demo-pli-camera-2 icon-5x"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <p class="text-2x mar-no text-semibold">241</p>
                                    <p class="mar-no">Photos</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-danger panel-colorful media middle pad-all">
                                <div class="media-left">
                                    <div class="pad-hor">
                                        <i class="demo-pli-add-user icon-5x"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <p class="text-2x mar-no text-semibold">241</p>
                                    <p class="mar-no">User</p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Order Status</h3>
                                </div>

                                <!--Data Table-->
                                <!--===================================================-->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Invoice</th>
                                                <th>User</th>
                                                <th>Order date</th>
                                                <th>Amount</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Tracking Number</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><a href="#" class="btn-link"> Order #53431</a></td>
                                                <td>Steve N. Horton</td>
                                                <td><span class="text-muted">Oct 22, 2014</span></td>
                                                <td>$45.00</td>
                                                <td class="text-center">
                                                    <div class="label label-table label-success">Paid</div>
                                                </td>
                                                <td class="text-center">-</td>
                                            </tr>
                                            <tr>
                                                <td><a href="#" class="btn-link"> Order #53432</a></td>
                                                <td>Charles S Boyle</td>
                                                <td><span class="text-muted">Oct 24, 2014</span></td>
                                                <td>$245.30</td>
                                                <td class="text-center">
                                                    <div class="label label-table label-info">Shipped</div>
                                                </td>
                                                <td class="text-center">CGX0089734531</td>
                                            </tr>
                                            <tr>
                                                <td><a href="#" class="btn-link"> Order #53433</a></td>
                                                <td>Lucy Doe</td>
                                                <td><span class="text-muted">Oct 24, 2014</span></td>
                                                <td>$38.00</td>
                                                <td class="text-center">
                                                    <div class="label label-table label-info">Shipped</div>
                                                </td>
                                                <td class="text-center">CGX0089934571</td>
                                            </tr>
                                            <tr>
                                                <td><a href="#" class="btn-link"> Order #53434</a></td>
                                                <td>Teresa L. Doe</td>
                                                <td><span class="text-muted">Oct 15, 2014</span></td>
                                                <td>$77.99</td>
                                                <td class="text-center">
                                                    <div class="label label-table label-info">Shipped</div>
                                                </td>
                                                <td class="text-center">CGX0089734574</td>
                                            </tr>
                                            <tr>
                                                <td><a href="#" class="btn-link"> Order #53435</a></td>
                                                <td>Teresa L. Doe</td>
                                                <td><span class="text-muted">Oct 12, 2014</span></td>
                                                <td>$18.00</td>
                                                <td class="text-center">
                                                    <div class="label label-table label-success">Paid</div>
                                                </td>
                                                <td class="text-center">-</td>
                                            </tr>
                                            <tr>
                                                <td><a href="#" class="btn-link">Order #53437</a></td>
                                                <td>Charles S Boyle</td>
                                                <td><span class="text-muted">Oct 17, 2014</span></td>
                                                <td>$658.00</td>
                                                <td class="text-center">
                                                    <div class="label label-table label-danger">Refunded</div>
                                                </td>
                                                <td class="text-center">-</td>
                                            </tr>
                                            <tr>
                                                <td><a href="#" class="btn-link">Order #536584</a></td>
                                                <td>Scott S. Calabrese</td>
                                                <td><span class="text-muted">Oct 19, 2014</span></td>
                                                <td>$45.58</td>
                                                <td class="text-center">
                                                    <div class="label label-table label-warning">Unpaid</div>
                                                </td>
                                                <td class="text-center">-</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <hr class="new-section-xs">
                                </div>
                                <!--===================================================-->
                            </div>
                        </div>
                    </div>


                </div>
                <!--===================================================-->
                <!--End page content-->

            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
        </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->
@endsection






