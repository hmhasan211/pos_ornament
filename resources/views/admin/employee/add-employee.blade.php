@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Employee</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Employee</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Employee</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    {{ Form::open(['route'=>'employee.store','method'=>'post','id'=>'CustomerForm','enctype'=>'multipart/form-data']) }}

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::hidden('id',null,['class'=>'form-control','id'=>'id']) }}
                                        {{ Form::label('Customername','Name : ',['class'=>'control-label'])}}
                                        {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Ex: Customer Name','id'=>'name'])}}
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('salary') ? 'has-error' : ''}}">
                                        {{ Form::label('Customername','Salary : ',['class'=>'control-label'])}}
                                        {{ Form::text('salary',old('salary'),['class'=>'form-control','placeholder'=>'Ex: Customer Name','id'=>'name'])}}
                                        @if ($errors->has('salary'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('salary') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>


                                    <div class="col-lg-12 col-sm-12 {{$errors->has('father') ? 'has-error' : ''}}">
                                        {{ Form::label('Customername','Father : ',['class'=>'control-label'])}}
                                        {{ Form::text('father',old('father'),['class'=>'form-control','placeholder'=>'Ex: Customer Name','id'=>'name'])}}
                                        @if ($errors->has('father'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('father') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>


                                    <div class="col-lg-12 col-sm-12 {{$errors->has('mother') ? 'has-error' : ''}}">
                                        {{ Form::label('Mother','Mother : ',['class'=>'control-label'])}}
                                        {{ Form::text('mother',old('mother'),['class'=>'form-control','placeholder'=>'Ex: Mother Name','id'=>'name'])}}
                                        @if ($errors->has('mother'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('mother') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>


                                    <div class="col-lg-12 col-sm-12 {{$errors->has('wife') ? 'has-error' : ''}}">
                                        {{ Form::label('Customername','Wife : ',['class'=>'control-label'])}}
                                        {{ Form::text('wife',old('wife'),['class'=>'form-control','placeholder'=>'Ex: wife Name','id'=>'name'])}}
                                        @if ($errors->has('wife'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('wife') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>


                                    <div class="col-lg-12 col-sm-12 {{$errors->has('nid') ? 'has-error' : ''}}">
                                        {{ Form::label('Customername','NID : ',['class'=>'control-label'])}}
                                        {{ Form::text('nid',old('nid'),['class'=>'form-control','placeholder'=>'Ex: 123456789101245','id'=>'name'])}}
                                        @if ($errors->has('nid'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('nid') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('phone1') ? 'has-error' : ''}}">
                                        {{ Form::label('Customername','Phone 1 : ',['class'=>'control-label'])}}
                                        {{ Form::text('phone1',old('phone1'),['class'=>'form-control','placeholder'=>'Ex: Customer Name','id'=>'name'])}}
                                        @if ($errors->has('phone1'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('phone1') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>


                                    <div class="col-lg-12 col-sm-12 {{$errors->has('phone2') ? 'has-error' : ''}}">
                                        {{ Form::label('Customername','Phone 2 : ',['class'=>'control-label'])}}
                                        {{ Form::text('phone2',old('phone2'),['class'=>'form-control','placeholder'=>'Ex: Customer Name','id'=>'name'])}}
                                        @if ($errors->has('phone2'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('phone2') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('image') ? 'has-error' : ''}}">

                                        {{ Form::label('Customerimage','Image : ',['class'=>'control-label'])}}
                                        {{ Form::file('image',old('image'),['class'=>'form-control','required'])}}
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>



                                    <div class="col-lg-12 col-sm-12 {{$errors->has('address') ? 'has-error' : ''}}">

                                        {{ Form::label('Customeraddress','Address : ',['class'=>'control-label'])}}
                                        {{ Form::textarea('address',null,['class'=>'form-control','placeholder'=>'Ex: Customer Address','id'=>'address'])}}
                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif

                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('SAVE EMPLOYEE',['type'=>'submit','id'=>'saveCustomer','class'=>'btn btn-primary']) }}
                                        {{ Form::hidden('canceledit',"Cancel Edit",['type'=>'reset','id'=>'cancel','class'=>'btn btn-danger']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12 table-responsive">
                                    <table class="table table-bordered table-striped" id="brandTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Father </th>
                                            <th>Mother </th>
                                            <th>Wife </th>
                                            <th>Nid </th>
                                            <th>Phone1 </th>
                                            <th>Salary </th>
                                            <th>Image </th>
                                            <th>Address</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                        @foreach($employees as $info)
                                            <tr id="rowid{{$info->id}}" class="abcd">
                                                <td>{{++$i}}</td>
                                                <td id="name{{ $info->id }}" data-id="{{ $info->name }}">{{$info->name}}</td>
                                                <td id="name{{ $info->id }}" data-id="{{ $info->father }}">{{$info->father}}</td>
                                                <td id="name{{ $info->id }}" data-id="{{ $info->mother }}">{{$info->mother}}</td>
                                                <td id="name{{ $info->id }}" data-id="{{ $info->wife }}">{{$info->wife}}</td>
                                                <td id="name{{ $info->id }}" data-id="{{ $info->nid }}">{{$info->nid}}</td>
                                                <td id="name{{ $info->id }}" data-id="{{ $info->phone1 }}">{{$info->phone1}}</td>
                                                <td id="phone{{ $info->id }}" data-id="{{ $info->salary }}">{{$info->salary}}</td>
                                                <td id="phone{{ $info->id }}" data-id="{{ $info->image }}"><img src="{{ asset('public/admin/product/upload/'.$info->image) }}" style="height: 60px;width: 60px;"></td>
                                                <td id="address{{ $info->id }}" data-id="{{ $info->address }}">
                                                    @if(!empty($info->address))
                                                        {{ substr($info->address,0,10) }}
                                                    @else
                                                        <p class="text-center">no address</p>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a class="btn btn-sm btn-info edit" data-id="{{ $info->id }}" href="{{ route('employee.edit',$info->id) }}"><i class="demo-pli-pen-5"></i></a> ||
                                                    <button class="btn btn-sm btn-danger erase" data-id="{{$info->id}}" data-url="{{url('Employee/erase')}}">
                                                        <i class="demo-pli-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#brandTable').DataTable();
            $("#canceledit").hide();
        });

        /* edit request start */
        $(document).on('click','.edit',function(){
            $("#cancel").attr("type","button");
            var id = $(this).attr('data-id');
            var name = $("#name"+id).attr('data-id');
            var phone = $("#phone"+id).attr('data-id');
            var address = $("#address"+id).attr('data-id');
            var route = '{{ route("customer.update") }}';
            $("#name").val(name);
            $("#phone").val(phone);
            $("#address").val(address);
            $("#id").val(id);
            $('#CustomerForm').attr('action',route);
            $("#saveCustomer").text("Update Customer");
            $("#saveCustomer").attr('class','btn btn-info');
            $("#cancel").attr('type','reset');
        });

        $("#cancel").click(function(){
            var route = '{{ route("customer.add") }}';
            $("#CustomerForm").attr('action',route);
            $("#id").val("");
            $("#saveCustomer").text("Add Customer");
            $("#saveCustomer").attr('class','btn btn-primary');
            $(this).hide(299)
        });
        /* edit request end */
    </script>

@endsection