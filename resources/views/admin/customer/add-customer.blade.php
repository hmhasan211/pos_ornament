@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Brand</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Brand</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Brand</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    {{ Form::open(['route'=>'customer.store','method'=>'post','id'=>'CustomerForm','enctype'=>'multipart/form-data']) }}

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::hidden('id',null,['class'=>'form-control','id'=>'id']) }}
                                        {{ Form::label('Customername','Customer Name : ',['class'=>'control-label'])}}
                                        {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Ex: Customer Name','id'=>'name'])}}
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('phone') ? 'has-error' : ''}}">

                                        {{ Form::label('Customerphone','Customer Phone : ',['class'=>'control-label'])}}
                                        {{ Form::text('phone',old('phone'),['class'=>'form-control','placeholder'=>'Ex: Customer Name','id'=>'phone'])}}
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>



                                    <div class="col-lg-12 col-sm-12 {{$errors->has('customer_image') ? 'has-error' : ''}}">

                                        {{ Form::label('Customerimage','Customer Image : ',['class'=>'control-label'])}}
                                        {{ Form::file('customer_image',old('customer_image'),['class'=>'form-control','required'])}}
                                        @if ($errors->has('customer_image'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('customer_image') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    </div>



                                    <div class="col-lg-12 col-sm-12 {{$errors->has('address') ? 'has-error' : ''}}">

                                        {{ Form::label('Customeraddress','Address : ',['class'=>'control-label'])}}
                                        {{ Form::textarea('address',null,['class'=>'form-control','placeholder'=>'Ex: Customer Address','id'=>'address'])}}
                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif

                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('Save Customer',['type'=>'submit','id'=>'saveCustomer','class'=>'btn btn-primary']) }}
                                        {{ Form::hidden('canceledit',"Cancel Edit",['type'=>'reset','id'=>'cancel','class'=>'btn btn-danger']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                    <table class="table table-bordered table-striped" id="brandTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Customer Name</th>
                                            <th>Phone </th>
                                            <th>Address</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                            @foreach($customers as $info)
                                                <tr id="rowid{{$info->id}}" class="abcd">
                                                    <td>{{++$i}}</td>
                                                    <td id="name{{ $info->id }}" data-id="{{ $info->name }}">{{$info->name}}</td>
                                                    <td id="phone{{ $info->id }}" data-id="{{ $info->phone }}">{{$info->phone}}</td>
                                                    <td id="address{{ $info->id }}" data-id="{{ $info->address }}">
                                                        @if(!empty($info->address))
                                                            {{ $info->address }}
                                                        @else
                                                            <p class="text-center">no address found</p>
                                                        @endif


                                                    </td>
                                                    <td>
                                                        <a class="btn btn-sm btn-info edit" data-id="{{ $info->id }}"><i class="demo-pli-pen-5"></i></a> ||
                                                        <button class="btn btn-sm btn-danger erase" data-id="{{$info->id}}" data-url="{{url('CustomerManagement/Customer/erase')}}"><i class="demo-pli-trash"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#brandTable').DataTable();
            $("#canceledit").hide();
        });

        /* edit request start */
        $(document).on('click','.edit',function(){
            $("#cancel").attr("type","button");
            var id = $(this).attr('data-id');
            var name = $("#name"+id).attr('data-id');
            var phone = $("#phone"+id).attr('data-id');
            var address = $("#address"+id).attr('data-id');
            var route = '{{ route("customer.update") }}';
            $("#name").val(name);
            $("#phone").val(phone);
            $("#address").val(address);
            $("#id").val(id);
            $('#CustomerForm').attr('action',route);
            $("#saveCustomer").text("Update Customer");
            $("#saveCustomer").attr('class','btn btn-info');
            $("#cancel").attr('type','reset');
        });

        $("#cancel").click(function(){
            var route = '{{ route("customer.add") }}';
            $("#CustomerForm").attr('action',route);
            $("#id").val("");
            $("#saveCustomer").text("Add Customer");
            $("#saveCustomer").attr('class','btn btn-primary');
            $(this).hide(299)
        });
        /* edit request end */
    </script>

@endsection