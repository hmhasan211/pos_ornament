@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add New Purchase</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Product</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    
                    <div class="panel">

                        <div class="panel-heading">
                            <h3 class="panel-title">Add New Purchase</h3>
                        </div>


                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">

                                    <div class="row">
                                        <!--  PRODUCT Brand NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('id','Id',['class'=>'control-label'])}}
                                            {{ Form::select('id',['1'=>'1st','2'=>'2nd','3'=>'3rd'],false,['class'=>'form-control','required','id'=>'id'])}}

                                        </div>
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('name','Name',['class'=>'control-label'])}}
                                            {{ Form::text('name',null,['class'=>'form-control','required','id'=>'name'])}}

                                        </div>
                                        <!-- / PRODUCT Brand NAME  -->

                                        <!--  PRODUCT category_id NAME  -->

                                        <!-- / PRODUCT category_id NAME  -->

                                        <!--  PRODUCT subcategory_id NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('price') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('rate','Rate',['class'=>'control-label'])}}
                                            {{ Form::text('rate',null,['class'=>'form-control','required','id'=>'price'])}}
                                        </div>
                                        <!-- / PRODUCT subcategory_id NAME  -->

                                    </div>{{-- Brand Category Sub-Category Row End here  --}}

                                    <div class="row">

                                        <!--  PRODUCT SIZE NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('qty') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('qty','Quantity',['class'=>'control-label'])}}
                                            {{ Form::text('qty',null,['class'=>'form-control','id'=>'qty'])}}
                                        </div>

                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('country') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('country','Country',['class'=>'control-label'])}}
                                            {{ Form::select('country',['1'=>'Bangladesh','2'=>'India','3'=>'Thailand'],false,['class'=>'form-control','required','id'=>'country'])}}

                                        </div>
                                        <!-- / PRODUCT SIZE NAME  -->

                                        <!--  PRODUCT color NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('brand') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('brand','Brand',['class'=>'control-label'])}}
                                            
                                            {{ Form::text('brand',null,['class'=>'form-control','required','id'=>'brand'])}}
                                        </div>
                                        <!-- / PRODUCT color NAME  -->

                                        <!--  PRODUCT karat NAME  -->

                                    </div>{{-- Size Karat Color Row End Here --}}

                                   



                                    {{-- Product Qty Price and Rate Here --}}



                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            <div class="form-group">
                                                <button class="btn btn-info form-control" type="button" id="add_bucket">
                                                    Add To Bucket
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 pull-right">
                                    <h1 class="text-center bg-primary">Bucket List</h1>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Rate </th>
                                            <th>Quantity</th>
                                            <th>Country</th>
                                            <th>Brnad</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="cart_items">

                                        </tbody>
                                    </table>
                                    <hr>

                                    {{-- suplier information added start from here --}}



                                </div>
                            </div>
                        </div><!--  Panel Body Close -->

                    </div><!-- Panel Close -->

                </div><!-- Row Close -->
           </div><!-- Page Content Close -->
        </div>
    </div>
    <script>
        $(document).ready(function(){
            Carts();
        });


        $(document).on('click','#add_bucket',function(){
            var brand = $("#brand").val();
            var id = $("#id").val();
            var name = $("#name").val();
            var qty = $("#qty").val();
            var rate = $("#price").val();
            var country = $("#country").val();
            if(rate < 0 ||  qty <= 0){
                alert('Fields can not be empty');
                }else{
                    $.ajax({
                        method : "post",
                        url : "{{ route('democart.add') }}",
                        data:{id:id,name:name,"_token":'{{ csrf_token() }}',qty:qty,price:rate,country:country,brand:brand},
                        dataType:'html',
                        success:function(response){
                            console.log(response);
                            $('#cart_items').html(response);

                        },
                        error:function(err){
                            console.log(err);
                        }
                    });
                }
        });

        /* onload added products fetch */

        function Carts(){
            $.ajax({
                method:"get",
                url : "{{ route('democart.carts') }}",
                dataType:'html',
                success:function(response){
                    $("#cart_items").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            })
        }

        /* onload added products fetch */

        /*onclick update qty and price start*/

        $(document).on('click','#update_item',function () {
           var token= '{{ csrf_token() }}';
            var rowId = $(this).attr('data-id');
            var qty=$('#cart_qty_edit').val();
            var price=$('#cart_price_edit').val();
            $.ajax({
                method:"post",
                url:'{{ route("democart.update") }}',
                data:{rowId:rowId,"_token":token,qty:qty,price:price},
                dataType:"html",
                success:function(response){
                    location.reload();
                },
                error:function(err){
                    console.log(err);
                }
            });
        });

        /*onclick update qty and price end*/
        /*  Cart Remove Start */
        $(document).on('click','#remove_item',function(){
            var token = '{{ csrf_token() }}';
            var rowId = $(this).attr('data-id');
            $.ajax({
                method:"post",
                url:'{{ route("democart.remove") }}',
                data:{rowId:rowId,"_token":token},
                dataType:"html",
                success:function(response){
                    $("#cart_items").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        });
        /*  Cart Remove End */
    </script>

   @endsection