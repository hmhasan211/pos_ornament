-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2018 at 04:24 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `barcodes`
--

CREATE TABLE `barcodes` (
  `id` int(10) UNSIGNED NOT NULL,
  `barcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stockin_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `stockout_id` int(10) UNSIGNED DEFAULT NULL,
  `gold_maker_id` int(10) UNSIGNED DEFAULT NULL,
  `ext2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `barcodes`
--

INSERT INTO `barcodes` (`id`, `barcode`, `stockin_id`, `product_id`, `stockout_id`, `gold_maker_id`, `ext2`, `created_at`, `updated_at`) VALUES
(18, '000003000004', 18, 3, 3, NULL, NULL, '2018-11-11 05:54:05', '2018-11-11 22:13:58'),
(19, '000002000003', 19, 2, NULL, 2, NULL, '2018-11-11 05:54:05', '2018-11-12 00:38:51'),
(20, '000003000005', 20, 3, NULL, NULL, NULL, '2018-11-14 03:57:37', '2018-11-14 03:57:37'),
(21, '000002000004', 21, 2, NULL, NULL, NULL, '2018-11-15 11:36:40', '2018-11-15 11:36:40'),
(22, '000001000002', 22, 1, NULL, NULL, NULL, '2018-11-15 11:43:06', '2018-11-15 11:43:06'),
(23, '000003000006', 23, 3, 4, NULL, NULL, '2018-11-15 11:50:44', '2018-11-15 11:56:39'),
(24, '000003000007', 24, 3, 5, NULL, NULL, '2018-11-15 11:50:44', '2018-11-15 11:56:40'),
(25, '000003000008', 25, 3, 6, NULL, NULL, '2018-11-15 11:51:29', '2018-11-15 11:56:40');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Mikimoto', '2018-11-10 23:29:38', '2018-11-10 23:29:38'),
(2, 'Kalliyan', '2018-11-10 23:29:43', '2018-11-10 23:29:43'),
(3, 'TZB', '2018-11-10 23:29:48', '2018-11-10 23:29:48');

-- --------------------------------------------------------

--
-- Table structure for table `cash_books`
--

CREATE TABLE `cash_books` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `expense` double(8,2) DEFAULT NULL,
  `income` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cash_books`
--

INSERT INTO `cash_books` (`id`, `invoice_id`, `expense`, `income`, `created_at`, `updated_at`) VALUES
(14, 14, 60000.00, NULL, '2018-11-11 05:54:05', '2018-11-11 05:54:05'),
(15, 14, 9900.00, NULL, '2018-11-11 05:55:23', '2018-11-11 05:55:23'),
(16, 15, 29000.00, NULL, '2018-11-11 22:13:58', '2018-11-11 22:13:58'),
(17, 15, 257.00, NULL, '2018-11-11 22:44:56', '2018-11-11 22:44:56'),
(20, 16, 40000.00, NULL, '2018-11-12 00:38:52', '2018-11-12 00:38:52'),
(21, 17, 68000.00, NULL, '2018-11-14 03:57:37', '2018-11-14 03:57:37'),
(22, 17, 600.00, NULL, '2018-11-14 04:03:22', '2018-11-14 04:03:22'),
(23, 18, 38000.00, NULL, '2018-11-15 11:36:40', '2018-11-15 11:36:40'),
(24, 18, 500.00, NULL, '2018-11-15 11:36:51', '2018-11-15 11:36:51'),
(25, 19, 38000.00, NULL, '2018-11-15 11:43:06', '2018-11-15 11:43:06'),
(26, 19, 500.00, NULL, '2018-11-15 11:45:45', '2018-11-15 11:45:45'),
(27, 20, 1200.00, NULL, '2018-11-15 11:50:44', '2018-11-15 11:50:44'),
(28, 21, 70000.00, NULL, '2018-11-15 11:51:29', '2018-11-15 11:51:29'),
(29, 22, 0.00, NULL, '2018-11-15 11:56:40', '2018-11-15 11:56:40');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `cimage`, `created_at`, `updated_at`) VALUES
(1, 'Head', NULL, '2018-11-10 23:30:22', '2018-11-10 23:30:22'),
(2, 'Hands ( হাত )', NULL, '2018-11-10 23:30:26', '2018-11-10 23:30:26');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Gold', '2018-11-10 23:31:03', '2018-11-10 23:31:03'),
(2, 'Diamond', '2018-11-10 23:31:07', '2018-11-10 23:31:07'),
(3, 'Silver', '2018-11-10 23:31:11', '2018-11-10 23:31:11');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `phone`, `image`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Md. Muslim', '01945737858', '', 'Chittagong', '2018-11-11 04:57:38', '2018-11-11 04:57:38'),
(2, 'Md. Emran', '01945737825', '', 'address', '2018-11-11 05:44:40', '2018-11-11 05:44:40'),
(3, 'Mr.X Customer', '01829894659', '', 'address', '2018-11-15 11:56:39', '2018-11-15 11:56:39');

-- --------------------------------------------------------

--
-- Table structure for table `gold_makers`
--

CREATE TABLE `gold_makers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pre_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gold_makers`
--

INSERT INTO `gold_makers` (`id`, `name`, `phone`, `nid`, `address`, `image`, `age`, `per_address`, `pre_address`, `gender`, `nationality`, `created_at`, `updated_at`) VALUES
(2, 'Mr. GoldMaker', '01945737858', '', 'address', '2018_71e6a8.jpg', NULL, NULL, NULL, NULL, NULL, '2018-11-12 00:30:53', '2018-11-12 00:30:53');

-- --------------------------------------------------------

--
-- Table structure for table `gold_types`
--

CREATE TABLE `gold_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gold_types`
--

INSERT INTO `gold_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'New', '2018-11-10 23:33:48', '2018-11-10 23:33:48'),
(2, 'Old', '2018-11-10 23:33:52', '2018-11-10 23:33:52');

-- --------------------------------------------------------

--
-- Table structure for table `instocks`
--

CREATE TABLE `instocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_price` double(8,2) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `total_price` double(8,2) NOT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `vori` int(11) DEFAULT NULL,
  `ana` int(11) DEFAULT NULL,
  `roti` int(11) DEFAULT NULL,
  `miliroti` int(11) DEFAULT NULL,
  `ext1` int(11) DEFAULT NULL,
  `ext2` int(11) DEFAULT NULL,
  `ext3` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoice_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_id` int(10) UNSIGNED DEFAULT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `gold_maker_id` int(10) UNSIGNED DEFAULT NULL,
  `total_amount` double(8,2) NOT NULL,
  `total_less` double(8,2) DEFAULT NULL,
  `total_paid` double(8,2) DEFAULT NULL,
  `total_balance` double(8,2) DEFAULT NULL,
  `total_discount` double(8,2) DEFAULT NULL,
  `payment_deadline` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_trxid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ext1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extstring2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ext2` int(11) NOT NULL,
  `ext3` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_makingcharge` float(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `invoice_no`, `supplier_id`, `customer_id`, `gold_maker_id`, `total_amount`, `total_less`, `total_paid`, `total_balance`, `total_discount`, `payment_deadline`, `payment_type`, `payment_trxid`, `note`, `ext1`, `extstring2`, `ext2`, `ext3`, `created_at`, `updated_at`, `total_makingcharge`) VALUES
(14, '2018-11-11_843575', 2, NULL, NULL, 70600.00, 500.00, 60000.00, 9900.00, 200.00, '11/13/2018', 'cash', '0', 'Thank you so much for stay with Ringer-Soft jewellery Shop', NULL, NULL, 0, 0, '2018-11-11 05:54:04', '2018-11-11 05:54:04', 600.00),
(15, '2018-11-12_7ec120', NULL, 1, NULL, 30200.00, 500.00, 29000.00, 257.00, 443.00, '11/20/2018', 'cash', NULL, 'Thank you so much for stay with Ringer-Soft jewellery Shop', NULL, NULL, 0, 0, '2018-11-11 22:13:58', '2018-11-11 22:13:58', 200.00),
(16, '2018-11-12_3a3283', NULL, NULL, 2, 40000.00, NULL, 40000.00, 0.00, 0.00, NULL, '0', NULL, '0', NULL, NULL, 0, 0, '2018-11-12 00:38:51', '2018-11-12 00:38:51', NULL),
(17, '2018-11-14_c0b528', 2, NULL, NULL, 69106.25, 500.00, 68000.00, 600.00, 6.25, '11/27/2018', 'cash', '0', 'Thank you so much for stay with Ringer-Soft jewellery Shop', NULL, NULL, 0, 0, '2018-11-14 03:57:37', '2018-11-14 03:57:37', 200.00),
(18, '2018-11-15_b4ee61', 2, NULL, NULL, 40200.00, 200.00, 38000.00, 2000.00, 0.00, '11/22/2018', 'cash', '0', 'Thank you so much for stay with Ringer-Soft jewellery Shop', NULL, NULL, 0, 0, '2018-11-15 11:36:40', '2018-11-15 11:36:40', 200.00),
(19, '2018-11-15_783bc1', 4, NULL, NULL, 39845.00, 800.00, 38000.00, 1000.00, 45.00, '11/20/2018', 'cash', '0', 'Thank you so much for stay with Ringer-Soft jewellery Shop', NULL, NULL, 0, 0, '2018-11-15 11:43:05', '2018-11-15 11:43:05', 200.00),
(20, '2018-11-15_ba9480', 2, NULL, NULL, 129106.25, 200.00, 1200.00, 127706.25, 0.00, '11/28/2018', 'cash', '0', 'Thank you so much for stay with Ringer-Soft jewellery Shop', NULL, NULL, 0, 0, '2018-11-15 11:50:44', '2018-11-15 11:50:44', 200.00),
(21, '2018-11-15_f78528', 2, NULL, NULL, 71252.00, 200.00, 70000.00, 552.00, 500.00, '11/21/2018', 'cash', '0', 'Thank you so much for stay with Ringer-Soft jewellery Shop', NULL, NULL, 0, 0, '2018-11-15 11:51:29', '2018-11-15 11:51:29', 2.00),
(22, '2018-11-15_1dda7f', NULL, 3, NULL, 200156.25, 50000.00, 0.00, 199856.25, 500.00, '11/21/2018', 'cash', NULL, 'Thank you so much for stay with Ringer-Soft jewellery Shop', NULL, NULL, 0, 0, '2018-11-15 11:56:39', '2018-11-15 11:56:39', 200.00);

-- --------------------------------------------------------

--
-- Table structure for table `karats`
--

CREATE TABLE `karats` (
  `id` int(10) UNSIGNED NOT NULL,
  `karat_size` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `karats`
--

INSERT INTO `karats` (`id`, `karat_size`, `created_at`, `updated_at`) VALUES
(1, 10, '2018-11-10 23:31:34', '2018-11-10 23:31:34'),
(2, 11, '2018-11-10 23:31:37', '2018-11-10 23:31:37'),
(3, 21, '2018-11-10 23:31:43', '2018-11-10 23:31:43'),
(4, 22, '2018-11-10 23:31:46', '2018-11-10 23:31:46'),
(5, 24, '2018-11-10 23:31:49', '2018-11-10 23:31:49');

-- --------------------------------------------------------

--
-- Table structure for table `localcosts`
--

CREATE TABLE `localcosts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `ext1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ext2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ext3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ext4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `localcosts`
--

INSERT INTO `localcosts` (`id`, `name`, `amount`, `ext1`, `ext2`, `ext3`, `ext4`, `created_at`, `updated_at`) VALUES
(2, 'Tea', 49, NULL, NULL, NULL, NULL, '2018-11-12 02:50:12', '2018-11-12 02:50:12'),
(3, 'Paan', 10, NULL, NULL, NULL, NULL, '2018-11-12 03:01:26', '2018-11-12 03:01:26');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_06_084719_create_brands_table', 1),
(4, '2018_09_09_042150_create_categories_table', 1),
(5, '2018_09_09_042151_create_sub_categories_table', 1),
(6, '2018_09_09_055910_create_colors_table', 1),
(7, '2018_09_09_055919_create_sizes_table', 1),
(8, '2018_09_09_070739_create_karats_table', 1),
(9, '2018_09_09_070740_create_origins_table', 1),
(10, '2018_09_10_073230_create_gold_types_table', 1),
(11, '2018_09_15_102546_create_weights_table', 1),
(12, '2018_09_22_050307_create_supliers_table', 1),
(13, '2018_09_22_050308_create_products_table', 1),
(14, '2018_09_22_050309_create_customers_table', 1),
(15, '2018_09_22_050310_create_gold_makers_table', 1),
(16, '2018_09_22_113007_create_invoices_table', 1),
(17, '2018_09_22_113650_create_stockins_table', 1),
(18, '2018_09_22_113651_create_stockouts_table', 1),
(19, '2018_09_23_113653_create_out_stocks_table', 1),
(20, '2018_09_23_113654_create_in_stocks_table', 1),
(21, '2018_09_23_113655_create_barcodes_table', 1),
(22, '2018_09_24_055216_create_cash_books_table', 1),
(23, '2018_10_28_050350_create_price_settings_table', 1),
(24, '2018_11_12_065935_create_local_costs_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `origins`
--

CREATE TABLE `origins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `origins`
--

INSERT INTO `origins` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'UAE', '2018-11-10 23:31:57', '2018-11-10 23:31:57'),
(2, 'INDIA', '2018-11-10 23:32:03', '2018-11-10 23:32:03'),
(3, 'USA', '2018-11-10 23:32:08', '2018-11-10 23:32:08'),
(4, 'QATAR', '2018-11-10 23:32:12', '2018-11-10 23:32:12'),
(5, 'DUBAI', '2018-11-10 23:32:20', '2018-11-10 23:32:20'),
(6, 'BANGLADESH', '2018-11-10 23:32:24', '2018-11-10 23:32:24');

-- --------------------------------------------------------

--
-- Table structure for table `outstocks`
--

CREATE TABLE `outstocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_price` double(8,2) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `total_price` double(8,2) NOT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `vori` int(11) DEFAULT NULL,
  `ana` int(11) DEFAULT NULL,
  `roti` int(11) DEFAULT NULL,
  `miliroti` int(11) DEFAULT NULL,
  `ext1` int(11) DEFAULT NULL,
  `ext2` int(11) DEFAULT NULL,
  `ext3` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `outstocks`
--

INSERT INTO `outstocks` (`id`, `product_price`, `product_qty`, `total_price`, `invoice_id`, `product_id`, `vori`, `ana`, `roti`, `miliroti`, `ext1`, `ext2`, `ext3`, `created_at`, `updated_at`) VALUES
(1, 40000.00, 1, 40000.00, 16, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-12 00:38:52', '2018-11-12 00:38:52');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pricesettings`
--

CREATE TABLE `pricesettings` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight_id` int(10) UNSIGNED NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_local_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` int(11) NOT NULL,
  `product_details` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gold_type_id` int(10) UNSIGNED NOT NULL,
  `brand_id` int(10) UNSIGNED NOT NULL,
  `categorie_id` int(10) UNSIGNED NOT NULL,
  `sub_categorie_id` int(10) UNSIGNED NOT NULL,
  `size_id` int(10) UNSIGNED NOT NULL,
  `karat_id` int(10) UNSIGNED NOT NULL,
  `origin_id` int(10) UNSIGNED NOT NULL,
  `color_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `product_code`, `product_local_name`, `product_price`, `product_details`, `product_image`, `gold_type_id`, `brand_id`, `categorie_id`, `sub_categorie_id`, `size_id`, `karat_id`, `origin_id`, `color_id`, `created_at`, `updated_at`, `isActive`) VALUES
(1, 'Golden Ring', 'PIVU', 'local name', 39645, 'details', '2018_c14bdd.jpg', 1, 1, 2, 2, 1, 4, 5, 2, '2018-11-10 23:35:05', '2018-11-10 23:35:05', 1),
(2, 'Boys Bracelet', 'PX1A', 'local name', 40000, 'product details', '2018_0b762d.png', 1, 2, 2, 3, 2, 5, 4, 2, '2018-11-10 23:37:02', '2018-11-10 23:37:02', 0),
(3, 'TT', 'PT20', 'local name', 30000, 'DD', '2018_06695e.jpg', 1, 1, 2, 2, 1, 4, 3, 1, '2018-11-11 05:36:25', '2018-11-11 05:36:25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Small', '2018-11-10 23:31:19', '2018-11-10 23:31:19'),
(2, 'Large', '2018-11-10 23:31:26', '2018-11-10 23:31:26');

-- --------------------------------------------------------

--
-- Table structure for table `stockins`
--

CREATE TABLE `stockins` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_price` double(8,2) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `total_price` double(8,2) NOT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `vori` int(11) DEFAULT NULL,
  `ana` int(11) DEFAULT NULL,
  `roti` int(11) DEFAULT NULL,
  `miliroti` int(11) DEFAULT NULL,
  `ext1` int(11) DEFAULT NULL,
  `ext2` int(11) DEFAULT NULL,
  `ext3` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stockins`
--

INSERT INTO `stockins` (`id`, `product_price`, `product_qty`, `total_price`, `invoice_id`, `product_id`, `vori`, `ana`, `roti`, `miliroti`, `ext1`, `ext2`, `ext3`, `created_at`, `updated_at`) VALUES
(18, 30000.00, 1, 30000.00, 14, 3, 1, 0, 0, 0, NULL, NULL, NULL, '2018-11-11 05:54:05', '2018-11-11 05:54:05'),
(19, 40000.00, 1, 40000.00, 14, 2, 1, 0, 0, 0, NULL, NULL, NULL, '2018-11-11 05:54:05', '2018-11-11 05:54:05'),
(20, 30000.00, 1, 68906.25, 17, 3, 2, 4, 4, 5, NULL, NULL, NULL, '2018-11-14 03:57:37', '2018-11-14 03:57:37'),
(21, 40000.00, 1, 40000.00, 18, 2, 1, 0, 0, 0, NULL, NULL, NULL, '2018-11-15 11:36:40', '2018-11-15 11:36:40'),
(22, 39645.00, 1, 39645.00, 19, 1, 1, 0, 0, 0, NULL, NULL, NULL, '2018-11-15 11:43:05', '2018-11-15 11:43:05'),
(23, 30000.00, 1, 68906.25, 20, 3, 2, 4, 4, 5, NULL, NULL, NULL, '2018-11-15 11:50:44', '2018-11-15 11:50:44'),
(24, 30000.00, 1, 60000.00, 20, 3, 2, 0, 0, 0, NULL, NULL, NULL, '2018-11-15 11:50:44', '2018-11-15 11:50:44'),
(25, 30000.00, 1, 71250.00, 21, 3, 2, 6, 0, 0, NULL, NULL, NULL, '2018-11-15 11:51:29', '2018-11-15 11:51:29');

-- --------------------------------------------------------

--
-- Table structure for table `stockouts`
--

CREATE TABLE `stockouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_price` double(8,2) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `total_price` double(8,2) NOT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `vori` int(11) DEFAULT NULL,
  `ana` int(11) DEFAULT NULL,
  `roti` int(11) DEFAULT NULL,
  `miliroti` int(11) DEFAULT NULL,
  `ext1` int(11) DEFAULT NULL,
  `ext2` int(11) DEFAULT NULL,
  `ext3` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stockouts`
--

INSERT INTO `stockouts` (`id`, `product_price`, `product_qty`, `total_price`, `invoice_id`, `product_id`, `vori`, `ana`, `roti`, `miliroti`, `ext1`, `ext2`, `ext3`, `created_at`, `updated_at`) VALUES
(1, 40000.00, 1, 120000.00, 11, 2, 3, 0, 0, 0, NULL, NULL, NULL, '2018-11-11 04:57:39', '2018-11-11 04:57:39'),
(2, 30000.00, 1, 30000.00, 13, 3, 1, 0, 0, 0, NULL, NULL, NULL, '2018-11-11 05:44:40', '2018-11-11 05:44:40'),
(3, 30000.00, 1, 30000.00, 15, 3, 1, 0, 0, 0, NULL, NULL, NULL, '2018-11-11 22:13:58', '2018-11-11 22:13:58'),
(4, 30000.00, 1, 68906.25, 22, 3, 2, 4, 4, 5, NULL, NULL, NULL, '2018-11-15 11:56:39', '2018-11-15 11:56:39'),
(5, 30000.00, 1, 60000.00, 22, 3, 2, 0, 0, 0, NULL, NULL, NULL, '2018-11-15 11:56:39', '2018-11-15 11:56:39'),
(6, 30000.00, 1, 71250.00, 22, 3, 2, 6, 0, 0, NULL, NULL, NULL, '2018-11-15 11:56:40', '2018-11-15 11:56:40');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Tiara', 1, '2018-11-10 23:30:39', '2018-11-10 23:30:39'),
(2, 'Rings', 2, '2018-11-10 23:30:47', '2018-11-10 23:30:47'),
(3, 'Bracelet', 2, '2018-11-10 23:30:54', '2018-11-10 23:30:54');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `phone`, `image`, `address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Md Ibrahim Tinku', '01688491670', '2018_0035df.jpg', 'address', '2018-11-10 23:39:11', '2018-11-10 23:39:11', NULL),
(2, 'Md Rimon', '012345678912', '2018_4837e2.jpg', 'address', '2018-11-11 00:06:41', '2018-11-11 00:06:41', NULL),
(3, 'SHUKKUR', '01811223344', '2018_3d9a19.jpg', 'ADDRESS', '2018-11-11 05:38:52', '2018-11-11 05:38:52', NULL),
(4, 'Mr.X Supllier', '01765797915', '2018_783bc1.png', 'address', '2018-11-15 11:43:05', '2018-11-15 11:43:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usertype` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `usertype`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'AL-Hamid Jewellery', 'alhamid', 'admin', 'support@ringersoft.com', '$2y$10$xKwAFiCtSTy7tZb1gXfZNO2AF6qw2ubEsUoq7927z6lF/1s0FWjt6', 'dQmhVGuCsLC2TVvYZnSYcsFFAbVU3BhM4W09qSXXzLjYxWp9n5AQZYuJe5Ky', '2018-11-10 23:29:18', '2018-11-10 23:29:18');

-- --------------------------------------------------------

--
-- Table structure for table `weights`
--

CREATE TABLE `weights` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `weights`
--

INSERT INTO `weights` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Gram', '2018-11-10 23:32:39', '2018-11-10 23:32:39'),
(2, 'Ounce', '2018-11-10 23:33:42', '2018-11-10 23:33:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barcodes`
--
ALTER TABLE `barcodes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barcodes_stockin_id_foreign` (`stockin_id`),
  ADD KEY `barcodes_product_id_foreign` (`product_id`),
  ADD KEY `barcodes_stockout_id_foreign` (`stockout_id`),
  ADD KEY `barcodes_gold_maker_id_foreign` (`gold_maker_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cash_books`
--
ALTER TABLE `cash_books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cash_books_invoice_id_foreign` (`invoice_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gold_makers`
--
ALTER TABLE `gold_makers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gold_types`
--
ALTER TABLE `gold_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instocks`
--
ALTER TABLE `instocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `instocks_invoice_id_foreign` (`invoice_id`),
  ADD KEY `instocks_product_id_foreign` (`product_id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_supplier_id_foreign` (`supplier_id`),
  ADD KEY `invoices_customer_id_foreign` (`customer_id`),
  ADD KEY `invoices_gold_maker_id_foreign` (`gold_maker_id`);

--
-- Indexes for table `karats`
--
ALTER TABLE `karats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `localcosts`
--
ALTER TABLE `localcosts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `origins`
--
ALTER TABLE `origins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outstocks`
--
ALTER TABLE `outstocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `outstocks_invoice_id_foreign` (`invoice_id`),
  ADD KEY `outstocks_product_id_foreign` (`product_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pricesettings`
--
ALTER TABLE `pricesettings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pricesettings_weight_id_foreign` (`weight_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_gold_type_id_foreign` (`gold_type_id`),
  ADD KEY `products_brand_id_foreign` (`brand_id`),
  ADD KEY `products_categorie_id_foreign` (`categorie_id`),
  ADD KEY `products_sub_categorie_id_foreign` (`sub_categorie_id`),
  ADD KEY `products_size_id_foreign` (`size_id`),
  ADD KEY `products_karat_id_foreign` (`karat_id`),
  ADD KEY `products_origin_id_foreign` (`origin_id`),
  ADD KEY `products_color_id_foreign` (`color_id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stockins`
--
ALTER TABLE `stockins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stockins_invoice_id_foreign` (`invoice_id`),
  ADD KEY `stockins_product_id_foreign` (`product_id`);

--
-- Indexes for table `stockouts`
--
ALTER TABLE `stockouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stockouts_invoice_id_foreign` (`invoice_id`),
  ADD KEY `stockouts_product_id_foreign` (`product_id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `weights`
--
ALTER TABLE `weights`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barcodes`
--
ALTER TABLE `barcodes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cash_books`
--
ALTER TABLE `cash_books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gold_makers`
--
ALTER TABLE `gold_makers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `gold_types`
--
ALTER TABLE `gold_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `instocks`
--
ALTER TABLE `instocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `karats`
--
ALTER TABLE `karats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `localcosts`
--
ALTER TABLE `localcosts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `origins`
--
ALTER TABLE `origins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `outstocks`
--
ALTER TABLE `outstocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pricesettings`
--
ALTER TABLE `pricesettings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stockins`
--
ALTER TABLE `stockins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `stockouts`
--
ALTER TABLE `stockouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `weights`
--
ALTER TABLE `weights`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barcodes`
--
ALTER TABLE `barcodes`
  ADD CONSTRAINT `barcodes_gold_maker_id_foreign` FOREIGN KEY (`gold_maker_id`) REFERENCES `gold_makers` (`id`),
  ADD CONSTRAINT `barcodes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `barcodes_stockin_id_foreign` FOREIGN KEY (`stockin_id`) REFERENCES `stockins` (`id`),
  ADD CONSTRAINT `barcodes_stockout_id_foreign` FOREIGN KEY (`stockout_id`) REFERENCES `stockouts` (`id`);

--
-- Constraints for table `cash_books`
--
ALTER TABLE `cash_books`
  ADD CONSTRAINT `cash_books_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`);

--
-- Constraints for table `instocks`
--
ALTER TABLE `instocks`
  ADD CONSTRAINT `instocks_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  ADD CONSTRAINT `instocks_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `invoices_gold_maker_id_foreign` FOREIGN KEY (`gold_maker_id`) REFERENCES `gold_makers` (`id`),
  ADD CONSTRAINT `invoices_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`);

--
-- Constraints for table `outstocks`
--
ALTER TABLE `outstocks`
  ADD CONSTRAINT `outstocks_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  ADD CONSTRAINT `outstocks_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `pricesettings`
--
ALTER TABLE `pricesettings`
  ADD CONSTRAINT `pricesettings_weight_id_foreign` FOREIGN KEY (`weight_id`) REFERENCES `weights` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  ADD CONSTRAINT `products_categorie_id_foreign` FOREIGN KEY (`categorie_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `products_color_id_foreign` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`),
  ADD CONSTRAINT `products_gold_type_id_foreign` FOREIGN KEY (`gold_type_id`) REFERENCES `gold_types` (`id`),
  ADD CONSTRAINT `products_karat_id_foreign` FOREIGN KEY (`karat_id`) REFERENCES `karats` (`id`),
  ADD CONSTRAINT `products_origin_id_foreign` FOREIGN KEY (`origin_id`) REFERENCES `origins` (`id`),
  ADD CONSTRAINT `products_size_id_foreign` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`id`),
  ADD CONSTRAINT `products_sub_categorie_id_foreign` FOREIGN KEY (`sub_categorie_id`) REFERENCES `sub_categories` (`id`);

--
-- Constraints for table `stockins`
--
ALTER TABLE `stockins`
  ADD CONSTRAINT `stockins_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  ADD CONSTRAINT `stockins_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `stockouts`
--
ALTER TABLE `stockouts`
  ADD CONSTRAINT `stockouts_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  ADD CONSTRAINT `stockouts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
