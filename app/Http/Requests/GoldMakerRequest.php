<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GoldMakerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'=>'required|string',
            'phone'=>'required|min:11|unique:gold_makers,id',
            'goldmakerimage'=>'image|mimes:jpeg,jpg,gif,png',
            //'address'=>'string'
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Sir Please insert Gold Maker name for save.',
            'phone.required'=>'Sir Please insert Gold Maker phone number',
            'phone.min'=>'Phone number minimum 11 digit required',
            'phone.unique'=>'Sir Gold Maker phone number already exists for other Gold Maker, please add another phone number',
            'goldmakerimage.mimes'=>'Sir image must be jpeg,jpg,gif or png',
            //'address.string'=>'Sir Please insert Alphabet word for saving as customers address'
        ];
    }
}
