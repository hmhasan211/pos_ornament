<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'=>'required|string',
            'phone'=>'required|min:11|unique:customers',
            'customer_image'=>'images|mimes:jpg,png'
            //'address'=>'string'
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Sir Please insert customers name for save.',
            'phone.required'=>'Sir Please insert customers phone number',
            'phone.min'=>'Phone number minimum 11 digit required',
            'phone.unique'=>'Sir customers phone number already exists for other customers, please add another phone number',
            //'address.string'=>'Sir Please insert Alphabet word for saving as customers address'
        ];
    }
}
