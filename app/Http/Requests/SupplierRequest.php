<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string',
            'phone'=>'required|min:11|unique:suppliers',
            //'address'=>'string'
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Sir Please insert suppliers name for save.',
            'phone.required'=>'Sir Please insert suppliers phone number',
            'phone.min'=>'Phone number minimum 11 digit required',
            'phone.unique'=>'Sir suppliers phone number already exists for other suppliers, please add another phone number',
            //'address.string'=>'Sir Please insert Alphabet word for saving as suppliers address'
        ];
    }
}
