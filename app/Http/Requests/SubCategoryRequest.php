<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubCategoryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'=>'required|string',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Please insert sub-category name.',
        ];
    }
}
