<?php

namespace App\Http\Controllers\backend;

use App\Model\Employee;
use App\Model\InStock;
use App\Model\mahajan;
use App\Model\Mortgage_cashbook;
use App\Model\Receive_from_mahajan;
use App\Model\SendtoMahajan;
use App\Model\mortgage;
use App\Model\mortgage_invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Product;
use App\Model\ProductColor;
use App\Model\ProductSize;
use App\Model\ProductKarat;
use App\Model\OriginProduct;
use App\Model\Customer;
use App\Model\karat;
use App\Model\Color;
use App\Model\Size;
use App\Model\Weight;
use App\Model\Origin;
use App\Model\Brand;
use App\Model\Category;
use App\Model\GoldType;
use App\Model\Invoice;
use App\Model\Stockin;
use App\Model\CashBook;
use Cart;
use App\Model\Supplier;
use App\Model\SubCategory;
use App\Model\Barcode;
use Illuminate\Validation\Rule;
//use App\MortgageRepository;

use DB;
use Auth;

class MortgageController extends Controller
{

        /*private $repository;

        function  __construct(MortgageRepository $mortgageRepository)
        {
            $this->repository = $mortgageRepository;
        }*/

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Cart::destroy();
        $product_name = Product::where('isActive',3)->pluck('product_name', 'id');
        $product_code = Product::where('isActive',3)->pluck('product_code', 'id');
        $weight = Weight::pluck('name', 'id');
        $customers = Customer::pluck('name','id');
        
        return view('admin.mortgage.add-mortgage', compact('product_code', 'product_name', 'weight', 'customers'));
    }

    public function view_mortgage(Request $request)
    {
        $mortgages = mortgage::orderBy('id','DESC')->get();
        return view('admin.mortgage.view-mortgage',compact('mortgages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function store(Request $request)
    {

        if (!empty($request->customer_id)) {
            $sup_id = $request->customer_id;

        } else {
            $this->validate($request, [
                'name' => 'required',
                'phone' => 'required|unique:suppliers',
                'image' => 'image|mimes:jpg,jpeg,png,gif,svg',
            ]);

            $image = $request->file('image');
            $image_name = substr(md5(time()), 0, 6);
            $filename = Date('Y') . '_' . $image_name . "." . $image->getClientOriginalExtension();
            $image->move(base_path('public/admin/supplier/'), $filename);

            $supplier = new Customer();
            $supplier->name = $request->name;
            $supplier->phone = $request->phone;
            $supplier->image = $filename;
            $supplier->address = $request->Supplier_address;
            $supplier->type = 2;
            $supplier->save();
            $supplier_id = DB::table('customers')->orderBy('id', 'desc')->first();

            $sup_id = $supplier_id->id;
        }

            $invoice = new mortgage_invoice();

            if ($request->payment_type != "bkash" || $request->payment_type != "rocket" || $request->payment_type != "card" || $request->payment_type != "check") {
                $invoice->payment_trxid = 0;
            } else {
                $invoice->payment_trxid = $request->bkash_code;
            }

            $inv_serial = substr(md5(time()), 0, 6);
            $invoice->invoice_no = Date('Y-m-d') . "_" . $inv_serial;
            $invoice->customer_id = $sup_id;
            $invoice->total_amount = $request->total_amount;
            $invoice->total_less = $request->less;
            $invoice->total_paid = $request->total_paid;
            $invoice->total_balance = $request->total_balance;
            $invoice->payment_type = $request->payment_type;
            $invoice->payment_deadline = $request->payment_deadline;
            $invoice->interest = $request->total_interest;
            $invoice->note = $request->sale_note;
            $invoice->save();


            $invoice_id = DB::table('mortgage_invoices')->orderBy('id', 'desc')->first();
            $inv_id = $invoice_id->id;

            /* Last invoice id  catch End */

            $cart_item = Cart::content();

       // dd($cart_item);

            foreach ($cart_item as $cart) {

                    /* Product Information Fetch */
                    $stockin = new mortgage();
                    $stockin->mortgage_invoice_id = $inv_id;
                    $stockin->product_id = $cart->id;
                    $stockin->product_price = $cart->price;
                    $stockin->total_price = $cart->options->total_price;
                    $stockin->vori = $cart->options->vori;
                    $stockin->ana = $cart->options->ana;
                    $stockin->roti = $cart->options->roti;
                    $stockin->miliroti = $cart->options->miliroti;
                    //$stockin->pmt_date = $cart->options->pmt_date;
                    $stockin->save();

                    $stockin_id = mortgage::where('product_id', $cart->id)->orderBy('id', 'desc')->first();

                    /* Barcode Generate Start */
                    $newcode = 0;
                    $y = 0;
                    $code_generate = 0;
                    for ($i = 1; $i <= $cart->qty; $i++) {

                        /* Product Id Exist or Not If Exist then what's the last id of this Product*/

                        $pro_id = Barcode::where('product_id', $cart->id)->orderBy('id', 'desc')->first();
                        if ($pro_id) {
                            /* when product id already exists . Then it will be start new loop here for store barcode inside Barcode_purchases table*/
                            $barcode = new Barcode();
                            $stringConvert = substr($pro_id->barcode, 6, 13);
                            $barcode->barcode = str_pad($cart->id, 6, 0, STR_PAD_LEFT) . str_pad($stringConvert + 1, 6, 0, STR_PAD_LEFT);
                            //$barcode->stockin_id = $stockin_id->id;
                            $barcode->product_id = $cart->id;
                            $barcode->mortgage_id = $stockin_id->id;
                            //$barcode->stockout_id = NULL;
                            $barcode->save();
                        } else {
                            $barcode = new Barcode();
                            $code_generate = str_pad($cart->id, 6, 0, STR_PAD_LEFT) . str_pad($cart->id + $i, 6, 0, STR_PAD_LEFT);
                            $barcode->barcode = $code_generate;

                            //$barcode->stockin_id = $stockin_id->id;
                            $barcode->product_id = $cart->id;
                            $barcode->mortgage_id = $stockin_id->id;
                            //$barcode->stockout_id = NULL;
                            $barcode->save();
                        }
                    }
                    /* Barcode Generate End */
                    /* cash book store start */
            }

                $cashbook = new CashBook;
                $cashbook->mortgage_invoice_id = $inv_id;
                $cashbook->expense = $request->total_paid;
                $cashbook->save();
                /* Cash book store end */

                Cart::destroy();
                session()->flash('success','Mortgage Entry Successful');
                return redirect()->route('mortgage.add');


    }


    public function barcodelist(){
        $barcodes = Barcode::whereNotNull('instock_id')->orderBy('id','DESC')->get();
        return view('admin.mortgage.barcode',compact('barcodes'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */


    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*Add to cart start*/
    public function MortgageAddCart(Request $request)
    {

        $product_info = Product::find($request->id);
        Cart::add([
            'id' => $product_info[0]->id,
            'name' => $product_info[0]->product_name,
            'price' => $request->product_price,
            'qty' => $request->product_qty,
            'options' => [
                'vori' => $request->vori,
                'ana' => $request->ana,
                'roti' => $request->roti,
                'miliroti' => $request->miliroti,
                'gram' => $request->gram,
                'total_price' => $request->total_product_price,
                //'status' => 'mortgage',

            ]
        ]);

        echo $this->Carts();
    }


    public function Carts()
    {
        //Cart::destroy();
        $cart_info = Cart::content();

        $i = 0;
        $total = 0;
        $m = 0;

        if (Cart::count() != 0) {
            foreach ($cart_info as $cart_details) {


                    $total += $cart_details->price;
                    echo '<tr>
                    <td>' . ++$i . '</td>
                    <td>' . $cart_details->name . '</td>
                    <td style="width : 15%;"> 
                        ' . $cart_details->options->gram . ' গ্রাম
                    </td>
                    <td style="width : 20%;">
                        <input type="number" id="cart_price" value="' . $cart_details->price . '" class="form-control" style="display:none" >
                        ' . $cart_details->qty . ' টি
                    </td>
                    <td> 
                        <span id="sub_total' . $cart_details->id . '">' . $cart_details->price . ' টাকা </span>
                    </td>
                    
                    
                    <td> 
                        <button type="button" style="display:none" class="btn btn-primary" data-id="' . $cart_details->rowId . '" id="update_item"><i class="fa fa-edit"></i>  || </button>
                           
                        <button type="button" class="btn btn-danger" data-id="' . $cart_details->rowId . '" id="remove_item"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>';

                    echo '<script>
                           $("#total_amount").val(' . $total . ');
                           $("#total_interest").val(' . $total*0.03 . ');
                           
                    </script>';
                }


        } else {
            echo '<tr><td colspan="7"><p class="bg-danger text-center" style="padding:8px 0"> No item found at Shopping-cart </p>';
        }

    }
    /*Add to cart end*/

    /* Cart Item Remove Start */
    public function CartsRemove(Request $request)
    {
        $rowId = $request->rowId;
        Cart::remove($rowId);
        if (Cart::count() != 0) {
            echo $this->Carts();
        } else {
            echo '<tr><td colspan="6"><p class="bg-danger text-center" style="padding:8px 0"> No item found at Shopping-cart </p>';
        }
    }

    /* Cart Item Remove End*/

    public function MortgageInvoiceDue()
    {
        $invoice = mortgage_invoice::orderBy('id','DESC')->get();
        return view('admin.mortgage.due-invoice-mortgage',compact('invoice'));
    }

    /****************send to mahajan start***************/
    public function send_to_mahajan(Request $request)
    {
        //$repository = $this->repository;

        $send = SendtoMahajan::where('status',null)->get();
        $invoice = mortgage_invoice::pluck('invoice_no','id');
        $mahajan = mahajan::pluck('name','id');
        $employee = Employee::pluck('name','id');

        return view('admin.mortgage.sendtomahajan',compact('send','invoice','mahajan','employee'));
    }

    public function send_to_mahajan_store(Request $request){
        $this->validate($request, [
            //'mahajan_receipt_no' =>'required|unique:sendtomahajans,mahajan_receipt_no,null,null,mahajan_id,\'.$request->mahajan_id,',
            'mahajan_receipt_no' => ['required',Rule::unique('sendto_mahajans')->where(function ($query) use ($request) {
                return $query->where('mahajan_id', $request->mahajan_id);
            })],
            'mahajan_id' => 'required',
            'employee_id' => 'required',
            'amount' => 'required',
            'interest' => 'required',
            'description' => 'required',
            'in_date' => 'required',
            'out_date' => 'required',

        ]);
        $input=$request->all();
        $cash=new CashBook();
        $cash->receipt_no=$request->invoice_id;
        $cash->income=$request->amount;
        $cash->save();
        SendtoMahajan::create($input);
        Session()->flash('success','Sent to mahajan saved successfully');
        return redirect()->route('sendtomahajan.add');
    }

    public function send_to_mahajan_update(Request $request){

        $updaterow=SendtoMahajan::find($request->id);
        $updaterow->out_date=$request->out_date;
        $updaterow->interest=$request->interest;
        $updaterow->update();
        $data['success']=1;
        return json_encode($data);
        //return redirect()->route('sendtomahajan.add');
    }

    public function send_to_mahajan_view($id){
        $info = SendtoMahajan::find($id);
        return view('admin.mortgage.view_sendtomahajan',compact('info'));
    }


    /***********send to mahajan end*****************/


    /*******************Receive from mahajan starts**********************/
    public function receive_from_mahajan(Request $request)
    {
        //$repository = $this->repository;

        $receive = Receive_from_mahajan::all();
        //$invoice = Invoice::whereNotNull('total_interest')->pluck('invoice_no','id');
        $invoice = SendtoMahajan::where('status',null)->get();
        //dd($invoice);
        $mahajan = mahajan::pluck('name','id');
        $employee = Employee::pluck('name','id');

        return view('admin.mortgage.receive_from_mahajan',compact('receive','invoice','mahajan','employee'));
    }

    public function get_mortgage_info(Request $request)
    {
        $receipt_no=$request->id;
        $data=SendtoMahajan::where('mahajan_receipt_no',$receipt_no)->first();
        //$mh_name=$data->mahajan->name;
        //dd($mh_name);
        $data['mahajan']=$data->mahajan->name;
        return json_encode($data);
        //return $mh_name;
        //return view('admin.mortgage.receive_from_mahajan',compact('send','invoice','mahajan','employee'));
    }

    public function receive_from_mahajan_store(Request $request){
        $cash=new CashBook();
        $cash->receipt_no=$request->invoice_id;
        $cash->expense=$request->amount;

        $receive=new Receive_from_mahajan();
        $receive->mahajan_receipt_no=$request->mahajan_receipt_no;
        $receive->employee_id=$request->employee_id;
        $receive->mahajan_id=$request->mahajan_id;
        $receive->out_date=$request->out_date;
        $receive->paid_amount=$request->paid_amount;
        $receive->description=$request->description;
        $sent=SendtoMahajan::where('mahajan_receipt_no',$request->mahajan_receipt_no)->where('mahajan_id',$request->mahajan_id)->first();
        $sent->status=1;
        $sent->update();

        $receive->save();
        $cash->save();
        Session()->flash('success','Receive From Mahajan saved successfully');
        return redirect()->route('receivemahajan.add');
    }
    /******************Receive from mahajan ends*****************/

    /**********pay interest to mahajan starts*********/
    public function pay_int_to_mahajan(Request $request)
    {
        //$repository = $this->repository;

        $send = Mortgage_cashbook::where('invoice_id',null)->get();
        $invoice = SendtoMahajan::where('status',null)->get();
        $mahajan = mahajan::pluck('name','id');
        $employee = Employee::pluck('name','id');

        return view('admin.mortgage.pay_interest_to_mahajan',compact('send','invoice','mahajan','employee'));
    }

    public function pay_int_to_mahajan_store(Request $request)
    {
        $this->validate($request, [
            'mahajan_receipt_no' => 'required',
            'mahajan_id' => 'required',
            'employee_id' => 'required',
            'interest' => 'required',
            'description' => 'required',
            'pmt_date' => 'required',
        ]);
        $input=new Mortgage_cashbook();
        $input->receipt_no=$request->mahajan_receipt_no;
        $input->mahajan_id=$request->mahajan_id;
        $input->employee_id=$request->employee_id;
        $input->expense=$request->interest;
        $input->description=$request->description;
        $input->pmt_date=$request->pmt_date;
        $input->save();
        return redirect()->route('paytomahajan.add');
    }
    /**********pay interest to mahajan ends*********/

    /**********receive interest from customer starts*********/
    public function rcv_int_customer(Request $request)
    {
        //$repository = $this->repository;

        $send = Mortgage_cashbook::whereNotNull('invoice_id')->get();
        $invoice = mortgage_invoice::all();
        //$customer = Customer::pluck('name','id');
        $employee = Employee::pluck('name','id');

        return view('admin.mortgage.receive_interest_from_customer',compact('send','invoice','employee'));
    }

    public function get_mortgage_invoice(Request $request)
    {
        $receipt_no=$request->id;
        $data=mortgage_invoice::find($receipt_no);

        //$mh_name=$data->mahajan->name;
        //dd($mh_name);
        $data['customer']=$data->customer->name;
        return json_encode($data);
        //return $mh_name;
        //return view('admin.mortgage.receive_from_mahajan',compact('send','invoice','mahajan','employee'));
    }

    public function rcv_int_cust_store(Request $request)
    {
        $this->validate($request, [
            'invoice_no' => 'required',
            'employee_id' => 'required',
            'interest' => 'required',
            'description' => 'required',
            'pmt_date' => 'required',
        ]);
        $input=new Mortgage_cashbook();
        $input->invoice_id=$request->invoice_no;
        $input->customer_id=$request->customer_id;
        $input->employee_id=$request->employee_id;
        $input->income=$request->interest;
        $input->description=$request->description;
        $input->pmt_date=$request->pmt_date;
        $input->save();
        return redirect()->route('receive_int_customer.add');
    }
    /**********receive interest from customer ends*********/
}
