<?php

namespace App\Http\Controllers\backend;

use App\Model\mahajan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MahajanController extends Controller
{
    public function index()
    {
        $suppliers = mahajan::orderBy('id','DESC')->get();
        return view('admin.mahajan.add-mahajan',compact('suppliers'));
    }


    public function store(Request $request)
    {
        $supplier = new mahajan();
        $supplier->name = $request->name;
        $supplier->phone = $request->phone;
        $supplier->address =$request->address;
        $supplier->save();
        session()->flash('success','Mahajan has been added successfully');
        return redirect()->route('mahajan.add');
    }


    public function update(Request $request)
    {
        $supplier = mahajan::find($request->id);
        $supplier->name = $request->name;
        $supplier->phone = $request->phone;
        $supplier->address = $request->address;
        $supplier->update();
        session()->flash('success','Mahajan has been updated successfully');
        return redirect()->route('mahajan.add');
    }


    public function destroy(Request $request)
    {
        $delete = mahajan::find($request->id);
        $delete->delete();
    }


    /* suppliers search */
    public function suppliersearch(Request $request)
    {
        $supplier = mahajan::find($request->supplier_id);
        echo json_encode($supplier);
        //  echo '{"id":"1"}';
    }

    /* suppliers end */

    public function SearchSubcategory(Request $request)
    {
        $category_id = Category::find($request->category_id);

        foreach($category_id->sub_categories as $subcategory){
            echo '<option value="'.$subcategory->id.'">'.$subcategory->name.'</option>';
        }

    }
}
