<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Size;
use DB;

class SizeController extends Controller
{

    public function index()
    {
        $size= Size::all();
        return view('admin.size.add-size',compact('size'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:sizes',
        ]);

        $size = new Size();
        $size->name = $request->name;
        $size->save();
        session()->flash('success','size has store Successfully');
        return redirect()->route('size.add');
    }

    public function show($id)
    {
        $size = Size::find($id);
        return view('admin.size.edit-size',compact('size'));
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:sizes',
        ]);

        $size = Size::find($request->id);
        $size->name = $request->name;
        $size->update();
        session()->flash('success','size has Update Successfully');
        return redirect()->route('size.add');
    }

    public function destroy(Request $request)
    {
        $delete = Size::find($request->id);
        $delete->delete();
    }
}

