<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Karat;
use DB;

class KaratController extends Controller
{
    
    public function index()
    {
        $karat= karat::all();
        return view('admin.karat.add-karat',compact('karat'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'karat_size'=>'required|unique:karats',
        ]);

        $karat = new karat();
        $karat->karat_size = $request->karat_size;
        $karat->save();
        session()->flash('success','Karat has store Successfully');
        return redirect()->route('karat.add');
    }

    public function show($id)
    {
       $karat = karat::find($id);
       return view('admin.karat.edit-karat',compact('karat'));
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'karat_size'=>'required|unique:karats',
        ]);

        $karat =karat::find($request->id);
        $karat->karat_size = $request->karat_size;
        $karat->update();
        session()->flash('success','karat has Update Successfully');
        return redirect()->route('karat.add');
    }

    public function destroy(Request $request)
    {
        $delete = karat::find($request->id);
        $delete->delete();
    }
}
