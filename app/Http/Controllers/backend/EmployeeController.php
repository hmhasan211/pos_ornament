<?php

namespace App\Http\Controllers\backend;

use App\Model\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        return view('admin.employee.add-employee',compact('employees'));
    }

    public function store(Request $request)
    {
       // dd($request->all());
        $data=[];
        $image = $request->file('image');
        $filename = Date('Y')."_".substr(md5(time()),0,6).".".$image->getClientOriginalExtension();
        $data = $request->except('image');
        $data['image'] = $filename;
        $image->move(base_path('public/admin/product/upload/'),$filename);
        Employee::create($data);
        session()->flash('success','Employee has store Successfully complete');
        return redirect()->route('employee.add');
    }

    public function show($id)
    {
        $employee = Employee::find($id);
        return view('admin.employee.edit-employee',compact('employee'));
    }

    public function edit($id)
    {

    }

    public function update(Request $request)
    {
        $employee = Employee::find($request->id);
        if($request->has('image')){
            $path = "public/admin/product/upload/".$employee->image;
            unlink($path);
            $image = $request->file('image');
            $filename = Date('Y')."_".substr(md5(time()),0,6).".".$image->getClientOriginalExtension();
            $data = $request->except('image');
            $data['image'] = $filename;
            $image->move(base_path('public/admin/product/upload/'),$filename);
            $employee->update($data);
        }else{
            $employee->update($request->all());
        }

        session()->flash('success','Employee has update Successfully complete');
        return redirect()->route('employee.add');
    }

    public function destroy(Request $request)
    {
        $employee = Employee::find($request->id);
        $path = "public/admin/product/upload/".$employee->image;
        unlink($path);
        $employee->delete();
    }
}
