<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CostType;
class CostTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = CostType::orderBy('id','DESC')->get();
        return view('admin.localcost.cost-type',compact('types'));
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|unique:cost_types'
        ]);
        CostType::create($request->all());
        session()->flash('success','Cost Type Add successfully');
        return redirect()->route('localcost.typestore');
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $type = CostType::find($id);
        return view('admin.localcost.edit-costtype',compact('type'));
    }


    public function update(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|unique:cost_types'
        ]);
        $update = CostType::find($request->id);
        $update->update($request->all());
        session()->flash('success','Cost Type update successfully');
        return redirect()->route('localcost.typestore');
    }

    function destroy($id)
    {
        //
    }
}
