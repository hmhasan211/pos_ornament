<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Supplier;
use App\Model\Category;
use App\Http\Requests\SupplierRequest;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::orderBy('id','DESC')->get();
        return view('admin.supplier.add-supplier',compact('suppliers'));
    }


    public function store(SupplierRequest $request)
    {
        $supplier = new Supplier();
        $supplier->name = $request->name;
        $supplier->phone = $request->phone;
        $supplier->address =$request->address;
        $supplier->save();
        session()->flash('success','Supplier has store successful');
        return redirect()->route('supplier.add');
    }


    public function update(SupplierRequest $request)
    {
        $supplier = Supplier::find($request->id);
        $supplier->name = $request->name;
        $supplier->phone = $request->phone;
        $supplier->address = $request->address;
        $supplier->update();
        session()->flash('success','Supplier has update successful');
        return redirect()->route('supplier.add');
    }


    public function destroy(Request $request)
    {
        $delete = Supplier::find($request->id);
        $delete->delete();
    }


    /* suppliers search */
    public function suppliersearch(Request $request)
    {
        $supplier = Supplier::find($request->supplier_id);
        echo json_encode($supplier);
       //  echo '{"id":"1"}';
    }

    /* suppliers end */

    public function SearchSubcategory(Request $request)
    {
        $category_id = Category::find($request->category_id);

        foreach($category_id->sub_categories as $subcategory){
            echo '<option value="'.$subcategory->id.'">'.$subcategory->name.'</option>';
        }

    }


}
