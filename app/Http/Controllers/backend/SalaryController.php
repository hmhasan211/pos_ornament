<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\EmployeeSalary;

class SalaryController extends Controller
{
    public function index(){
        $employees = Employee::pluck('name','id');
        $salaries = EmployeeSalary::all();
        return view('admin.salary.add-salary',compact('employees','salaries'));
    }

    public function store(Request $request){
        // dd($request->all());
        EmployeeSalary::create($request->all());
        session()->flash('success','Employee salary has store successfully');
        return redirect()->route('salary.add');
    }

    public function history(Request $request){
        $employees = Employee::all();
        $employe = Employee::pluck('name','id');
        return view('admin.salary.history-salary',compact('employees','employe'));
    }

    public function search(Request $request){
        $employee_id = $request->employee_id;
        $start = $request->start;
        $end = $request->end;
        $salary = EmployeeSalary::whereBetween('paid_date',[$start,$end])->get();
        $i=0;
        $total=0;
        if($salary->Count()>0){
            foreach ($salary as $info){
                $total=$total+$info->salary;
                echo '<tr>
                    <td>'.++$i.'</td>
                    <td>'.$info->employee->name.'</td>
                    <td>'.$info->employee->salary.'</td>
                    <td>'.$info->salary.'</td>
                    <td>Paid</td>
                    <td>'.$info->paid_date.'</td>
                  </tr>';
            }
        }else{
            echo '<tr> <td colspan="6" rowspan="6" class="bg-danger text-center">No Record Found</td> </tr>';
        }

    }
}
