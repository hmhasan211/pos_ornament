<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Brand;
use App\Model\Category;
use App\Model\Size;
use App\Model\Product;
use App\Model\Color;
use App\Model\Karat;
use App\Model\Origin;
use App\Model\GoldType;
use App\Model\Supplier;
use App\Model\SubCategory;

class ProductController extends Controller
{

    public function index()
    {
        $brand = Brand::pluck('name', 'id');
        $category = Category::pluck('name', 'id');
        $size = Size::pluck('name', 'id');
        $color = Color::pluck('name', 'id');
        $karat = Karat::pluck('karat_size', 'id');
        $origin = Origin::pluck('name', 'id');
        $gold_type = GoldType::pluck('name', 'id');
        $suppliers = Supplier::pluck('name','id');
        $category = Category::pluck('name','id');
        return view('admin.product.add-product',compact('brand','category','size','color','karat','origin','gold_type','category'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'product_code'=>'required|string|Max:4',
            'product_name'=>'required|string',
            'product_price'=>'required|string',
            'product_local_name'=>'required|string',
            'size_id'=>'required',
            'color_id'=>'required',
            'karat_id'=>'required',
            'origin_id'=>'required',
            'product_image'=>'image|mimes:jpeg,png,jpg,gif,svg',
            'product_image'=>'required',
            'product_details'=>'required|string',
            'categorie_id'=>'required',
            'sub_categorie_id'=>'required',
            'isActive'=>'required'
        ]);


        $product = new Product();

        if( $request->hasFile('product_image'))
        { 
            $image = $request->file('product_image');
            $image_name = substr(md5(time()),0,6);
            $filename = Date('Y').'_'.$image_name.".".$image->getClientOriginalExtension();
            $product->product_image = $filename;
            $image->move(base_path('public/admin/product/upload/'),$filename);
            $data = $request->except('product_image');
            $data['product_image'] = $filename;
            $product = Product::query()->create($data);
        }else{
            $product = Product::query()->create($request->except('product_image'));
        }


        session()->flash('success','Product has Store Successfull');

        return redirect()->route('product.add');
    }

    /* Product Edit Start */

    public function show($id)
    {
        $brand = Brand::pluck('name', 'id');
        $category = Category::pluck('name', 'id');
        $size = Size::pluck('name', 'id');
        $color = Color::pluck('name', 'id');
        $karat = Karat::pluck('karat_size', 'id');
        $origin = Origin::pluck('name', 'id');
        $gold_type = GoldType::pluck('name', 'id');
        $product_edit = Product::find($id);
        return view('admin.product.edit-product',compact('product_edit','brand','category','size','color','karat','origin','gold_type'));
    }

    /* Product Edit End */


    public function viewproduct()
    {
        $product = Product::all();
        return view('admin.product.view-product',compact('product'));
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'product_code'=>'required|string',
            'product_name'=>'required|string',
            'product_price'=>'required|string',
            'product_local_name'=>'required|string',
            'product_image'=>'image|mimes:jpeg,png,jpg,gif,svg',
            'product_details'=>'required|string',
            'size_id'=>'required',
            'color_id'=>'required',
            'karat_id'=>'required',
            'origin_id'=>'required'
        ]);

        $product = Product::find($request->id);

        /* image remove */

        if( $request->hasFile('product_image'))
        {
            /* old image unlink start */

            $img_path = DB::table('products')->where('id',$request->id)->first();
            $path = 'public/admin/product/upload/'.$img_path->product_image;
            unlink($path);

            /* old image unlink end */

            $image = $request->file('product_image');
            $image_name = substr(md5(time()),0,6);
            $filename = Date('Y').'_'.$image_name.".".$image->getClientOriginalExtension();
            $product->product_image = $filename;
            $image->move(base_path('public/admin/product/upload/'),$filename);
            $data = $request->except('product_image');
            $data['product_image'] = $filename;
            $product->update($data);
         }else{
            $product->update($request->except('product_image'));
        }


        session()->flash('success','Product has update Successfull');
        return redirect()->route('product.view');
    }


    public function destroy(Request $request)
    {
        /* Image Unlink Start */
        $img_path = DB::table('products')->where('id',$request->id)->first();
        $path = 'public/admin/product/upload/'.$img_path->product_image;
     
        unlink($path);
        /* Image Unlink Start */

        $delete = Product::find($request->id);
        $delete->delete();
    }

    public function status_update(Request $request)
    {
        $id = $request->id;
        $status = $request->isactive;
        $update = Product::find($id);
        $update->isActive = $status;
        $update->update();
        session()->flash('success','Product Status update successful');
        return redirect()->route('product.view');
    }


    public function SearchSubcategory(Request $request)
    {
        $category_id = Category::find($request->category_id);

        foreach($category_id->sub_categories as $subcategory){
            echo '<option value="'.$subcategory->id.'">'.$subcategory->name.'</option>';
        }

    }



}
