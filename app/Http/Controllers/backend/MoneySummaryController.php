<?php

namespace App\Http\Controllers\backend;

use App\Model\CostType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MoneySummary;

class MoneySummaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = CostType::pluck('name','id');
        $received = MoneySummary::where('status',1)->get();
        return view('admin.money.receive-money',compact('types','received'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'phone'=>'required|min:11',
            'amount'=>'required',
            'cost_type_id'=>'required',
            'note'=>'string|max:3500',
        ]);

        $store = $request->all();
        $store['status']=1;

        MoneySummary::create($store);
        session()->flash('success','Receive money successfully stored');
        return redirect()->route('moneysummary.receiveadd');
    }


    public function CostView()
    {
        $types = CostType::pluck('name','id');
        $received = MoneySummary::where('status',2)->get();
        return view('admin.money.cost-money',compact('types','received'));
    }


    public function storeExpense(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'phone'=>'required|min:11',
            'amount'=>'required',
            'cost_type_id'=>'required',
            'note'=>'string|max:3500',
        ]);
        $store = $request->all();
        $store['status']=2;
        //dd($store);
        MoneySummary::create($store);
        session()->flash('success','Expense money successfully stored');
        return redirect()->route('moneysummary.costmoney');
    }

    public function IncomeReport(){
        return view('admin.money.income-report');
    }

    public function ExpenseReport(){
        return view('admin.money.expense-report');
    }

    public function SearchIncome(Request $request){
        $status = $request->status;
        $start = $request->start;
        $end = $request->end;
       // dd($request->all());
        $salary = MoneySummary::whereBetween('created_at',[$start,$end])->where('status',$status)->get();
       // $salary = MoneySummary::where('status',$status)->get();
       // dd($salary);

        $i=0;
        $total=0;
        if($salary->Count()>0){
            foreach ($salary as $info){
                $total=$total+$info->salary;
                echo '<tr>
                    <td>'.++$i.'</td>
                    <td>'.$info->name.'</td>
                    <td>'.$info->phone.'</td>
                    <td>'.$info->cost->name.'</td>
                    <td>'.$info->amount.'</td>
                    <td>'.$info->created_at.'</td>
                  </tr>';
            }
        }else{
            echo '<tr> <td colspan="6" rowspan="6" class="bg-danger text-center">No Record Found</td> </tr>';
        }

    }
}
