<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\GoldType;
use DB;

class GoldTypeController extends Controller
{
    
    public function index()
    {
        $goldtype= goldtype::all();
        return view('admin.goldtype.add-goldtype',compact('goldtype'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:gold_types',
        ]);

        $goldtype = new GoldType();
        $goldtype->name = $request->name;
        $goldtype->save();
        session()->flash('success','goldtype has store Successfully');
        return redirect()->route('goldtype.add');
    }

    public function show($id)
    {
       $goldtype = GoldType::find($id);
       return view('admin.goldtype.edit-goldtype',compact('goldtype'));
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:gold_types',
        ]);

        $goldtype = GoldType::find($request->id);
        $goldtype->name = $request->name;
        $goldtype->update();
        session()->flash('success','goldtype has Update Successfully');
        return redirect()->route('goldtype.add');
    }

    public function destroy(Request $request)
    {
        $delete = GoldType::find($request->id);
        $delete->delete();
    }









}

