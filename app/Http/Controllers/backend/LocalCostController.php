<?php

namespace App\Http\Controllers\backend;

use App\Model\CostType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\LocalCost;

class LocalCostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $costs = LocalCost::all();
        $type = CostType::pluck('name','id');
        return view('admin.localcost.add-cost',compact('costs','type'));
    }

   
    public function store(Request $request)
    {
        $this->validate($request,[
            'cost_type_id'=>'required',
            'amount'=>'required',
            'costdate'=>'required',
        ],['costdate.required'=>'You must select Cost date','cost_type_id.required'=>'You must select Cost Name']);

        LocalCost::create($request->all());
       session()->flash('success','Local Cost Saved');
       return redirect()->route('localcost.add');

    }


    public function destroy(Request $request)
    {
        $delete = LocalCost::find($request->id);
        $delete->delete();
    }

    public function history(Request $request){
        $costtypes = CostType::pluck('name','id');
        return view('admin.localcost.localcost-history',compact('costtypes'));
    }

    public function search(Request $request){
        $start = $request->start;
        $end = $request->end;
        $costtype=$request->cost_type_id;
        if (!empty($costtype)) {
            $costs = LocalCost::whereBetween('costdate', [$start, $end])->where('cost_type_id',$costtype)->orderBY('id','DESC')->get();
        }else{
           $costs = LocalCost::whereBetween('costdate', [$start, $end])->orderBY('id','DESC')->get();
        }

        $i=0;
        $total=0;
        if($costs->Count()>0){
            foreach ($costs as $cost){

                $total+=$cost->amount;
                echo '<tr>
                    <td>'.++$i.'</td>
                    <td>'.$cost->cost->name.'</td>
                    <td>'.$cost->amount.'</td>
                    <td>'.$cost->created_at.'</td>
                  </tr>';
            }
            echo '<tr><td></td></td><td><strong>Total Amount:</strong></td><td>' .number_format($total).'</td><td></td></tr>';
        }else{
            echo '<tr> <td colspan="6" rowspan="6" class="bg-danger text-center">No Record Found</td> </tr>';
        }

    }


}
