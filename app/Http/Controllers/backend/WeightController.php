<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Weight;

class WeightController extends Controller
{
    public function index()
    {
        $weight= Weight::all();
        return view('admin.weight.add-weight',compact('weight'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:categories',
        ]);

        $weight = new Weight();
        $weight->name = $request->name;
        $weight->save();
        session()->flash('success','weight has store Successfully');
        return redirect()->route('weight.add');
    }

    public function show($id)
    {
        $weight = Weight::find($id);
        return view('admin.weight.edit-weight',compact('weight'));
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:weights',
        ]);

        $weight = Weight::find($request->id);
        $weight->name = $request->name;
        $weight->update();
        session()->flash('success','weight has Update Successfully');
        return redirect()->route('weight.add');
    }

    public function destroy(Request $request)
    {
        $delete = Weight::find($request->id);
        $delete->delete();
    }
}
