<?php

namespace App\Http\Controllers\backend;

use App\Model\Stockout;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Product;
use App\Model\Karat;
use App\Model\Color;
use App\Model\Size;
use App\Model\Weight;
use App\Model\Origin;
use App\Model\Brand;
use App\Model\Category;
use App\Model\GoldType;
use App\Model\Invoice;
use App\Model\Stockin;
use App\Model\CashBook;
use App\Model\Sale;
use Cart;
use App\Model\Customer;
use App\Model\Barcode;
use DB;
use Auth;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Cart::destroy();
        $pro = Product::orderBy('id','desc')->get();
        $product = Product::groupBy('product_code')->get();
        $barcode = Barcode::where('stockout_id',null)->pluck('barcode','barcode');
        $karat = Karat::pluck('karat_size','id');
        $size = Size::pluck('name','id');
        $origin = Origin::pluck('name','id');
        $weight = Weight::pluck('name','id');
        $brand = Brand::pluck('name','id');
        $category = Category::pluck('name','id');
        $Customer = Customer::pluck('name','id');

        return view('admin.sale.add-sale',compact('product','barcode','karat','size','origin','weight','brand','category','Customer','pro'));
    }
    public function getWeightByBarcode(Request $request){
        $barcode = $request->barcode;
        $price = $request->price;
        $pid = Barcode::where('barcode','=',$barcode)->first();
        if($pid) {
            $product_info = Product::Where('id', $pid->product_id)->first();
            $pro_data = Stockin::where('id', $pid->stockin_id)->where('product_id', $pid->product_id)->first();
            $vori=$pro_data->vori*(16*6*10);
            $ana=$pro_data->ana*(6*10);
            $roti=$pro_data->roti*10;
            $miliroti=$pro_data->miliroti;
            $subtotal=($vori+$ana+$roti+$miliroti)*($price/960);
            return $subtotal;
        }
    }

    public function BarcodeSearch(Request $request)
    {
        $barcode = $request->barcode;
        $pid = Barcode::where('barcode','=',$barcode)->first();
        if($pid){
            $product_info = Product::Where('id',$pid->product_id)->first();
            $pro_data = Stockin::where('id',$pid->stockin_id)->where('product_id',$pid->product_id)->first();
            /* if cart has data */
            /* check already sold or not start */
            if($pid->stockout_id == NULL ){
                /* check already sold or not End */
                if($pid->gold_maker_id != NULL){
                    return "goldmaker";
                }
                else{
                    if(Cart::count()>0){
                        /* barcode match finding foreach loop start */
                        foreach (Cart::content() as $cart){
                            if($barcode == $cart->options->barcode){
                                return $barcode;
                            }
                        }
                        /* barcode match finding foreach loop end */
                        /* add to sale cart start */
                        $this->addsaleitem($product_info,$pro_data,$barcode);
                        echo $this->SaleCart();
                        /* add to sale cart End */
                    }else{
                        $this->addsaleitem($product_info,$pro_data,$barcode);
                        echo $this->SaleCart();
                        // cart blank and add new data into cart
                    }
                }
            }else{
                return "__".$barcode;
            }/* already sold else condition end here */
        }else{
            //echo $this->SaleCart();
            return "__".$barcode;
        }
    }/* method ending */

    /* add to sale cart item start */

    public function addsaleitem($product_info,$pro_data,$barcode)
    {
       
        Cart::add([
            'id'=> $product_info->id,
            'name'=> $product_info->product_name,
            'price'=>$product_info->product_price,
            'qty'=>1,
            'options'=>[
                'barcode'=>$barcode,
                'vori'=>$pro_data->vori,
                'ana'=>$pro_data->ana,
                'roti'=>$pro_data->roti,
                'miliroti'=>$pro_data->miliroti,
                'total'=>$pro_data->total_price,
                'status'=>'sale',
            ]
        ]);

    }



    /* add to sale cart item End */

    /* Sale Cart Publish */

    public function BarcodeSerialSearch(Request $request)
    {

        $start = $request->start;
        $end = $request->end;

        /* TOTAL NUMBER OF ITEM WHICH WILL BE ADDED INTO CART . WE SHOULD MAKE A LOOP FOR IT .*/

        $error = 0;
        for($start;$start<=$end;$start++){

            if(Cart::count()>0){
                $barcode = str_pad($start,12,0,STR_PAD_LEFT);
                $pid = Barcode::where('barcode','=',$barcode)->first();
                if($pid){
                    /* product sold or not checking start */
                    if($pid->stockout_id == NULL){
                        /* already added or not checking start */
                        if($pid->gold_maker_id !=NULL){
                            return "goldmaker";
                        }else{
                            $cart_lists = Cart::content();
                            foreach ($cart_lists as $cart){
                                if($start == $cart->options->barcode){
                                    return $start;
                                }
                            }
                            /* already added or not checking end */

                            $product_info = Product::Where('id',$pid->product_id)->first();
                            $pro_data = Stockin::where('id',$pid->stockin_id)->where('product_id',$pid->product_id)->first();
                            $this->addsaleitem($product_info,$pro_data,$barcode);
                        }
                    }else{
                        echo '<script> alert("Sorry this product already sold");</script>';
                    }
                }else{
                    // echo $this->SaleCart();

                    return "__".$barcode;
                }
            }else{
                $barcode = str_pad($start,12,0,STR_PAD_LEFT);
                $pid = Barcode::where('barcode','=',$barcode)->first();
                if($pid){
                    if($pid->stockout_id == NULL){
                        $product_info = Product::Where('id',$pid->product_id)->first();
                        $pro_data = Stockin::where('id',$pid->stockin_id)->where('product_id',$pid->product_id)->first();
                        $this->addsaleitem($product_info,$pro_data,$barcode);
                    }else{
                        echo '<script> alert("Sorry this product already sold");</script>';
                    }

                }else{
                    //echo $this->SaleCart();
                    return "__".$barcode;
                }
            }
            /* already added or not checking end */
            /* Add to Shopping Cart */

        }/* For Loop Ending here*/
        /* cart item show start */

        echo $this->SaleCart();

        /* cart item show end */
    }

    public function SaleCart(){

        $cartitem = Cart::content();
      //dd($cartitem);
        $i = 0 ;
        $j=0;
        $k=0;
        if(Cart::count() > 0){
            $total = 0;
            foreach ($cartitem as $cartinfo) {

                    $total += $cartinfo->options->total;

                    $pro = Barcode::where('barcode', '=', $cartinfo->options->barcode)->first();
                    $product_info = Product::Where('id', $cartinfo->id)->first();

                    foreach ($product_info->categorie->sub_categories as $sub) {
                    }
                    if ($cartinfo->qty > 1) {
                        $qty = '<label class="label label-danger">Remove this ' . $cartinfo->qty . '</label>';
                    } else {
                        $qty = $cartinfo->qty;
                    }

                    echo '<tr id="rowid' . $cartinfo->options->barcode . '">
                            <td>' . ++$i . '</td>
                            <td id="barcodeCol' . $cartinfo->rowId . '" data-barcode=' . $cartinfo->options->barcode . '>' . $cartinfo->options->barcode . '</td>
                            <td>' . $product_info->product_code . '</td>
                            <td style="width: 300px;">' . $product_info->product_name . '</td>
                            <td>' . $product_info->brand->name . '</td>
                            <td>' . $product_info->origin->name . '</td>
                            <td>' . $product_info->karat->karat_size . '</td>
                            <td>' . $product_info->gold_type->name . '</td>
                            <td style="width: 150px;"> <input type="number" id="cart_price_update" value="' . $cartinfo->price . '" class="form-control" data-id="' . $cartinfo->rowId . '">  </td>
                            <td>মোট = <p style="display:inline-block" id="mottaka' . $cartinfo->rowId . '">' . $cartinfo->options->total . '</p> টাকা</td>
                            <td>
                                <button type="button" data-id="' . $cartinfo->rowId . '" id="updateList" class="fa fa-pencil btn btn-info">
                                    </button>
                                    <button type="button" data-id="' . $cartinfo->rowId . '" id="removelist' . ++$k . '" onclick="itemremove(' . ++$j . ')" class="fa fa-trash btn btn-danger">
                                    </button>
                            </td>
                          </tr>';

            }
            echo '<tr><td colspan="16"><p class="bg-primary text-center" style="padding:8px 0">Total Cart Item : '.Cart::count().' </p>
            <script>
                $("#total_price").val("'.$total.'");
                $("#total_balance").val("'.$total.'");
                $("#afterdiscount").val("'.$total.'");

                $("#total_balance").css({"background":"red","color":"#fff","font-weight":"bold","letter-spacing":"2px"});
            </script>';
        }
        else{
            echo '<tr><td colspan="16"><p class="bg-danger text-center" style="padding:8px 0"> No item found at Shopping-cart </p></tr>';
        }

    }

    public function salecartupdate(Request $request){

        $rowId = $request->rowId;
        $price = $request->price;
        $subtotal = $request->total;

        /* Find out by rowId */
        $dummytotal=0;
        foreach (Cart::content() as $items){
            if($items->rowId == $rowId){
                $d["id"]=$items->id;
                $pro =  Product::find($items->id);
                $d["name"]=$pro->product_name;

                if($items->options->vori!=null or $items->options->vori !=0){
                    $vori = $items->options->vori*16*6*10;
                }else{
                    $vori = 0;
                }

                if($items->options->ana != null or $items->options->ana!=0){
                    $ana = ($items->options->ana*6)*10;
                }else{
                    $ana = 0;
                }

                if($items->options->roti !=null or $items->options->roti !=0){
                    $roti = $items->options->roti*10;
                }else{
                    $roti =0;
                }

                if($items->options->miliroti!=null or $items->options->miliroti != 0){
                    $miliroti = $items->options->miliroti;
                }else{
                    $miliroti = 0;
                }

                $permilirotiprice = $items->price/960;
                $totalrotti = $vori+$ana+$roti+$miliroti;
                $dummytotal=$permilirotiprice*$totalrotti;

                Cart::update($rowId, [
                    'id' => $items->id,
                    'price' => $price,
                    'name'=>$pro->name,
                    'qty' => 1,
                    'options'=>[
                        'barcode'=>$items->options->barcode,
                        'total'=>$subtotal,
                        'vori'=>$vori,
                        "ana" => $ana,
                        "roti" => $roti,
                        "miliroti" => $miliroti,
                    ]
                ]);
                session()->flash('success','Product price has been updated');
            }
        }

        $d["qty"]=1;

        $d["vori"]=$vori;
        $d["ana"]=$ana;
        $d["roti"]=$roti;
        $d["miliroti"]=$miliroti;
        $d["price"]=$price;
        $d["amount"]=$totalrotti;
        $d["total"]=$dummytotal;

        //dd($d);

       // echo $this->SaleCart();

        /*$cartitem = Cart::content();

        $data = [];
        $total = 0;
        if(Cart::count() > 0) {

            foreach (Cart::content() as $cartinfo) {
                $vori = $cartinfo->options->vori;
                $ana = $cartinfo->options->ana;
                $roti = $cartinfo->options->roti;
                $miliroti = $cartinfo->options->miliroti;
                $permilirotiprice = $cartinfo->price/960;
                $totalrotti = ($vori*16)+($ana*6)+($roti*10)+$miliroti;
                $data["totalroti"]=$totalrotti;
                $total= $total+($permilirotiprice*$totalrotti);
            }
        }
        $data["total"] = $total;
        return response()->json($data);*/
    }




    public function SaleCartRemove(Request $request){
        $rowId = $request->rowId;
        Cart::remove($rowId);
        if(Cart::count() > 0){
            echo $this->SaleCart();
        }
        else{
            echo '<tr><td colspan="16"><p class="bg-danger text-center" style="padding:8px 0"> No item found at Shopping-cart </p>';
        }
    }
    


    public function store(Request $request)
    {

        $cus_id = '';
      // dd($request->all());

        /* customer references found or not check */
        if( !empty($request->customer_id)){
            $cus_id = $request->customer_id;
        }else{
             $this->validate($request,[
                'customer_name'=>'required',
                'phone'=>'required|unique:customers',
                'customerimage'=>'required|image|mimes:jpg,jpeg,png,gif,svg',
            ]);

           // dd($request->all());

           /* store customer in customer table */
            $customer = new Customer();
            $customer->name = $request->customer_name;
            $customer->phone = $request->phone;
            $customer->address = $request->customer_address;

            $image = $request->file('customerimage');

            $image_name = substr(md5(time()),0,6);
            $filename = Date('Y').'_'.$image_name.".".$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/customer/'),$filename);


            $customer->save();
            $customer_id = DB::table('customers')->orderBy('id','desc')->first();
            $cus_id= $customer_id->id;
        }

        /* Invoice Create start */

        $invoice = new Invoice();

        if($request->payment_type =="bkash"){
            $invoice->payment_trxid = "bkash";
        }
        elseif($request->payment_type =="rocket"){
            $invoice->payment_trxid = "rocket";
        }
        else{
            $invoice->payment_trxid = $request->bkash_code;
        }

        $inv_serial = substr(md5(time()),0,6);
        $invoice->invoice_no = Date('Y-m-d')."_".$inv_serial;
        $invoice->customer_id = $cus_id;
        $invoice->total_amount = $request->total_amount;
        $invoice->total_paid = $request->total_paid;
        $invoice->total_balance = $request->total_balance;
        $invoice->total_discount = $request->total_discount;
        $invoice->total_less = $request->total_less;

        $invoice->total_makingcharge = $request->making_charge;
        $invoice->payment_deadline = $request->payment_deadline;
        $invoice->payment_type = $request->payment_type;
        $invoice->note = $request->sale_note;
        $invoice->save();

        /* Last invoice id  catch start */

        $invoice_id = DB::table('invoices')->orderBy('id','desc')->first();
        $inv_id = $invoice_id->id;

        /* Last invoice id  catch End */

        $cart_item = Cart::content();
    
        foreach($cart_item as $cart) {
            if ($cart->options->status == 'sale') {
                /* Product Information Fetch */
                //dd($cart->options->weight_amount);

                $sale = new Stockout();
                $sale->invoice_id = $inv_id;
                $sale->product_id = $cart->id;
                $sale->product_price = $cart->price;
                $sale->product_qty = 1;
                $sale->total_price = $cart->options->total;

                $sale->vori = $cart->options->vori;
                $sale->ana = $cart->options->ana;
                $sale->roti = $cart->options->roti;
                $sale->miliroti = $cart->options->miliroti;
                $sale->save();

                /* Barcode Generate Start */
                $sale_info = DB::table('stockouts')->orderBy('id', 'desc')->first();
                $sale_id = $sale_info->id;
                $barcode_update = Barcode::where('barcode', $cart->options->barcode);
                $barcode_update->update(['stockout_id' => $sale_id]);
                /* Barcode Generate End */

            }
        }

        /* cash book store start */
        $cashbook = new CashBook;
        $cashbook->invoice_id = $inv_id;
        $cashbook->expense = $request->total_paid;
        $cashbook->save();

        /* Cash book store end */
        Cart::destroy();

        session()->flash('success','Product SALE succesfully complete');
        return redirect()->route('sale.view');


    }


    public function viewsale(Request $request)
    {
        $sales = Stockout::all();
        return view('admin.sale.view-sale',compact('sales'));
    }

    
    public function show($id)
    {
        //
    }

    
    public function SaleInvoice($id)
    {
        $invoice = Invoice::find($id);
        return view('admin.sale.invoice-info-sale', compact('invoice'));
    }


    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function barcodelist(){
        $barcodes = Barcode::where('stockout_id',null)->where('gold_maker_id',null)->whereNotNull('stockin_id')->get();
        return view('admin.sale.barcode',compact('barcodes'));
    }
}
