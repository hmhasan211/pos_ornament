<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Color;
use DB;

class ColorController extends Controller
{

    public function index()
    {
        $color= Color::all();
        return view('admin.color.add-color',compact('color'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:colors',
        ]);
        $color = new Color();
        $color->name = $request->name;
        $color->save();
        session()->flash('success','color has store Successfully');
        return redirect()->route('color.add');
    }

    public function show($id)
    {
        $color = color::find($id);
        return view('admin.color.edit-color',compact('color'));
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:colors',
        ]);

        $color = Color::find($request->id);
        $color->name = $request->name;
        $color->update();
        session()->flash('success','Color has Update Successfully');
        return redirect()->route('color.add');
    }

    public function destroy(Request $request)
    {
        $delete = Color::find($request->id);
        $delete->delete();
    }
}

