<?php

namespace App\Http\Controllers\backend;

use App\Model\InStock;
use App\Model\OutStock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Product;
use App\Model\ProductColor;
use App\Model\ProductSize;
use App\Model\ProductKarat;
use App\Model\OriginProduct;
use App\Model\Invoice;
use App\Model\Stockin;
use App\Model\CashBook;
use App\Model\Stockout;
use Cart;
use App\Model\customer;
use App\Model\SubCategory;
use App\Model\Barcode;
use App\Model\GoldMaker;
use App\Model\BarcodeGoldMaker;
use App\Model\Supplier;
use DB;
use Auth;

class StockController extends Controller
{
    public function index()
    {

        Cart::destroy();
        $GoldMakers = GoldMaker::pluck('name', 'id');
        return view('admin.stock.stock-out', compact('GoldMakers'));
    }


    public function BarcodeSearch(Request $request)
    {
        $barcode = $request->barcode;
        //dd($barcode);
     //   $pid = Barcode::where('barcode', $barcode)->where('outstock_id',null)->where('stockout_id',null)->where('instock_id','!=',null)->orWhere()->first();
        $pid =  DB::select("SELECT * FROM barcodes WHERE barcode='$barcode' AND stockout_id is null and outstock_id is null AND (stockin_id is NOT Null or instock_id is NOT Null)" );
        $stockin_id = $pid[0]->stockin_id;
        $instock_id=$pid[0]->instock_id;
        $product_id=$pid[0]->product_id;
        if ($pid) {
            $product_info = Product::Where('id', $product_id)->first();

            if($stockin_id) {
                $pro_data = Stockin::where('id', $stockin_id)->where('product_id', $product_id)->first();
            }else{
                $pro_data = InStock::where('id', $instock_id)->where('product_id', $product_id)->first();


            }
            /* Cart have data or not */
            /* if cart has data */
            /* check already sold or not start */
            if ($pid[0]->stockout_id == NULL) {
                /* check already sold or not End */
                if (Cart::count() > 0) {
                    /* barcode match finding foreach loop start */
                    foreach (Cart::content() as $cart) {
                        if ($barcode == $cart->options->barcode) {
                            return $barcode;
                        }
                    }
                    /* barcode match finding foreach loop end */
                    /* add to sale cart start */
                    $this->addsaleitem($product_info, $pro_data, $barcode, $stockin_id,$instock_id);
                    echo $this->SaleCart();
                    /* add to sale cart End */
                } else {
                    $this->addsaleitem($product_info, $pro_data, $barcode, $stockin_id,$instock_id);
                    echo $this->SaleCart();
                    // cart blank and add new data into cart
                }
            } else {
                return "__" . $barcode;
            }/* already sold else condition end here */
        } else {
            //echo $this->SaleCart();
            return "not-found : " . $barcode;
        }
    }/* method ending */

    /* add to sale cart item start */

    public function addsaleitem($product_info, $pro_data, $barcode, $stockin_id,$instock_id)
    {
        Cart::add([
            'id' => $product_info->id,
            'name' => $product_info->product_name,
            'price' => $product_info->product_price,
            'qty' => 1,
            'options' => [
                'barcode' => $barcode,
                //'weight_id' => $pro_data->weight_id,
                //'weight_amount' => $pro_data->weight_amount,
                'stockin_id' => $stockin_id,
                'instock_id' => $instock_id,
            ]
        ]);

    }



    /* add to sale cart item End */

    /* Sale Cart Publish */

    public function BarcodeSerialSearch(Request $request)
    {

        $start = $request->start;
        $end = $request->end;

        /* TOTAL NUMBER OF ITEM WHICH WILL BE ADDED INTO CART . WE SHOULD MAKE A LOOP FOR IT .*/

        $error = 0;
        for ($start; $start <= $end; $start++) {

            if (Cart::count() > 0) {
                $barcode = str_pad($start, 12, 0, STR_PAD_LEFT);
                $pid = Barcode::where('barcode', '=', $barcode)->first();
                if ($pid) {
                    /* product sold or not checking start */
                    if ($pid->stockout_id == NULL) {
                        /* already added or not checking start */
                        $cart_lists = Cart::content();
                        foreach ($cart_lists as $cart) {
                            if ($start == $cart->options->barcode) {
                                return $start;
                            }
                        }
                        /* already added or not checking end */

                        $product_info = Product::Where('id', $pid->product_id)->first();
                        $pro_data = Stockin::where('id', $pid->stockin_id)->where('product_id', $pid->product_id)->first();
                        $this->addsaleitem($product_info, $pro_data, $barcode);
                    } else {
                        echo '<script> alert("Sorry this product already sold");</script>';
                    }
                } else {
                    // echo $this->SaleCart();

                    return "__" . $barcode;
                }
            } else {
                $barcode = str_pad($start, 12, 0, STR_PAD_LEFT);
                $pid = Barcode::where('barcode', '=', $barcode)->first();
                if ($pid) {
                    if ($pid->stockout_id == NULL) {
                        $product_info = Product::Where('id', $pid->product_id)->first();
                        $pro_data = Stockin::where('id', $pid->stockin_id)->where('product_id', $pid->product_id)->first();
                        $this->addsaleitem($product_info, $pro_data, $barcode);
                    } else {
                        echo '<script> alert("Sorry this product already sold");</script>';
                    }

                } else {
                    //echo $this->SaleCart();
                    return "__" . $barcode;
                }
            }
            /* already added or not checking end */
        }/* For Loop Ending here*/
        /* cart item show start */
        echo $this->SaleCart();
        /* cart item show end */
    }

    public function SaleCart()
    {

        $cartitem = Cart::content();
        $i = 0;
        $j = 0;
        $k = 0;
        if (Cart::count() > 0) {
            $total = 0;
            foreach ($cartitem as $cartinfo) {

                $total += $cartinfo->price * $cartinfo->qty;

                $pro = Barcode::where('barcode', '=', $cartinfo->options->barcode)->first();
                $product_info = Product::Where('id', $cartinfo->id)->first();

                foreach ($product_info->categorie->sub_categories as $sub) {
                }
                if ($cartinfo->qty > 1) {
                    $qty = '<label class="label label-danger">Remove this ' . $cartinfo->qty . '</label>';
                } else {
                    $qty = $cartinfo->qty;
                }

                echo '<tr id="rowid' . $cartinfo->options->barcode . '">
                    <td>' . ++$i . '</td>
                    <td>' . $cartinfo->options->barcode . '</td>
                    <td>' . $product_info->product_code . '</td>
                    <td>' . $product_info->product_name . '</td>
                    <td>' . $product_info->brand->name . '</td>
                    <td>' . $product_info->origin->name . '</td>
                    <td>' . $product_info->karat->karat_size . '</td>
                    <td>' . $product_info->size->name . '</td>
                    <td>' . $product_info->color->name . '</td>
                    <td>' . $product_info->gold_type->name . '</td>
                    <td>' . $cartinfo->price . '</td>
                    <td>' . $qty . '</td>
                    <td>' . $product_info->product_price * $cartinfo->qty . '</td>
                    <td><button type="button" data-id="' . $cartinfo->rowId . '" id="removelist' . ++$k . '" onclick="itemremove(' . ++$j . ')" class="fa fa-trash btn btn-danger"></button></td>
                  </tr>';

            }
            echo '<tr><td colspan="16"><p class="bg-primary text-center" style="padding:8px 0">Total Cart Item : ' . Cart::count() . ' </p>
            <script>
                $("#total_price").val("' . $total . '");
                $("#total_balance").val("' . $total . '");
                $("#total_balance").css({"background":"red","color":"#fff","font-weight":"bold","letter-spacing":"2px"});
            </script>
            ';
        } else {
            echo '<tr><td colspan="16"><p class="bg-danger text-center" style="padding:8px 0"> No item found at Shopping-cart </p>';
        }

    }

    /* OUT STOCK STORE START FROM HERE */
    public function store(Request $request)
    {
        $this->validate($request, [
            'gold_maker_id' => 'required'
        ]);

        $items = Cart::content();
        foreach ($items as $value) {
            $save = new BarcodeGoldMaker;
            $save->gold_maker_id = $request->gold_maker_id;
            $save->barcode_purchase_id = $value->options->barcode_id;
            $save->save();

            /* status change for Barcode Purchase Table */
            $status = Barcode::find($value->options->barcode_id);
            $status->salestatus = 1;
            $status->update();
        }
        Cart::destroy();
        session()->flash('success', 'PRODUCT STOCK OUT has succesful');
        return redirect()->route('stock.out');

    }
    /* OUT STOCK STORE END FROM HERE */

    /* process view start */

    public function gold_maker_view()
    {
        //status=1 refers its sent to gold maker to produce new product
        $stockouts = OutStock::whereNotNull('gold_maker_id')->orderBy('id', 'DESC')->get();


        return view('admin.stock.view-stockout', compact('stockouts'));
    }
    /* process view end */

    /* stockin barcode view start */

    public function stock_barcode()
    {
        $barcodes = DB::select("SELECT * FROM barcodes WHERE stockout_id is null and outstock_id is null AND (stockin_id is NOT Null or instock_id is NOT Null)" );
        return view('admin.stock.barcode', compact('barcodes'));
    }

    /* stockin barcode view end */


    /* Get Return back product from Gold Maker */

    public function stockin()
    {
        Cart::destroy();
        $product_name = Product::pluck('product_name', 'id');
        $product_code = Product::pluck('product_code', 'id');
        $outinvoice = OutStock::whereNotNull('invoice_id')->pluck('invoice_id');
        $invoices = Invoice::whereIn('id', $outinvoice)->pluck('invoice_no', 'id');
        $goldmaker = GoldMaker::pluck('name', 'id');
        return view('admin.stock.in-stock', compact('product_code', 'product_name', 'goldmaker', 'invoices'));
    }

    /* Get Return back product from Gold Maker End */

    public function instockview()
    {
        $instock = InStock::orderBy('id', 'DESC')->get();
        return view('admin.stock.instock-view', compact('instock'));
    }


    public function stock()
    {
        $products = Product::groupBy('id')->paginate(10);
        return view('admin.stock.stock', compact('products'));
    }

    public function viewstock()
    {
        $i = 0;
        $products = Product::groupBy('id')->paginate(10);
        foreach ($products as $product) {
            $stockfound = Barcode::where('product_id', $product->id)->where('stockout_id', NULL)->where('gold_maker_id', NULL)->count();
            if ($stockfound > 0) {
                echo '<tr>
                    <td>' . ++$i . '</td>
                    <td>' . $product->product_code . '</td>
                    <td>' . $product->product_name . '</td>
                    <td>' . $stockfound . '</td>
                </tr>';
            }
        }
    }


    public function SearchStock(Request $request)
    {
        //Cart::destroy()
        $search_key = $request->proname;

        $search = Product::where('product_name', 'LIKE', "%" . $search_key . "%")->orWhere('product_code', 'LIKE', '%' . $search_key . '%')->get();
        $i = 0;
        $c = 0;
        if ($search) {
            $c += $search->count();
            if ($c > 0) {
                $i = 0;
                foreach ($search as $info) {
                    $stockfound = Barcode::where('product_id', $info->id)->where('stockout_id', NULL)->count();
                    if ($stockfound > 0) {
                        echo "<tr>
                          <td>" . ++$i . "</td>
                          <td>" . $info->product_code . "</td>
                          <td>" . $info->product_name . "</td>
                          <td>" . $stockfound . "</td>
                      </tr>";
                    }


                }
                echo '<tr>
                         <td colspan="4" class="bg-primary text-center">Total Result Found : ' . $i . '</td>
                    </tr>';
            } else {
                echo '<tr>
                         <td colspan="4" class="bg-danger text-center">Nothing found for ' . $search_key . '</td>
                    </tr>';
            }

        } else {

        }
    }

    /* Gold For Gold Maker Start */

    public function GoldToGoldMaker(Request $request)
    {
        $gold_maker_id = $request->gold_maker_id;
        $description = $request->description;

        $total = 0;

        //   dd(Cart::content());
        foreach (Cart::content() as $product) {
            $total += $product->price;
        }

        $invoice = new Invoice();
        $inv_serial = substr(md5(time()), 0, 6);
        $invoice->invoice_no = Date('Y-m-d') . "_" . $inv_serial;
        $invoice->gold_maker_id = $gold_maker_id;
        $invoice->total_amount = $total;
        $invoice->total_paid = $total;
        $invoice->total_balance = 0;
        $invoice->total_discount = 0;
        $invoice->payment_type = 0;
        $invoice->note = 0;
        $invoice->save();

        /* Barcode table gold_maker_id update start */
        foreach (Cart::content() as $item) {

            /* outstocks table data insert start */
            $outstocks = new OutStock();
            $outstocks->invoice_id = $invoice->id;
            $outstocks->product_id = $item->id;
            $outstocks->stockin_id = $item->options->stockin_id;
            $outstocks->instock_id = $item->options->instock_id;
            $outstocks->gold_maker_id = $gold_maker_id;
            $outstocks->product_price = $item->price;
            $outstocks->product_qty = $item->qty;
            $outstocks->total_price = $item->price * $item->qty;
            $outstocks->description = $description;

            $outstocks->save();
            Barcode::where('barcode', $item->options->barcode)->update(['outstock_id' => $outstocks->id]);
            /* outstocks table data insert end */
        }

        /*$cashbook = new CashBook;
        $cashbook->invoice_id = $invoice->id;
        $cashbook->expense = $total;
        $cashbook->save();*/

        Cart::destroy();
        session()->flash('success', 'Gold handover to Gold-Maker successfuly complete');
        return redirect()->route('stock.out');
        /* Barcode table gold_maker_id update End */
        /*  update into */
    }


    /* Gold For Gold Maker End */

    /* Gold Maker Gold Handover Invoice Start */
    public function OutStockInvoice($id)
    {

        $invoice = Invoice::find($id);
        return view('admin.stock.invoice-info-goldmaker', compact('invoice'));

    }
    /* Gold Maker Gold Handover Invoice End */


    /*In Stock Process Start */
    public function instocksave(Request $request)
    {
        /* Gold Maker ID Exist or not */

        if (!empty($request->goldmaker_id)) {
            $gm_id = $request->goldmaker_id;
        } else {
            $this->validate($request, [
                'name' => 'required',
                'phone' => 'required|unique:gold_makers',
                'address' => 'required',
            ]);
            /* store Supplier in Supplier table */
            $goldmaker = new GoldMaker();
            $goldmaker->name = $request->name;
            $goldmaker->phone = $request->phone;
            $goldmaker->address = $request->address;
            $goldmaker->save();
            $gm_id = $goldmaker->id;
        }

        /* invoice create */

        $invoice = new Invoice();

        $inv_serial = substr(md5(time()), 0, 6);
        $invoice->invoice_no = Date('Y-m-d') . "_" . $inv_serial;
        $invoice->gold_maker_id = $gm_id;
        $invoice->total_amount = $request->total_amount;
        $invoice->total_paid = $request->total_amount;
        $invoice->total_balance = 0;
        $invoice->total_discount = 0;
        $invoice->payment_type = "Gold Maker Delivery";
        $invoice->payment_trxid = 0;
        $invoice->note = $request->sale_note;
        $invoice->save();

        $inv_id = $invoice->id;


        $cart_item = Cart::content();

        foreach ($cart_item as $cart) {

            /* Product Information Fetch */

            $stockin = new InStock();
            $stockin->invoice_id = $inv_id;
            //$stockin->outstock_invoice = $request->outstock_invoice;
            $stockin->product_id = $cart->id;
            $stockin->gold_maker_id = $gm_id;
            $stockin->product_price = $cart->price;
            $stockin->product_qty = 1;
            $stockin->total_price = $cart->options->total;

            $stockin->vori = $cart->options->vori;
            $stockin->ana = $cart->options->ana;
            $stockin->roti = $cart->options->roti;
            $stockin->miliroti = $cart->options->miliroti;

            $stockin->save();

            $stockin_id = $stockin->id;

            /* Barcode Generate Start */
            $newcode = 0;
            $y = 0;
            $code_generate = 0;
            for ($i = 1; $i <= $cart->qty; $i++) {

                /* Product Id Exist or Not If Exist then what's the last id of this Product */

                $pro_id = Barcode::where('product_id', $cart->id)->orderBy('id', 'desc')->first();
                if ($pro_id) {
                    /* when product id already exists . Then it will be start new loop here for store barcode inside Barcode_purchases table */
                    $barcode = new Barcode();
                    $stringConvert = substr($pro_id->barcode, 6, 13);
                    $barcode->barcode = str_pad($cart->id, 6, 0, STR_PAD_LEFT) . str_pad($stringConvert + 1, 6, 0, STR_PAD_LEFT);
                    $barcode->instock_id = $stockin_id;
                    $barcode->product_id = $cart->id;
                    $barcode->save();
                } else {
                    $barcode = new Barcode();
                    $code_generate = str_pad($cart->id, 6, 0, STR_PAD_LEFT) . str_pad($cart->id + $i, 6, 0, STR_PAD_LEFT);
                    $barcode->barcode = $code_generate;
                    $barcode->instock_id = $stockin_id;
                    $barcode->product_id = $cart->id;
                    $barcode->save();
                }
            }
            /* Barcode Generate End */

        }
        /* cash book store start */
        /*$cashbook = new CashBook;
        $cashbook->invoice_id = $inv_id;
        $cashbook->expense = $request->total_amount;
        $cashbook->save();*/
        /* Cash book store end */

        Cart::destroy();
        session()->flash('success', 'In Stock succesfully complete');
        return redirect()->route('stock.in');


    }


    /*In Stock Process End */


}
