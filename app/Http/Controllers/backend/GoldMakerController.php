<?php

namespace App\Http\Controllers\backend;

use App\Model\GoldMaker;
use App\Model\InStock;
use App\Model\OutStock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Brand;
use App\Model\Category;
use App\Model\Size;
use App\Model\Product;
use App\Model\Color;
use App\Model\Karat;
use App\Model\Origin;
use App\Model\GoldType;
use App\Model\Supplier;
use App\Model\SubCategory;
use App\Model\Weight;
use App\Model\ProductColor;
use App\Model\ProductKarat;
use App\Model\ProductSize;
use App\Model\OriginProduct;
use App\Http\Requests\GoldMakerRequest;
use Cart;

class GoldMakerController extends Controller
{

    public function index()
    {
        $GoldMakers = GoldMaker::orderBy('id', 'DESC')->get();
        return view('admin.goldmaker.add-goldmaker', compact('GoldMakers'));
    }


    public function store(GoldMakerRequest $request)
    {
        $goldmaker = new GoldMaker();
        $goldmaker->name = $request->name;
        $goldmaker->phone = $request->phone;
        $goldmaker->address = $request->address;

        $image = $request->file('goldmakerimage');
        if ($image) {
            $image_name = substr(md5(time()), 0, 6);
            $filename = Date('Y') . '_' . $image_name . "." . $image->getClientOriginalExtension();
            $image->move(base_path('public/admin/goldmaker/'), $filename);
            $goldmaker->image = $filename;
        }
        $goldmaker->save();
        session()->flash('success', 'Gold Maker has store successful');
        return redirect()->route('goldmaker.add');
    }


    public function update(GoldMakerRequest $request)
    {
        $goldmaker = GoldMaker::find($request->id);
        $goldmaker->name = $request->name;
        $goldmaker->phone = $request->phone;
        $goldmaker->address = $request->address;
        $image = $request->file('goldmakerimage');
        if ($image) {
            $image_name = substr(md5(time()), 0, 6);
            $filename = Date('Y') . '_' . $image_name . "." . $image->getClientOriginalExtension();
            $image->move(base_path('public/admin/goldmaker/'), $filename);
            $goldmaker->image = $filename;
        }
        $goldmaker->update();
        session()->flash('success', 'Gold Maker has update successful');
        return redirect()->route('goldmaker.add');
    }


    public function destroy(Request $request)
    {
        $delete = GoldMaker::find($request->id);
        $delete->delete();
    }


    /* customers search */
    public function customersearch(Request $request)
    {
        $goldmaker = GoldMaker::find($request->customer_id);
        echo json_encode($goldmaker);
    }

    /* customers end */


    /* Showing gold makers in a grid start */

    public function gold_maker_grid()
    {
        $goldmakers = GoldMaker::all();
        return view('admin.goldmaker.profile-grid-goldmaker', compact('goldmakers'));

    }

    /* Showing gold makers in a grid end */

    /* Gold makers individual invoice start */
    public function gold_maker_invoice($id)
    {

        $instocks = InStock::where('gold_maker_id', $id)->get();
        $outstocks = OutStock::where('gold_maker_id', $id)->get();
        return view('admin.goldmaker.invoice-goldmaker', compact('instocks', 'outstocks'));

    }
    /* Gold makers individual invoice end */

    /* Barcode List Create */
    public function barcodelist(Request $request)
    {
        if (Cart::count() > 0) {

            $data = Cart::content();

            foreach ($data as $value) {
                if ($value->id == $request->barcode) {
                    echo "<script> alert('Barcode already exits in this Barcode List');</script>";
                } else {
                    Cart::add([
                        'id' => (int)$request->barcode,
                        'name' => $request->barcode,
                        'qty' => 1,
                        'price' => 1,
                    ]);
                }
            }
        } else {
            Cart::add([
                'id' => (int)$request->barcode,
                'name' => $request->barcode,
                'qty' => 1,
                'price' => 1,
            ]);
        }
        echo $this->barcodelist_fetch();
    }

    /* Barcode List Create end */

    /* List Fetch start */

    public function barcodelist_fetch()
    {
        $data = Cart::content();
        foreach ($data as $value) {
            echo '<p class="bg-primary" style="padding:5px;">' . $value->name . ' <button id="coderemove" class="btn btn-danger fa fa-trash pull-right" data-id="' . $value->rowId . '" type="button"></button> </p>';
        }
    }

    /* List Fetch End*/

    /* Item Remove from List  Start */
    public function coderemove(Request $request)
    {
        Cart::remove($request->rowId);
    }
    /* Item Remove from  List End*/


    /* Gold Maker Search Start */

    public function goldmakersearch(Request $request)
    {
        $supplier = GoldMaker::find($request->goldmaker_id);
        echo json_encode($supplier);
    }

    /* Gold Maker Search End */
}

