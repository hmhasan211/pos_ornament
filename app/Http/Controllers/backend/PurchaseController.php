<?php

namespace App\Http\Controllers\backend;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Product;
use App\Model\ProductColor;
use App\Model\ProductSize;
use App\Model\ProductKarat;
use App\Model\OriginProduct;
use App\Model\karat;
use App\Model\Color;
use App\Model\Size;
use App\Model\Weight;
use App\Model\Origin;
use App\Model\Brand;
use App\Model\Category;
use App\Model\GoldType;
use App\Model\Invoice;
use App\Model\Stockin;
use App\Model\CashBook;
use Cart;
use App\Model\Supplier;
use App\Model\SubCategory;
use App\Model\Barcode;
use DB;
use Auth;

class PurchaseController extends Controller
{
    
    public function index()
    {
       //dd(Cart::content());
        Cart::destroy();
        $product_name = Product::pluck('product_name','id');
        $product_code = Product::pluck('product_code','id');
        $weight = Weight::pluck('name','id');
        $suppliers = Supplier::pluck('name','id');
        return view('admin.purchase.add-purchase',compact('product_code','product_name','weight','suppliers'));
    }



  
    public function  ProductFind(Request $request)
    {
        $product_info = Product::where('product_code',$request->product_code_id)->orWhere('id',$request->product_code_id)->first();

        foreach ($product_info->categorie->sub_categories as $sub) { }

        $packet_data ='{"id":"'.$product_info->id.'","product_name":"'.$product_info->product_name.'","product_price":"'.$product_info->product_price.'","product_details":"'.$product_info->product_details.'","karat":"'.$product_info->karat->karat_size.'","color":"'.$product_info->color->name.'","size":"'.$product_info->size->name.'","origin":"'.$product_info->origin->name.'","product_local_name":"'.$product_info->product_local_name.'","brand":"'.$product_info->brand->name.'","category":"'.$product_info->categorie->name.'","subcategorie":"'.$sub->name.'","goldtype":"'.$product_info->gold_type->name.'"}';
        echo json_encode($packet_data);
       

    }


    public function ProductAddCart(Request $request)
    {

        $product_info = Product::find($request->id);


        Cart::add([
            'id'=> $product_info[0]->id,
            'name'=> $product_info[0]->product_name,
            'price'=>$request->product_price,
            'qty'=>$request->product_qty,
            'options'=>[
                'vori'=>$request->vori,
                'ana'=>$request->ana,
                'roti'=>$request->roti,
                'miliroti'=>$request->miliroti,
                'total'=>$request->rate,
                'making_charge'=>$request->making_charge,
                'gram'=>$request->gram,
                'status'=>'purchase',
            ]
        ]);


       echo $this->Carts();
    }


    public function Carts()
    {


        /* foreach (Cart::content() as $cart){
            dd($cart->options->status);
        }*/
        //Cart::destroy();
        $cart_info = Cart::content();



        $i=0;
        $total = 0;
        $m = 0;


        if(Cart::count() != 0){

            foreach($cart_info as $cart_details) {
               // dd($cart_details);

                if ($cart_details->options->status == 'purchase') {

                    $sub = $cart_details->options->total;
                    $making_charge = $cart_details->options->making_charge;
                    $total = $total + $sub + $making_charge;
                    echo '<tr>
                    <td>' . ++$i . '</td>
                    <td>' . $cart_details->name . '</td>
                    <td style="width : 15%;"> 
                        ' . $cart_details->options->gram . ' গ্রাম 
                    </td>
                    <td style="width : 20%;">
                        <input type="number" id="cart_price" value="' . $cart_details->price . '" class="form-control" style="display:none" >
                        ' . $cart_details->price . ' টাকা
                    </td>
                    <td> 
                        <span id="sub_total' . $cart_details->id . '">' . $cart_details->options->total . ' টাকা </span>
                    </td>
                    <td> 
                        <button type="button" style="display:none" class="btn btn-primary" data-id="' . $cart_details->rowId . '" id="update_item"><i class="fa fa-edit"></i>  || </button>
                           
                        <button type="button" class="btn btn-danger" data-id="' . $cart_details->rowId . '" id="remove_item"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>';
                    $m = $m + $cart_details->options->making_charge;
                    echo '<script>
                           $("#total_amount").val(' . $total . ');
                           $("#making_ch").val(' . $m . ');
                           
                    </script>';
                }
            }
        }
        else{
            echo '<tr><td colspan="6"><p class="bg-danger text-center" style="padding:8px 0"> No item found at Shopping-cart </p>';
        }

    }




    public function CartUpdate(Request $request){

        $rowId = $request->rowId;
        $product_info = Product::find($request->id);

        foreach($product_info as $product){}

        Cart::update($rowId,[
            'id'=> $product_info[0]->id,
            'name'=> $product_info[0]->product_name,
            'price'=>$request->product_price,
            'qty'=>$request->product_qty,
            'options'=>[
                'vori'=>$request->vori,
                'ana'=>$request->ana,
                'roti'=>$request->roti,
                'miliroti'=>$request->miliroti,
                'total'=>$request->rate
            ]
        ]);
        echo $this->Carts();
    }

    /* Cart Item Remove Start */
    public function CartsRemove(Request $request)
    {
        $rowId = $request->rowId;
        Cart::remove($rowId);
        if(Cart::count() != 0){
            echo $this->Carts();
        }
        else{
            echo '<tr><td colspan="6"><p class="bg-danger text-center" style="padding:8px 0"> No item found at Shopping-cart </p>';
        }
    }

    /* Cart Item Remove End*/

    public function CartFind(Request $request){
        $item = Cart::get($request->rowId);

       // return "{'price':'".$item->options->total."'}";
        $product_info = Product::where('product_code',$item->id)->orWhere('id',$item->id)->first();

        foreach ($product_info->categorie->sub_categories as $sub) { }

        $packet_data ='{"id":"'.$product_info->id.'","product_name":"'.$product_info->product_name.'","product_price":"'.$item->price   .'","product_details":"'.$product_info->product_details.'", "karat":"'.$product_info->karat->karat_size.'","color":"'.$product_info->color->name.'","size":"'.$product_info->size->name.'", "origin":"'.$product_info->origin->name.'","product_local_name":"'.$product_info->product_local_name.'", "brand":"'.$product_info->brand->name.'","category":"'.$product_info->categorie->name.'", "subcategorie":"'.$sub->name.'","goldtype":"'.$product_info->gold_type->name.'","qty":"'.$item->qty.'","vori":'.$item->options->vori.',"ana":'.$item->options->ana.',"roti":'.$item->options->roti.',"miliroti":'.$item->options->miliroti.',"total":'.$item->options->total.'}';
    return json_encode($packet_data);

    }

    /* subcategory according to Category parent id start */

    public function SearchSubcategory(Request $request)
    {
        $category_id = Category::find($request->category_id);

        foreach($category_id->sub_categories as $subcategory){
            echo '<option value="'.$subcategory->id.'">'.$subcategory->name.'</option>';
        }
    }

    public function store(Request $request)
    {
        $sup_id = '';
       // dd($request->all());
        /* Supplier references found or not check */

        if( !empty($request->supplier_id)){
            $sup_id = $request->supplier_id;
        }else{
            $this->validate($request,[
                'supplier_name'=>'required',
                'phone'=>'required|unique:suppliers',
                'supplierimage'=>'image|mimes:jpg,jpeg,png,gif,svg',
            ]);
           /* store Supplier in Supplier table */

            $image = $request->file('supplierimage');
            $image_name = substr(md5(time()),0,6);
            $filename = Date('Y').'_'.$image_name.".".$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/supplier/'),$filename);

            $supplier = new Supplier();
            $supplier->name = $request->supplier_name;
            $supplier->phone = $request->phone;
            $supplier->image = $filename;
            $supplier->address = $request->Supplier_address;
            $supplier->save();

            $supplier_id = DB::table('suppliers')->orderBy('id','desc')->first();
          
            $sup_id= $supplier_id->id;
        }

        /* Invoice Create start */

        $invoice = new Invoice();

        if($request->payment_type !="bkash" ||$request->payment_type !="rocket" || $request->payment_type !="card" || $request->payment_type !="check"){
            $invoice->payment_trxid = 0;
        }else{
            $invoice->payment_trxid = $request->bkash_code;
        }

        $inv_serial = substr(md5(time()),0,6);
        $invoice->invoice_no = Date('Y-m-d')."_".$inv_serial;
        $invoice->supplier_id = $sup_id;
        $invoice->total_amount = $request->total_amount;
        $invoice->total_paid = $request->total_paid;
        $invoice->total_makingcharge = $request->making;
        $invoice->total_less = $request->total_less;
        $invoice->total_balance = $request->total_balance;
        $invoice->total_discount = $request->total_discount;
        $invoice->payment_type = $request->payment_type;
        $invoice->payment_deadline = $request->payment_deadline;
        $invoice->note = $request->sale_note;
        $invoice->save();

        /* Last invoice id  catch start */

        $invoice_id = DB::table('invoices')->orderBy('id','desc')->first();
        $inv_id = $invoice_id->id;

        /* Last invoice id  catch End */

        $cart_item = Cart::content();
    
        foreach($cart_item as $cart) {
            if ($cart->options->status == 'purchase') {
                /* Product Information Fetch */

                $stockin = new Stockin();
                $stockin->invoice_id = $inv_id;
                $stockin->product_id = $cart->id;
                $stockin->product_price = $cart->price;
                $stockin->product_qty = $cart->qty;
                $stockin->total_price = $cart->options->total;

                $stockin->vori = $cart->options->vori;
                $stockin->ana = $cart->options->ana;
                $stockin->roti = $cart->options->roti;
                $stockin->miliroti = $cart->options->miliroti;

                $stockin->save();

                $stockin_id = Stockin::where('product_id', $cart->id)->orderBy('id', 'desc')->first();

                /* Barcode Generate Start */
                $newcode = 0;
                $y = 0;
                $code_generate = 0;
                for ($i = 1; $i <= $cart->qty; $i++) {

                    /* Product Id Exist or Not If Exist then what's the last id of this Product */

                    $pro_id = Barcode::where('product_id', $cart->id)->orderBy('id', 'desc')->first();
                    if ($pro_id) {
                        /* when product id already exists . Then it will be start new loop here for store barcode inside Barcode_purchases table */
                        $barcode = new Barcode();
                        $stringConvert = substr($pro_id->barcode, 6, 13);
                        $barcode->barcode = str_pad($cart->id, 6, 0, STR_PAD_LEFT) . str_pad($stringConvert + 1, 6, 0, STR_PAD_LEFT);
                        $barcode->stockin_id = $stockin_id->id;
                        $barcode->product_id = $cart->id;
                        $barcode->stockout_id = NULL;
                        $barcode->save();
                    } else {
                        $barcode = new Barcode();
                        $code_generate = str_pad($cart->id, 6, 0, STR_PAD_LEFT) . str_pad($cart->id + $i, 6, 0, STR_PAD_LEFT);
                        $barcode->barcode = $code_generate;

                        $barcode->stockin_id = $stockin_id->id;
                        $barcode->product_id = $cart->id;
                        $barcode->stockout_id = NULL;
                        $barcode->save();
                    }
                }
                /* Barcode Generate End */
            }
        }
        /* cash book store start */
        $cashbook = new CashBook;
        $cashbook->invoice_id = $inv_id;
        $cashbook->expense = $request->total_paid;
        $cashbook->save();
        /* Cash book store end */

        Cart::destroy();
         session()->flash('success','Product Purchase succesfully complete');
         return redirect()->route('purchase.add');
    }

    
    public function viewpurchase(Request $request)
    {
        $purchase = Stockin::orderBy('id','DESC')->get();
        return view('admin.purchase.view-purchase',compact('purchase'));
    }

    public function stock(){
      //  $stock = DB::SELECT('SELECT count(product_id) as product_stock,product_id,barcode from barcode_purchases where salestatus=0')->get();
        $products = Product::groupBy('id')->paginate(10);
        return view('admin.stock.stock',compact('products'));
    }

    public function viewstock(){

        $i=0;
        
        $products = Product::groupBy('id')->paginate(10);
        foreach($products as $product){
            $stockfound = BarcodePurchase::where('product_id',$product->id)->where('salestatus',0)->count();
            echo '<tr>
                    <td>'.++$i.'</td>
                    <td>'.$product->product_code.'</td>
                    <td>'.$product->product_name.'</td>
                    <td>'.$stockfound.'</td>
                </tr>';
        }

    }                                                                                        


    public function SearchStock(Request $request)
    {  
        $search_key = $request->proname;
        $search = Product::where('product_name','LIKE',"%".$search_key."%")->orWhere('product_code', 'LIKE', '%' . $search_key . '%')->get();
        $i=0;
        $c = 0;
        if($search){
            $c+=$search->count();
            if($c>0){
                foreach ($search as $info){
                    $stockfound = BarcodePurchase::where('product_id',$info->id)->where('salestatus',0)->count();
                    echo "<tr>
                          <td>".++$i."</td>
                          <td>".$info->product_code."</td>
                          <td>".$info->product_name."</td>
                          <td>".$stockfound."</td>
                      </tr>";
                }
            }else{
                echo '<tr>
                     <td colspan="4" class="bg-danger text-center">Nothing found for '.$search_key.'</td>
                </tr>';
            }

        }
        else{

        }
    }

   
    public function edit($id)
    {
        //
    }

  
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}


