<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Customer;
use App\Http\Requests\CustomerRequest;


class CustomerController extends Controller
{


    public function index()
    {
        $customers = Customer::orderBy('id','DESC')->get();
        return view('admin.customer.add-customer',compact('customers'));
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string',
            'phone'=>'required|min:11|unique:customers',
            'customer_image'=>'required'
        ]);
        $image = $request->file('customer_image');
        $image_name = substr(md5(time()),0,6);
        $filename = Date('Y').'_'.$image_name.".".$image->getClientOriginalExtension();
        $image->move(base_path('public/admin/supplier/'),$filename);

        $customer = new Customer();
        $customer->name = $request->name;
        $customer->phone = $request->phone;
        $customer->image = $filename;
        $customer->address =$request->address;
        $customer->save();
        session()->flash('success','customer Saved Successfully');
        return redirect()->route('customer.add');
    }


    public function update(CustomerRequest $request)
    {
        $customer = Customer::find($request->id);
        $customer->name = $request->name;
        $customer->phone = $request->phone;
        $customer->address = $request->address;
        $customer->update();
        session()->flash('success','customer has update successful');
        return redirect()->route('customer.add');
    }


    public function destroy(Request $request)
    {
        $delete = Customer::find($request->id);
        $delete->delete();
    }


    /* customers search */
    public function customersearch(Request $request)
    {
        $customer = Customer::find($request->customer_id);
        echo json_encode($customer);
       //  echo '{"id":"1"}';
    }

    /* customers end */



}
