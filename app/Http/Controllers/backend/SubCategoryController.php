<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\SubCategory;
use App\Http\Request\SubCategoryRequest;

class SubCategoryController extends Controller
{

    public function index()
    {
        $subcategory = SubCategory::all();
        $category = Category::pluck('name','id');
        return view('admin.sub-category.add-sub-category',compact('subcategory','category'));
    }

    public function store(Request $request)
    {
        $save = new SubCategory();
        $save->category_id = $request->category_id;
        $save->name= $request->name;
        $save->save();
        session()->flash('success','Sub-Category has store successfully');
        return redirect()->route('sub-category.add');
    }


    public function show($id)
    {
        $subcategory = SubCategory::find($id);
        $category = Category::all();
        return view('admin.sub-category.edit-sub-category',compact('subcategory','category'));
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);

        $exist = SubCategory::Where('name',$request->name)->Where('category_id',$request->category_id)->exists();
        if($exist){
            session()->flash('exist','Category and Sub-Category already exist in Database');
            return redirect('ProductManagement/sub-category/'.$request->id.'/Edit');
        }else{
            $subcatupdate = SubCategory::find($request->id);
            $subcatupdate->name = $request->name;
            $subcatupdate->update();
            session()->flash('success','Sub-category Successfully Updated.');
            return redirect()->route('sub-category.add');
        }



    }


    public function destroy(Request $request)
    {
        $delete = SubCategory::find($request->id);
        $delete->delete();
    }
}
