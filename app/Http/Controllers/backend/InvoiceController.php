<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Invoice;
use App\Model\CashBook;
use App\Model\Purchase;
use App\Model\Sale;
use Auth;

use Carbon\Carbon;
use function PhpParser\filesInDir;


class InvoiceController extends Controller
{

    public function index()
    {
        $invoice = Invoice::orderBy('id','DESC')->get();
        return view('admin.purchase.invoice-view',compact('invoice'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function InvoiceDetails($id)
    {
        $invoice = Invoice::find($id);
        return view('admin.purchase.invoice-info', compact('invoice'));
    }

    public function InvoiceConfirm(Request $request)
    {
        $invo = Invoice::find($request->id);
        $invo->order_status = 1;
        $invo->update();
        echo '{"confirm":"1"}';
    }

    public function InvoiceDue()
    {
      // dd(Carbon::now());
        $invoice = Invoice::whereNotNull('supplier_id')->where('total_interest',null)->orderBy('id','DESC')->get();
        return view('admin.purchase.due-invoice',compact('invoice'));
    }

    public function Sale_Invoice_Due()
    {
      // dd(Carbon::now());
        $invoice = Invoice::where('total_interest',null)->whereNotNull('customer_id')->orderBy('id','desc')->get();
        return view('admin.sale.due-invoice',compact('invoice'));
    }



    public function InvoiceDuePayment(Request $request)
    {
        $less = $request->less;
        $paid_amount = $request->amount;
        $interest = $request->total_interest;
        $get_row = Invoice::find($request->invoice_id);

        $old_less = $get_row->total_less;
        $total_less = $old_less+$less;
        $old_balance=$get_row->total_balance;
        $new_balance=$old_balance-$less;
        $get_row->total_less=$total_less;
        $get_row->total_balance=$new_balance-$paid_amount;
        $paid_current=$get_row->total_paid;
        $get_row->total_paid=$paid_current+$paid_amount;
        $get_row->total_interest=$interest;

        $get_row->update();

        /*if($less!=0){
            $invoice = Invoice::find($request->invoice_id);
            $invoice->total_less = $total_less;
            $invoice->update();
        }*/

        $update = new CashBook();
        $update->invoice_id =  $request->invoice_id;
        $update->expense = $request->amount;
        $update->save();
        echo '{"success":"yes"}';
    }
    public function changepaymentdate(Request $request){
        $cdate=$request->payment_deadline;
        $id=$request->invoice_id;
        $invoiceid=Invoice::find($id);
        $invoiceid->payment_deadline=$cdate;
        $invoiceid->update();

    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
