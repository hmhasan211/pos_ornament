<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category;
use DB;

class categoryController extends Controller
{
    
    public function index()
    {
        $category= Category::all();
        return view('admin.category.add-category',compact('category'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:categories',
        ]);

        $category = new Category();
        $category->name = $request->name;
        $category->save();
        session()->flash('success','Category has store Successfully');
        return redirect()->route('category.add');
    }

    public function show($id)
    {
       $category = Category::find($id);
       return view('admin.category.edit-category',compact('category'));
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:categories',
        ]);

        $category = Category::find($request->id);
        $category->name = $request->name;
        $category->update();
        session()->flash('success','Category has Update Successfully');
        return redirect()->route('category.add');
        
    }

    public function destroy(Request $request)
    {
        $delete = Category::find($request->id);
        $delete->delete();
    }
}

