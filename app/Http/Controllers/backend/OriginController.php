<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Origin;
use DB;

class OriginController extends Controller
{
    
    public function index()
    {
        $origin= Origin::all();
        return view('admin.origin.add-origin',compact('origin'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:origins',
        ]);

        $origin = new Origin();
        $origin->name = $request->name;
        $origin->save();
        session()->flash('success','origin has store Successfully');
        return redirect()->route('origin.add');
    }

    public function show($id)
    {
       $origin = origin::find($id);
       return view('admin.origin.edit-origin',compact('origin'));
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:origins',
        ]);

        $origin = Origin::find($request->id);
        $origin->name = $request->name;
        $origin->update();
        session()->flash('success','origin has Update Successfully');
        return redirect()->route('origin.add');
    }

    public function destroy(Request $request)
    {
        $delete = Origin::find($request->id);
        $delete->delete();
    }
}

