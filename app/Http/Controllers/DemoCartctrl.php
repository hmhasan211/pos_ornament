<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use Illuminate\Support\Facades\Session;

class DemoCartctrl extends Controller
{
    public function index(){

        return view('admin.democart.add');
    }

    public function AddToCart(Request $request)
    {

        Cart::add([
            'id'=> $request->id,
            'name'=> $request->name,
            'price'=>$request->price,
            'qty'=>$request->qty,
            'options'=>[
                'country'=>$request->country,
                'brand'=>$request->brand,

            ]
        ]);

        echo $this->Carts();
    }


    public function Carts()
    {
        $cart_info = Cart::content();

        $i=0;

        if(Cart::count() != 0){
            foreach($cart_info as $cart_details){
                echo '<tr>
                    <td>'.++$i.'</td>
                    <td>'.$cart_details->name.'</td>
                    <td style="width : 15%;">
                     <input type="number" id="cart_qty_edit" value="'.$cart_details->qty.'" class="form-control" >
                        
                    </td>
                    <td style="width : 20%;">
                    <input type="number" id="cart_price_edit" value="'.$cart_details->price.'" class="form-control" >
                        
                    </td>
                     <td style="width : 20%;">
                        
                        '.$cart_details->options->country.'
                    </td>
                     <td style="width : 20%;">
                        
                        '.$cart_details->options->brand.'
                    </td>
                    
                    <td> 
                        <button type="button" class="btn btn-primary" data-id="'.$cart_details->rowId.'" id="update_item"><i class="fa fa-edit"></i>  || </button>
                           
                        <button type="button" class="btn btn-danger" data-id="'.$cart_details->rowId.'" id="remove_item"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>';

            }
        }
        else{
            echo '<tr><td colspan="6"><p class="bg-danger text-center" style="padding:8px 0"> No item found at Shopping-cart </p>';
        }

    }
    public function CartsUpdate(Request $request){
        $rowId = $request->rowId;
        $price=$request->price;
        $qty=$request->qty;
        Cart::update($rowId, ['price' => $price,'qty'=>$qty]);
        session()->flash('success','Bucket has been updated');

    }
    public function CartsRemove(Request $request)
    {
        $rowId = $request->rowId;
        Cart::remove($rowId);
        if(Cart::count() != 0){
            echo $this->Carts();
        }
        else{
            echo '<tr><td colspan="6"><p class="bg-danger text-center" style="padding:8px 0"> No item found at Shopping-cart </p>';
        }
    }
}
