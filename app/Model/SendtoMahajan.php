<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SendtoMahajan extends Model
{
    protected $fillable = ['employee_id','mahajan_id', 'amount','interest','in_date','out_date','description','status','mahajan_receipt_no'];


    public function employee(){
        return $this->belongsTo('App\Model\Employee','employee_id');
    }
    public function mahajan(){
        return $this->belongsTo('App\Model\mahajan','mahajan_id');
    }

    public function instock(){
        return $this->belongsTo('App\Model\instock','instock_id');
    }
    public function product(){
        return $this->belongsTo('App\Model\Product','product_id');
    }
}
