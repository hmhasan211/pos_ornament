<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OutStock extends Model
{
    protected $table = "outstocks";
    protected $fillable = ['product_price', 'product_qty', 'total_price', 'invoice_id', 'product_id', 'weight_id', 'weight_amount'];

    public function Product()
    {
        return $this->belongsTo("App\Model\Product");
    }

    public function GoldMaker()
    {
        return $this->belongsTo('App\Model\GoldMaker');
    }

    public function weight()
    {
        return $this->belongsTo("App\Model\Weight");
    }

    public function size()
    {
        return $this->belongsTo('App\Model\Size');
    }

    public function karat()
    {
        return $this->belongsTo('App\Model\Karat');
    }

    public function color()
    {
        return $this->belongsTo('App\Model\Color');
    }


    public function origin()
    {
        return $this->belongsTo('App\Model\Origin');
    }


    public function gold_type()
    {
        return $this->belongsTo('App\Model\GoldType');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Model\Invoice');
    }

    public function barcode()
    {
        return $this->belongsTo('App\Model\Barcode');
    }

    public function stockin()
    {
        return $this->belongsTo('App\Model\Stockin');
    }
    public function instock()
    {
        return $this->belongsTo('App\Model\InStock');
    }
}
