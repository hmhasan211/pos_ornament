<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['name','image','father','mother','wife','nid','phone1','phone2','address','salary'];

    public function salaries(){
        return $this->hasMany('App\Model\EmployeeSalary');
    }
}
