<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InStock extends Model
{
    protected $table = "instocks";

    protected $fillable = ['total_price', 'product_qty', 'total_price', 'invoice_id', 'product_id','interest','pmt_date'];

    public function Product()
    {
        return $this->belongsTo("App\Model\Product",'product_id');
    }

    public function weight()
    {
        return $this->belongsTo("App\Model\Weight");
    }

    public function size()
    {
        return $this->belongsTo('App\Model\Size');
    }

    public function karat()
    {
        return $this->belongsTo('App\Model\Karat');
    }

    public function color()
    {
        return $this->belongsTo('App\Model\Color');
    }


    public function origin()
    {
        return $this->belongsTo('App\Model\Origin');
    }


    public function gold_type()
    {
        return $this->belongsTo('App\Model\GoldType');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Model\Invoice');
    }

    public function stock_out_invoice()
    {
        return $this->belongsTo('App\Model\Invoice','outstock_invoice');
    }

    public function gold_maker()
    {
        return $this->belongsTo('App\Model\GoldMaker');
    }
}
