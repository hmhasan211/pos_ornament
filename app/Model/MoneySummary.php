<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MoneySummary extends Model
{
    //
    protected $fillable = ['name','phone','amount','cost_type_id','status','note'];

    public function cost(){
        return $this->belongsTo('App\Model\CostType','cost_type_id');

    }
}
