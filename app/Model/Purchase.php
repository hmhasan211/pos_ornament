<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{

    public function Product()
    {
        return $this->belongsTo("App\Model\Product");
    }

    public function weight()
    {
        return $this->belongsTo("App\Model\Weight");
    }

    public function size()
    {
        return $this->belongsTo('App\Model\Size');
    }

    public function karat()
    {
        return $this->belongsTo('App\Model\Karat');
    }

    public function color()
    {
        return $this->belongsTo('App\Model\Color');
    }


    public function origin()
    {
        return $this->belongsTo('App\Model\Origin');
    }

    public function gold_type()
    {
        return $this->belongsTo('App\Model\GoldType');
    }

    public function invoices()
    {
        return $this->belongsTo('App\Model\Invoice');
    }



}
