<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BarcodeGoldMaker extends Model
{
    protected $fillable = ['barcode_purchase_id','gold_maker_id'];

    public function barcode_purchase()
    {
    	return $this->belongsTo('App\Model\BarcodePurchase');
    }


    public function GoldMaker()
    {
    	return $this->belongsTo('App\Model\GoldMaker');
    }


}
