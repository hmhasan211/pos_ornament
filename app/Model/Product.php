<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

	protected $fillable = ['product_name','product_code','product_local_name','product_price','product_details','product_image','brand_id','gold_type_id','categorie_id','sub_categorie_id','size_id','karat_id','origin_id','color_id','isActive'];

    public function size()
    {
    	return $this->belongsTo('App\Model\Size');
    }

    public function karat()
    {
    	return $this->belongsTo('App\Model\Karat');
    }

    public function color()
    {
    	return $this->belongsTo('App\Model\Color');
    }

    public function brand()
    {
        return $this->belongsTo("App\Model\Brand");
    }

    public function categorie()
    {
        return $this->belongsTo("App\Model\Category");
    }

    public function origin()
    {
    	return $this->belongsTo('App\Model\Origin');
    }

    public function gold_type()
    {
        return $this->belongsTo('App\Model\GoldType');
    }

    public function purchases()
    {
        return $this->hasMany("App\Model\Purchase");
    }

    public function invoices()
    {
        return $this->hasMany("App\Model\Invoice");
    }

    public function barcodes()
    {
        return $this->hasMany('App\Model\BarcodePurchase');
    }

    public function sales()
    {
        return $this->hasMany('App\Model\Sale');
    }
}
