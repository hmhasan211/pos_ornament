<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class mortgage_invoice extends Model
{
    public function customer()
    {
        return $this->belongsTo("App\Model\Customer");
    }
    public function cash_books(){
        return $this->hasMany('App\Model\CashBook');
    }
}
