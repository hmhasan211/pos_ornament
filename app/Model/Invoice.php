<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public function Stockins()
    {
        return $this->hasMany("App\Model\Stockin");
    }

    public function supplier()
    {
        return $this->belongsTo("App\Model\Supplier");
    }

    public function GoldMaker()
    {
        return $this->belongsTo('App\Model\GoldMaker');
    }

    public function cash_books(){
        return $this->hasMany('App\Model\CashBook');
    }

    public function Stockouts()
    {
        return $this->hasMany('App\Model\Stockout');
    }

    public function outstocks()
    {
        return $this->hasMany('App\Model\OutStock');
    }


    public function customer()
    {
        return $this->belongsTo("App\Model\Customer");
    }


}
