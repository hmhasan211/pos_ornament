<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Size extends Model

{
	protected $fillable = ['size_id','product_id'];
	
    public function products(){
    	return $this->belongsToMany('App\Model\Product');
    }
}
