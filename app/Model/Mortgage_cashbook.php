<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Mortgage_cashbook extends Model
{
    public function invoice()
    {
        return $this->belongsTo("App\Model\mortgage_invoice");
    }
    public function customer()
    {
        return $this->belongsTo("App\Model\Customer");
    }
    public function mahajan()
    {
        return $this->belongsTo("App\Model\Mahajan");
    }
}
