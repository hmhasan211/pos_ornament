<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class mortgage extends Model
{
    public function Product()
    {
        return $this->belongsTo("App\Model\Product",'product_id');
    }

    public function mortgage_invoice()
    {
        return $this->belongsTo('App\Model\mortgage_invoice','mortgage_invoice_id');
    }
}
