<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Karat extends Model
{

    protected $fillable = ['karat_id','product_id'];
	
    public function product(){
    	return $this->belongsToMany('App\Model\Product');
    }
}
