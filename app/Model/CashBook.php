<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CashBook extends Model
{
    public function invoices()
    {
        return $this->belongsTo('App\Model\Invoice');
    }
}
