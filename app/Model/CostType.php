<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CostType extends Model
{
    //protected $table = 'cost_types';
    protected $fillable = ['name'];
}
