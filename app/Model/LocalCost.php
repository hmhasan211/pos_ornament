<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LocalCost extends Model
{
    protected $table = 'localcosts';
    protected $fillable =['cost_type_id','amount','costdate'];


    public function cost(){
        return $this->belongsTo('App\Model\CostType','cost_type_id');
    }
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');

    }

}
