<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GoldMaker extends Model
{
    //
    public function invoices()
    {
        return $this->hasMany("App\Model\Invoice");
    }
}
