<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Stockin extends Model
{
    protected $fillable = ['product_price', 'product_qty', 'total_price', 'invoice_id', 'product_id','vori','ana','roti','miliroti'];

    public function Product()
    {
        return $this->belongsTo("App\Model\Product");
    }

    public function weight()
    {
        return $this->belongsTo("App\Model\Weight");
    }

    public function size()
    {
        return $this->belongsTo('App\Model\Size');
    }

    public function karat()
    {
        return $this->belongsTo('App\Model\Karat');
    }

    public function color()
    {
        return $this->belongsTo('App\Model\Color');
    }


    public function origin()
    {
        return $this->belongsTo('App\Model\Origin');
    }


    public function gold_type()
    {
        return $this->belongsTo('App\Model\GoldType');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Model\Invoice');
    }
    public function stock_out_invoice()
    {
        return $this->belongsTo('App\Model\Invoice','outstock_invoice');
    }
}
