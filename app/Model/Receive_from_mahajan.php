<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Receive_from_mahajan extends Model
{
    public function mahajan(){
        return $this->belongsTo('App\Model\mahajan','mahajan_id');
    }
}
