<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmployeeSalary extends Model
{
    protected $table= 'salaries';
    protected $fillable=['employee_id','paid_date','salary'];

    public function employee(){
        return $this->belongsTo('App\Model\Employee');
    }
}
