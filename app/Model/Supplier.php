<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = ['name','phone','address'];

    public function invoices()
    {
        return $this->hasMany("App\Model\Invoice");
    }
}
