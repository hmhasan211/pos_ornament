<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_name');
            $table->string('product_code');
            $table->string('product_local_name');
            $table->integer('product_price');
            $table->string('product_details',5000);
            $table->string('product_image');
            $table->unsignedInteger('gold_type_id');
            $table->unsignedInteger('brand_id');
            $table->unsignedInteger('categorie_id');
            $table->unsignedInteger('sub_categorie_id');
            $table->unsignedInteger('size_id');
            $table->unsignedInteger('karat_id');
            $table->unsignedInteger('origin_id');
            $table->unsignedInteger('color_id');
            $table->timestamps();
            $table->integer('isActive');
            $table->foreign('gold_type_id')->references('id')->on('gold_types');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('categorie_id')->references('id')->on('categories');
            $table->foreign('sub_categorie_id')->references('id')->on('sub_categories');
            $table->foreign('size_id')->references('id')->on('sizes');
            $table->foreign('karat_id')->references('id')->on('karats');
            $table->foreign('origin_id')->references('id')->on('origins');
            $table->foreign('color_id')->references('id')->on('colors');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
