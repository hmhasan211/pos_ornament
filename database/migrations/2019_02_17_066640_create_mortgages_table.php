<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMortgagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mortgages', function (Blueprint $table) {
            $table->increments('id');
            $table->float('total_price');
            $table->unsignedInteger('mortgage_invoice_id');
            $table->unsignedInteger('product_id');
            $table->integer('vori')->nullable();
            $table->integer('ana')->nullable();
            $table->integer('roti')->nullable();
            $table->integer('miliroti')->nullable();
            $table->text('interest')->nullable();
            $table->date('pmt_date')->nullable();
            $table->integer('product_price')->nullable();
            $table->timestamps();
            $table->foreign("mortgage_invoice_id")->references('id')->on('mortgage_invoices');
            $table->foreign("product_id")->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mortgages');
    }
}
