<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignOutstocksInvoiceProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('outstocks', function (Blueprint $table) {
            $table->foreign("invoice_id")->references('id')->on('invoices');
            $table->foreign("product_id")->references('id')->on('products');
            $table->foreign("gold_maker_id")->references('id')->on('gold_makers');
            $table->foreign("stockin_id")->references('id')->on('stockins');
            $table->foreign("instock_id")->references('id')->on('instocks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outstocks', function (Blueprint $table) {
            $table->dropForeign(['invoice_id']);
            $table->dropForeign(['product_id']);
            $table->dropForeign(['gold_maker_id']);
            $table->dropForeign(['stockin_id']);
            $table->dropForeign(['instock_id']);
        });
    }
}
