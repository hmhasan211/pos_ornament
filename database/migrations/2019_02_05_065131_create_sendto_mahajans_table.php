<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendtoMahajansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sendto_mahajans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mahajan_receipt_no')->nullable();
            $table->unsignedInteger('employee_id')->nullable();
            $table->unsignedInteger('mortgage_invoice_id')->nullable();
            $table->unsignedInteger('mahajan_id')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('interest')->nullable();
            $table->date('in_date')->nullable();
            $table->date('out_date')->nullable();
            $table->string('description')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sendto_mahajans');
    }
}
