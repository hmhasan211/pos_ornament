<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barcodes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('barcode');
            $table->unsignedInteger('stockin_id')->nullable();
            $table->unsignedInteger('product_id');
            $table->unsignedInteger("stockout_id")->nullable();
            $table->unsignedInteger("gold_maker_id")->nullable();
            $table->unsignedInteger("instock_id")->nullable();
            $table->unsignedInteger("outstock_id")->nullable();
            $table->unsignedInteger("mortgage_id")->nullable();
            $table->string("ext2")->nullable();
            $table->timestamps();
            $table->foreign('stockin_id')->references('id')->on('stockins');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('stockout_id')->references('id')->on('stockouts');
            $table->foreign('gold_maker_id')->references('id')->on('gold_makers');
            $table->foreign('instock_id')->references('id')->on('instocks');
            $table->foreign('outstock_id')->references('id')->on('outstocks');
            $table->foreign('mortgage_id')->references('id')->on('mortgages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barcodes');
    }
}
