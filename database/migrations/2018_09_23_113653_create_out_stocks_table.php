<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outstocks', function (Blueprint $table) {
            $table->increments('id');
            $table->float("product_price");
            $table->integer("product_qty");
            $table->float('total_price');
            $table->unsignedInteger('invoice_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('gold_maker_id');
            $table->unsignedInteger('stockin_id')->nullable();
            $table->unsignedInteger('instock_id')->nullable();
            $table->integer('vori')->nullable();
            $table->integer('ana')->nullable();
            $table->integer('roti')->nullable();
            $table->integer('miliroti')->nullable();
            $table->integer('description')->nullable();
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outstocks');
    }
}
