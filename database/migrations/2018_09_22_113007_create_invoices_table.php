<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_no');
            $table->unsignedInteger('supplier_id')->nullable();
            $table->unsignedInteger('customer_id')->nullable();
            $table->unsignedInteger('gold_maker_id')->nullable();
            $table->float('total_amount');
            $table->float('total_less')->nullable();
            $table->float('total_paid')->nullable();
            $table->float('total_balance')->nullable();
            $table->float('total_discount')->nullable();
            $table->float('total_makingcharge')->nullable();
            $table->string( 'payment_deadline')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('payment_trxid')->nullable();
            $table->string('note')->nullable();
            $table->string('ext1')->nullable();
            $table->string('extstring2')->nullable();
            $table->integer('ext2');
            $table->integer('ext3');
            $table->timestamps();
            $table->foreign("supplier_id")->references('id')->on('suppliers');
            $table->foreign("customer_id")->references('id')->on('customers');
            $table->foreign("gold_maker_id")->references('id')->on('gold_makers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
