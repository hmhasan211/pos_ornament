<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignSendtoMahajansMahajansTable extends Migration
{

    public function up()
    {
        Schema::table('sendto_mahajans', function (Blueprint $table) {
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('mortgage_invoice_id')->references('id')->on('mortgage_invoices');
            $table->foreign('mahajan_id')->references('id')->on('mahajans');
        });
    }

    public function down()
    {
        Schema::table('sendto_mahajans', function (Blueprint $table) {
            $table->dropForeign(['employee_id']);
            $table->dropForeign(['mortgage_invoice_id']);
            $table->dropForeign(['mahajan_id']);
        });
    }
}
