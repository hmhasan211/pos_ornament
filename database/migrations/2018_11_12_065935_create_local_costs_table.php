<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('localcosts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cost_type_id');
            $table->integer('amount');
            $table->date('costdate')->nullable();
            $table->string('ext2')->nullable();
            $table->string('ext3')->nullable();
            $table->string('ext4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('localcosts');
    }
}
