<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stockins', function (Blueprint $table) {
            $table->increments('id');
            $table->float("product_price");
            $table->integer("product_qty");
            $table->float('total_price');
            $table->unsignedInteger('invoice_id');
            $table->unsignedInteger('product_id');
            $table->integer('vori')->nullable();
            $table->integer('ana')->nullable();
            $table->integer('roti')->nullable();
            $table->integer('miliroti')->nullable();
            $table->unsignedInteger('outstock_invoice')->nullable();
            $table->integer('ext2')->nullable();
            $table->integer('ext3')->nullable();
            $table->timestamps();
            $table->foreign("invoice_id")->references('id')->on('invoices');
            $table->foreign("product_id")->references('id')->on('products');
            $table->foreign("outstock_invoice")->references('id')->on('invoices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stockins', function (Blueprint $table) {
            $table->dropForeign(['invoice_id']);
            $table->dropForeign(['product_id']);
            $table->dropForeign(['outstock_invoice']);

        });
    }
}
