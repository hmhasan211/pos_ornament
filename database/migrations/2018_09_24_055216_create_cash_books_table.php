<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_books', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('invoice_id')->nullable();
            $table->unsignedInteger('mortgage_invoice_id')->nullable();
            $table->string('receipt_no')->nullable();
            $table->float("expense")->nullable();
            $table->float("income")->nullable();
            $table->timestamps();
            $table->foreign("invoice_id")->references('id')->on("invoices");
            $table->foreign("mortgage_invoice_id")->references('id')->on("mortgage_invoices");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_books');
    }
}
