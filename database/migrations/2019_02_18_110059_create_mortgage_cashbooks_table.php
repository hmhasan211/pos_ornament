<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMortgageCashbooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mortgage_cashbooks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('invoice_id')->nullable();
            $table->unsignedInteger('employee_id')->nullable();
            $table->unsignedInteger('mahajan_id')->nullable();
            $table->unsignedInteger('customer_id')->nullable();
            $table->string('receipt_no')->nullable();
            $table->float("expense")->nullable();
            $table->float("income")->nullable();
            $table->string("description")->nullable();
            $table->date("pmt_date")->nullable();
            $table->timestamps();
            $table->foreign("invoice_id")->references('id')->on("invoices");
            $table->foreign("employee_id")->references('id')->on("employees");
            $table->foreign("mahajan_id")->references('id')->on("mahajans");
            $table->foreign("customer_id")->references('id')->on("customers");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mortgage_cashbooks');
    }
}
