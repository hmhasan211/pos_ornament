<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInStocksTable extends Migration
{
    public function up()
    {
        Schema::create('instocks', function (Blueprint $table) {
                $table->increments('id');
                $table->integer("product_qty");
                $table->float('total_price');
                $table->unsignedInteger('invoice_id');
                $table->unsignedInteger('product_id');
                $table->unsignedInteger('outstock_invoice')->nullable();
                $table->unsignedInteger('gold_maker_id')->nullable();
                $table->integer('vori')->nullable();
                $table->integer('ana')->nullable();
                $table->integer('roti')->nullable();
                $table->integer('miliroti')->nullable();
                $table->text('interest')->nullable();
                $table->date('pmt_date')->nullable();
                $table->integer('product_price')->nullable();
                $table->timestamps();
                $table->foreign("invoice_id")->references('id')->on('invoices');
                $table->foreign("product_id")->references('id')->on('products');
                $table->foreign("gold_maker_id")->references('id')->on('gold_makers');
        });
    }

    public function down()
    {
        Schema::dropIfExists('instocks');
    }
}
