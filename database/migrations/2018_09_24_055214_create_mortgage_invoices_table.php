<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMortgageInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mortgage_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_no');
            $table->unsignedInteger('customer_id')->nullable();
            $table->float('total_amount');
            $table->float('total_less')->nullable();
            $table->float('total_paid')->nullable();
            $table->float('total_balance')->nullable();
            $table->string( 'payment_deadline')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('payment_trxid')->nullable();
            $table->string('note')->nullable();
            $table->string('ext1')->nullable();
            $table->integer('ext3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mortgage_invoices');
    }
}
