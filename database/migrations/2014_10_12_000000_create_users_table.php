<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('username');
            $table->string('usertype');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert([
            'name' => 'Al hamid',
            'username' => 'al_hamid',
            'usertype' => 'admin',
            'email' => 'alhamid@gmail.com',
            'password' => bcrypt('123123'),
        ],
        [
            'name' => 'Employee',
            'username' => 'employee',
            'usertype' => 'user',
            'email' => 'employee@gmail.com',
            'password' => bcrypt('123123'),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
