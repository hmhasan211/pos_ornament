<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignCostTypeLocalCost extends Migration
{

    public function up()
    {
        Schema::table('localcosts', function (Blueprint $table) {
            $table->foreign('cost_type_id')->references('id')->on('cost_types');
        });
    }


    public function down()
    {
        Schema::table('localcosts', function (Blueprint $table) {
            $table->dropForeign(['cost_type_id']);
        });
    }
}
