<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiveFromMahajansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receive_from_mahajans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mahajan_receipt_no')->nullable();
            $table->unsignedInteger('employee_id')->nullable();
            $table->unsignedInteger('mahajan_id')->nullable();
            $table->integer('paid_amount')->nullable();
            $table->string('description')->nullable();
            $table->string('out_date')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receive_from_mahajans');
    }
}
