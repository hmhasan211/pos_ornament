<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignMoneySummaryCostType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('money_summaries', function (Blueprint $table) {
            //
            $table->foreign('cost_type_id')->references('id')->on('cost_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('money_summaries', function (Blueprint $table) {
            //
            $table->dropForeign(['cost_type_id']);
        });
    }
}
