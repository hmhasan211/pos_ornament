<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignReceiveFromMahajansEmployeeMahajan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receive_from_mahajans', function (Blueprint $table) {
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('mahajan_id')->references('id')->on('mahajans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receive_from_mahajans', function (Blueprint $table) {
            $table->dropForeign(['employee_id']);
            $table->dropForeign(['mahajan_id']);

        });
    }
}
